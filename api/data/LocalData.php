<?php 
class LocalData extends LocalEntity { 
 	public static function get($id){
		global $pdo;
		$sql = 'SELECT * FROM local where id=:id';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(':id',$id, PDO::PARAM_STR);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'LocalEntity');
		$obj = array();
		while ($obj = $stmt->fetch()) {
			$obj_vector[] = $obj;
		}
		return $obj_vector;
}
?>