<?php 
class PromotionData extends PromotionEntity { 
 	public static function get($id){
		global $pdo;
		$sql = 'SELECT * FROM promotion where id=:id';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(':id',$id, PDO::PARAM_STR);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'PromotionEntity');
		$obj = array();
		while ($obj = $stmt->fetch()) {
			$obj_vector[] = $obj;
		}
		return $obj_vector;
}
?>