<?php 
class TokenData extends TokenEntity { 
 	public static function get($id){
		global $pdo;
		$sql = 'SELECT * FROM token where id=:id';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(':id',$id, PDO::PARAM_STR);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'TokenEntity');
		$obj = array();
		while ($obj = $stmt->fetch()) {
			$obj_vector[] = $obj;
		}
		return $obj_vector;
}
?>