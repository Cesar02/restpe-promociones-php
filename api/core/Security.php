<?php
    class Security {
        
        private $isLogged = false;
        private $usuario_id = null;

        public function __construct($isRest){

            date_default_timezone_set('America/Lima');

            if (Security::getToken()) {
                if(Security::getToken() == "-1"){


                }else {
                    $token_id = Security::getClientIdByToken(Security::getToken());
                    // $token_id = 2;
                    if ($token_id == -1) {
                        exit(json_encode(array("tipo" => NOPERMITIDO, "mensajes" => array("Token inválido"), "data" => null)));
                    } else {
                        if (!$isRest) {
                            $this->isLogged = true;
                        }
                    }
                }
            } else {
                if(isset($_SESSION["persona_id"])){

                    $this->isLogged = true;

                }else if (!isset($_SESSION["persona_id"])) {
                    
                    if ($isRest) {
                        /**
                         * DEFINO LA VARAIBLE PUBLIC_SERVICES para poder loguear y no verifique mi token RESTFULL
                         */
                        if (defined("PUBLIC_SERVICES")) {
                            // exit(json_encode(array("tipo" => NOPERMITIDO, "mensajes" => array("Token invalido"), "data" => null)));
                        } else {
                            exit(json_encode(array("tipo" => NOPERMITIDO, "mensajes" => array("Debe iniciar sesión"),"data"=>array())));
                        }
                    } else {
                        $this->isLogged = false;
                    }
                } else {
                    $this->isLogged = true;
                }
            }
        }

        public static function getToken(){
            $token = "";
            if (isset($_GET["token"]) && $_GET["token"]) {
                $token = $_GET["token"];
            } else {
                $headers = apache_request_headers();
                if (isset($headers['Authorization'])) {
                    $matches = array();
                    preg_match('/Token token="(.*)"/', $headers['Authorization'], $matches);
                    if (isset($matches[1])) {
                        $token = $matches[1];
                    }
                }
            }
            return $token;
        }

        public function isLogged() {
            return $this->isLogged;
        }

        public static function setSession($nombre, $valor) {
            if (isset($_SESSION[$nombre])) {
                $_SESSION[$nombre] = $valor;
            } else {
                $_SESSION[$nombre] = $valor;
        }
        }

        public static function logout() {
            session_destroy();
        }

        public static function getSession($nombre) {
            if (isset($_SESSION[$nombre])) {
                return $_SESSION[$nombre];
            } else {
                return false;
            }
        }

        public static function getCurrentPersonabiId() {
        
                if (Security::getToken()) {
                    $usuario_id = Token::getUsuarioId(Security::getToken());
                    if ($usuario_id != -1) {
                        return $usuario_id;
                    } else {
                        return "0";
                    }
                    //Validar si el token es valido y no ha expirado
                }
                return Security::$usuario_id;
        }

        public static function getCurrentUserId() {
            if (isset($_SESSION["usuario_id"])) {
                return $_SESSION["usuario_id"];
            } else {
                $persona_id = Security::getCurrentClienteId();
                if($persona_id!=null && $persona_id!="" && $persona_id!="0"){
                    $usuario = Usuario::getByFields(array(
                        array('field'=>'persona_id','value'=>$persona_id,'operator'=>'=')
                    ))["usuario_array"];
                    if(isset($usuario) && is_array($usuario) && sizeof($usuario)>0){
                        return $usuario[0]->usuario_id;
                    }
                }
                return null;
            }
        }

        public static function getCurrentUser() {
            $objUsuario=Usuario::getById(Security::getCurrentUserId());
            if($objUsuario){
                return $objUsuario;
            }else{
                return "";
            }
        }

        public static function getCurrentUsername() {
            if (isset($_SESSION["usuario_nombres"])) {
                return $_SESSION["usuario_nombres"];
            } else {

            }
        }

        public static function getCurrentUserNick() {
            if (isset($_SESSION["usuario_usuario"])) {
                return $_SESSION["usuario_usuario"];
            } else {

            }
        }

        public static function esVersionOnline(){
            if (strpos($_SERVER["SERVER_NAME"], '.pe') !== false || strpos($_SERVER["SERVER_NAME"], '.com') !== false) {
                return true;
            } else {
                return false;
            }
        }

        public static function esSSL(){
            if ((isset($_SERVER['HTTP_X_FORWARDED_PORT']) && $_SERVER['HTTP_X_FORWARDED_PORT'] == '443') || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
                return true;
            } else {
                return false;
            }
        }

        public static function cors(){
            // Allow from any origin
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: PUT, GET, POST, OPTIONS, DELETE");
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
        }


        public static function getVersion(){
            $version = "";
            $headers = apache_request_headers();
            if (isset($headers['version'])) {
                $version = $headers['version'];
            }
            return $version;
        }

        public static function getClientIdByToken($token){

            $array_resultado = Token::getByFields(array(
                array('field'=>'token_valor','value'=>$token,'operator'=>'='),
                array('field'=>'token_estado','value'=>'1','operator'=>'=')
            ))['token_array'];
            if (count($array_resultado) == 0) {
                return -1;
            } else {
                /**
                 * validaria si el token debe expirar, y cuando debe expirar
                 */
                $fechaHoy = new DateTime();
                $fechaExpiracion = new DateTime($array_resultado[0]->token_fechaexpiracion);
                if ($fechaHoy > $fechaExpiracion) {
                    $array_resultado[0]->token_estado = 0;
                    $array_resultado[0]->update();
                    return -1;
                };
            };

            return $array_resultado[0]->token_id;
        }

        public static function getCurrentClienteId(){
            if (isset($_SESSION["cliente_id"])) {
                return $_SESSION["cliente_id"];
            } else {
                if (Security::getToken()) {
                    $client_id = Security::getClientIdByToken(Security::getToken());
                    if ($client_id != -1) {
                        return $client_id;
                    } else {
                        return "0";
                    }
                }
                // return Security::$usuario_id;
            }
        }

        public static function getCurrentSimboloMoneda(){
            return "S/";
        }

        public static function pathServer(){
            // exit(__DIR__);
            return __DIR__ . "/../../";
            /*
            $path_root = "C:\\xampp\\htdocs\\php_ahorramas_restpe\\";
            if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
                $path_root = "/Applications/XAMPP/htdocs/php_ahorramas_restpe/";
                if(stristr(PHP_OS, 'LINUX')){
                    $path_root = "/var/www/html/php_ahorramas_restpe/";
                }
            }
            return $path_root;
            */
        }

        public static function getCountry_id(){
            return 1;
        }

        
        public static function urlEncode64Encode($data)
        {
            return rtrim(strtr($data, '+/', '-_'), '=');
        }


        public static function generarTokenAleatorio(){
            // $token = new stdClass();

            // $fechaHoy = new DateTime();
            // $token->fecha = $fechaHoy->format("Y-m-d H:i:s");
            // $token->token = Utility::getUUID();
            // $token = Security::urlEncode64Encode(json_encode($token));
            // return base64_decode($token);
            return Utility::getUUID();
        }

    }

?>
