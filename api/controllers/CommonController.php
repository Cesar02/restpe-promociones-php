<?php

use phpDocumentor\Reflection\Types\Object_;

class CommonController
{

	function __construct()
	{
		$db = DB::getInstance();
		$pdo = $db->dbh;
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	public function login($params){
		$data = array();
		$mensajes = array();
		$tipo = ERROR;
		$datos = null;

		if(!isset($params->usuario_usuario) || $params->usuario_usuario == '')
		$mensajes[] = 'ingrese un usuario valido';

		if(!isset($params->usuario_clave) || $params->usuario_clave == '')
		$mensajes[] = 'ingrese una clave valida';

		if(empty($mensajes)){
			$datos = Usuario::usuarioExisteByUsuario_usuarioAndUsuarioClave($params->usuario_usuario , $params->usuario_clave);
			if($datos){
				$datos->token = Token::getTokenByUsuario_id($datos->usuario_id);
				if ($datos->token) {
					$tipo = SUCCESS;
					$mensajes[] = 'Logueado con exito...';
				} else {
					$mensajes[] = 'Usted aun no cuenta con un token...';
				}
			} else {
				$mensajes[] = 'Usuario no existe...';
			}
		};
		$data['data'] = $datos;
		$data['mensajes'] = $mensajes;
		$data['tipo'] = $tipo;
		$data['logs'] = Utility::$logs;
		return $data;
	}

	public function obtenerCategorias(){
		$data = array();
		$mensajes = array();
		$tipo = ERROR;
		$datos = null;

		$datos = Utility::obtenerCategoriaList();

		if($datos){
			$tipo = SUCCESS;
			$mensajes[] = 'Categorias obtenidas con exito...';
		} else {
			$mensajes[] = 'No hay categorias...';
		}

		$data['data'] = $datos;
		$data['mensajes'] = $mensajes;
		$data['tipo'] = $tipo;
		$data['logs'] = Utility::$logs;
		return $data;
	}

}
