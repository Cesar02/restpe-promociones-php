<?php

use Monolog\Handler\Curl\Util;

class PromotionController { 
     function __construct() { 
        $db = DB::getInstance();
        $pdo = $db->dbh;
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    public function add($obj) {
    	$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
    	$datos = '';
    	if (empty($mensajes)) {
    		$obj = new Promotion($obj);
    		$resultado = $obj->insert();
    		if ($resultado) {
    			$tipo = SUCCESS;
    			$mensajes[] = 'Se agregó con éxito.';
    			$datos = $resultado;
    		} else {
    			$tipo = DANGER;
    			$mensajes[] = 'Se produjo un error al crear. Inténtalo de nuevo.';
    		}
    	}
    	$data['mensajes'] = $mensajes;
    	$data['tipo'] = $tipo;
    	$data['data'] = $datos;
    	return $data;
    }
    
    public function update($obj) {
    	$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
    	if (empty($mensajes)) {
    		$obj=new Promotion($obj);
    		$resultado = $obj->update();
    		if ($resultado) {
    			$tipo = SUCCESS;
    			$mensajes[] = 'Se actualizó con éxito.';
    		} else {
    			$tipo = DANGER;
    			$mensajes[] = 'Se produjo un error al modificar. Inténtalo de nuevo.';
    		}
    	}
    	$data['mensajes'] = $mensajes;
    	$data['tipo'] = $tipo;
    	return $data;
    }
    
    public function getById($id) {
    	$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
    	$datos = Promotion::getById($id);
    	if(!$datos){
    		$mensajes[] = 'Error, Valor no Encontrado';
    		$tipo = DANGER;
    	}
    	$data['mensajes'] = $mensajes;
    	$data['tipo'] = $tipo;
    	$data['data'] = $datos;
    	return $data;
    }
    
    public function delete($id) {
    	$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
    	$obj = Promotion::getById($id);
    	if($obj!=false){
    	$obj->delete();
    	}else{
    		$mensajes[] = 'Error, Valor no Encontrado';
    		$tipo = DANGER;
    	}
    	$data['mensajes'] = $mensajes;
    	$data['tipo'] = $tipo;
    	return $data;
    }
    
    public function listarPorPaginacion($pagina,$registros) {
    	$array_salida = Promotion::getByFields(array(),array(),($pagina-1)*$registros,$registros);
    	$totalCount=$array_salida['totalCount'];
    	$array_salida=$array_salida['promotion_array'];
    	return array('lista'=>$array_salida,'totalCount'=>$totalCount);
    }
    
    public function getAllActivos() {
    	$array_salida = Promotion::getByFields(array(
    	array('field'=>'promotion_estado','value'=>'1','operator'=>'=')
    	));
    	$array_salida=$array_salida['promotion_array'];
    	return $array_salida;
    }

	public function promotionCreate($obj){

		$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
		$resultado = null;

		if(!isset($obj)){
			$mensajes[] = 'data es obligatorio.';
		};


		if(empty($mensajes)){

			// Pedimos token a Yape
			$access_token = Utility::getTokenYape();

			if($access_token && isset($access_token->access_token)){
				$resultado = Utility::createPromotionYape($access_token->access_token, $access_token->scope, $access_token->event, $obj);
				if($resultado['tipo'] == SUCCESS){
					Utility::$logs = $resultado['data'];

					$tipo = SUCCESS;
					$mensajes[] = 'Se agregó con éxito.';
				}else{
					$tipo = ERROR;
					$mensajes[] = 'Se produjo un error al crear la promonicion. Inténtalo de nuevo.';
				}

			}else{
				$tipo = ERROR;
				$mensajes[] = 'No se pudo obtener el token de Yape';
			};
		};

		$data['mensajes'] = $mensajes;
		$data['tipo'] = $tipo;
		$data['data'] = $resultado;
		$data['logs'] = Utility::$logs;
		return $data;
	}

	public function promotionUpdate($obj){

		$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
		$resultado = null;

		if(!isset($obj)){
			$mensajes[] = 'data es obligatorio.';
		};


		// Falta validar bien los campos

		if(empty($mensajes)){

			// Pedimos token a Yape
			$access_token = Utility::getTokenYape();

			if($access_token && isset($access_token->access_token)){
				$resultado = Utility::updatePromotionYape($access_token->access_token, $access_token->scope, $access_token->event, $obj);
				if($resultado['tipo'] == SUCCESS){
					Utility::$logs = $resultado['data'];

					$tipo = SUCCESS;
					$mensajes[] = 'Se agregó con éxito.';
				}else{
					$tipo = ERROR;
					$mensajes[] = 'Se produjo un error al crear la promonicion. Inténtalo de nuevo.';
				}

			}else{
				$tipo = ERROR;
				$mensajes[] = 'No se pudo obtener el token de Yape';
			};
		};

		$data['mensajes'] = $mensajes;
		$data['tipo'] = $tipo;
		$data['data'] = $resultado;
		$data['logs'] = Utility::$logs;
		return $data;
	}

	public function promotionDelete($promotion_id){

		$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
		$resultado = null;

		if(!isset($promotion_id) || !Utility::validarEnteroPositivo($promotion_id)){
			$mensajes[] = 'promotion_id es obligatorio.';
		};

		if(empty($mensajes)){
			// Validamos si existe la promotion
			$promotion = Promotion::promotionExisteByPromotionID($promotion_id);
			if($promotion){

				// Pedimos token a Yape
				$access_token = Utility::getTokenYape();

				if($access_token && isset($access_token->access_token)){

					$obj = new stdClass();
					$obj->promotion_id = $promotion_id;

					$resultado = Utility::deletePromotionYape($access_token->access_token, $access_token->scope, $access_token->event, $obj);
					if($resultado['tipo'] == SUCCESS){
						Utility::$logs = $resultado['data'];

						$tipo = SUCCESS;
						$mensajes[] = 'Se agregó con éxito.';
					}else{
						$tipo = ERROR;
						$mensajes[] = 'Se produjo un error al crear la promonicion. Inténtalo de nuevo.';
					}

				}else{
					$tipo = ERROR;
					$mensajes[] = 'No se pudo obtener el token de Yape';
				};

			}else{
				$mensajes[] = 'No se encontró la promotion';
				$tipo = ERROR;
			};
		};

		$data['mensajes'] = $mensajes;
		$data['tipo'] = $tipo;
		$data['data'] = $resultado;
		$data['logs'] = Utility::$logs;
		return $data;
	}

	public function promotionList(){
		$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
		$datos = Promotion::getPromotionList();
		$datos = $datos['promotion_array'];
		$promotionsForYape = array();

		if(sizeof($datos) > 0){
			// Utility::$logs[] = $datos;
			foreach ($datos as $promotion) {
				$promotionsForYape[] = Utility::obtenerPromotionYape($promotion , $promotion->productos);
			};
			$mensajes[] = 'Se encontraron promotions';
		};

		$data['mensajes'] = $mensajes;
		$data['tipo'] = $tipo;
		$data['data'] = $datos;
		$data['logs'] = Utility::$logs;
		// return $data;
		return $promotionsForYape;
	}

	public function promotionById($promotion_id){
		$data = array();
		$mensajes = array();
		$tipo = SUCCESS;
		$datos = Promotion::promotionExisteByPromotionID($promotion_id);
		$promotionForYape = null;

		if($datos){
			$promotionForYape = Utility::obtenerPromotionYape($datos , $datos->productos);
			$mensajes[] = 'Se encontró la promotion';
		}else{
			$mensajes[] = 'No se encontró la promotion';
			$tipo = ERROR;
		};

		$data['mensajes'] = $mensajes;
		$data['tipo'] = $tipo;
		$data['data'] = $datos;
		// return $data;
		return $promotionForYape;
	}

	public function productoStockById($producto_id){
		$data = array();
		$mensajes = array();
		$tipo = SUCCESS;
		$datos = Producto::obtenerProductoByProductoID($producto_id);
		if($datos){
			$mensajes[] = 'Se encontró el producto';
		}else{
			$mensajes[] = 'No se encontró el producto';
			$tipo = ERROR;
		};

		// $data['mensajes'] = $mensajes;
		// $data['tipo'] = $tipo;
		// $data['data'] = $datos;
		// return $data;
		$data['stock'] = true;
		$data['amount'] = 1;
		return $data;
	}

	public function venderProducto($obj){

		$data = array();
		$mensajes = array();
		$tipo = SUCCESS;
		$datos = array();

		$producto = Producto::obtenerProductoByProductoID($obj->product_id);
		if($producto){
			$stock = $producto->stock - $obj->amount;
			if($stock >= 0){
				$producto->stock = $stock;
				$producto->update();
				$mensajes[] = 'Se vendió el producto';
			}else{
				$mensajes[] = 'No hay stock suficiente';
				$tipo = ERROR;
			}
		}else{
			$mensajes[] = 'No se encontró el producto';
			$tipo = ERROR;
		}

		// $data['mensajes'] = $mensajes;
		// $data['tipo'] = $tipo;
		// $data['data'] = $datos;

		$data['status']  = $tipo == SUCCESS ? true : false;
		$data['coupons'] = array();
		return $data;
	}
    
}
?>