<?php

class TokenController { 
     function __construct() { 
        $db = DB::getInstance();
        $pdo = $db->dbh;
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    public function add($obj) {
    	$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
    	$datos = '';
    	if (empty($mensajes)) {
    		$obj = new Token($obj);
    		$resultado = $obj->insert();
    		if ($resultado) {
    			$tipo = SUCCESS;
    			$mensajes[] = 'Se agregó con éxito.';
    			$datos = $resultado;
    		} else {
    			$tipo = DANGER;
    			$mensajes[] = 'Se produjo un error al crear. Inténtalo de nuevo.';
    		}
    	}
    	$data['mensajes'] = $mensajes;
    	$data['tipo'] = $tipo;
    	$data['data'] = $datos;
    	return $data;
    }
    
    public function update($obj) {
    	$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
    	if (empty($mensajes)) {
    		$obj=new Token($obj);
    		$resultado = $obj->update();
    		if ($resultado) {
    			$tipo = SUCCESS;
    			$mensajes[] = 'Se actualizó con éxito.';
    		} else {
    			$tipo = DANGER;
    			$mensajes[] = 'Se produjo un error al modificar. Inténtalo de nuevo.';
    		}
    	}
    	$data['mensajes'] = $mensajes;
    	$data['tipo'] = $tipo;
    	return $data;
    }
    
    public function getById($id) {
    	$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
    	$datos = Token::getById($id);
    	if(!$datos){
    		$mensajes[] = 'Error, Valor no Encontrado';
    		$tipo = DANGER;
    	}
    	$data['mensajes'] = $mensajes;
    	$data['tipo'] = $tipo;
    	$data['data'] = $datos;
    	return $data;
    }
    
    public function delete($id) {
    	$data = array();
    	$mensajes = array();
    	$tipo = SUCCESS;
    	$obj = Token::getById($id);
    	if($obj!=false){
    	$obj->delete();
    	}else{
    		$mensajes[] = 'Error, Valor no Encontrado';
    		$tipo = DANGER;
    	}
    	$data['mensajes'] = $mensajes;
    	$data['tipo'] = $tipo;
    	return $data;
    }
    
    public function listarPorPaginacion($pagina,$registros) {
    	$array_salida = Token::getByFields(array(),array(),($pagina-1)*$registros,$registros);
    	$totalCount=$array_salida['totalCount'];
    	$array_salida=$array_salida['token_array'];
    	return array('lista'=>$array_salida,'totalCount'=>$totalCount);
    }
    
    public function getAllActivos() {
    	$array_salida = Token::getByFields(array(
    	array('field'=>'token_estado','value'=>'1','operator'=>'=')
    	));
    	$array_salida=$array_salida['token_array'];
    	return $array_salida;
    }
	
	public function obtenerToken(){

		$data = array();
		$mensajes = array();
		$tipo = ERROR;
		$datos = null;

		$token = new Token();
		$token->setToken_valor(Security::generarTokenAleatorio());
		$token->setToken_estado(ACTIVO);
		$token->setToken_ultimoacceso(Utility::getFechaHoraActual());
		$fecha = new DateTime();
		// 2 horas de duracion del token
		$fecha->add(new DateInterval('PT2H'));
		$token->setToken_fechaexpiracion($fecha->format('Y-m-d H:i:s'));
		$token->setToken_estado(ACTIVO);
		$token->setToken_fechacreacion(Utility::getFechaHoraActual());
		$datos = $token->insert() ;
		if($datos){
			$tipo = SUCCESS;
			$mensajes[] = 'Token obtenido con exito...';
		} else {
			$mensajes[] = 'No se pudo obtener el token...';
		}

		// $data['data'] = $datos;
		// $data['mensajes'] = $mensajes;
		// $data['tipo'] = $tipo;
		// $data['logs'] = Utility::$logs;
		$data['status'] = ($tipo == SUCCESS) ? true  : false;
		$data['token'] = ($tipo == SUCCESS ) ? $token->getToken_valor() : null;
		return $data;
	}
    
}
?>