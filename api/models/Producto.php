<?php 
class Producto extends ProductoEntity { 

    public static function executeQuery($query, $params = array()){
        global $pdo;
        $stmt = $pdo->prepare($query);
        for ($i = 0; $i < sizeof($params); $i++) {
            $stmt->bindValue($i + 1, $params[$i]);
        }
        $resultado = $stmt->execute();
        return $resultado;
    }

    public static function findWithQuery($query, $params = array()){
        global $pdo;
        $stmt = $pdo->prepare($query);
        for ($i = 0; $i < sizeof($params); $i++) {
            $stmt->bindValue($i + 1, $params[$i]);
        }
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, "Producto");

        $sent_vector = array();
        while ($sent = $stmt->fetch()) {
            $sent_vector[] = $sent;
        }
        return $sent_vector;
    }

    public static function obtenerProductoByProductoID($producto_id) {
        $producto = Producto::findWithQuery("SELECT * FROM producto WHERE producto_id = ? ", array($producto_id));
        if(sizeof($producto) > 0){
            return $producto[0];
        }else{
            return null;
        }
    }

    public static function obtenerProductosByPromotionID($promotion_id) {
        $vector = Producto::findWithQuery("SELECT * FROM producto WHERE promotion_id = ? ", array($promotion_id));
        return $vector;
    }
 }
?>