<?php 
class Promotion extends PromotionEntity { 

    public static function executeQuery($query, $params = array()){
        global $pdo;
        $stmt = $pdo->prepare($query);
        for ($i = 0; $i < sizeof($params); $i++) {
            $stmt->bindValue($i + 1, $params[$i]);
        }
        $resultado = $stmt->execute();
        return $resultado;
    }

    public static function findWithQuery($query, $params = array()){
        global $pdo;
        $stmt = $pdo->prepare($query);
        for ($i = 0; $i < sizeof($params); $i++) {
            $stmt->bindValue($i + 1, $params[$i]);
        }
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, "Promotion");

        $sent_vector = array();
        while ($sent = $stmt->fetch()) {
            $sent_vector[] = $sent;
        }
        return $sent_vector;
    }

    public static function promotionExisteByPromotionID($promotion_id) {
        $promotion = Promotion::findWithQuery("SELECT * FROM promotion WHERE promotion_id = ? ", array($promotion_id));
        if(sizeof($promotion) > 0){
            $promotion[0]->categoria = Utility::obtenerCateriaByID($promotion[0]->categoria_id);
            $promotion[0]->local = Local::getById($promotion[0]->local_id);
            $promotion[0]->productos = Producto::obtenerProductosByPromotionID($promotion[0]->promotion_id);
            return $promotion[0];
        }else{
            return null;
        }
    }

    public static function getPromotionList($objParametros = null , $pagina = PARAM_TODOS, $registros = PARAM_TODOS , $readonly = SI){

        global $pdo;
        $vector = array();
        $sqlSelect = "pr.*";
        $sqlInner = "";
        $sqlWhere = " and pr.promotion_estado = 1 "; 
        $sqlOrder = " order by pr.promotion_id desc";
        $sqlLimit = "";

        if(isset($pagina) && Utility::validarEnteroPositivo($pagina) && isset($registros) && Utility::validarEnteroPositivo($registros))
        $sqlLimit = " LIMIT " . ($pagina - 1) * $registros . "," . $registros;

        // if(isset($objParametros->busqueda) && strlen(trim($objParametros->busqueda)) && $objParametros->busqueda != PARAM_TODOS) {
        //     $sqlWhere .= " and ( cl.cliente_nombres like '%" . $objParametros->busqueda . "%'  ";
        //     $sqlWhere .= " or cl.cliente_dni like '%" . $objParametros->busqueda . "%' ) ";
        // };

        $sql = "SELECT SQL_CALC_FOUND_ROWS " . $sqlSelect ." FROM promotion pr " . $sqlInner . " where true " . $sqlWhere . $sqlOrder . $sqlLimit ;

        Utility::$logs[] = $sql;

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'PromotionEntity');
        $result = $pdo->query("SELECT FOUND_ROWS() AS totalCount");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();
        $obj = array();
        while ($obj = $stmt->fetch()) {
            $obj->categoria = Utility::obtenerCateriaByID($obj->categoria_id);
            $obj->local = Local::getById($obj->local_id);
            $obj->productos = Producto::obtenerProductosByPromotionID($obj->promotion_id);
            $vector[] = $obj;
        }
        return array("promotion_array" => $vector, "totalCount" => $row["totalCount"]);
    }

 }
?>