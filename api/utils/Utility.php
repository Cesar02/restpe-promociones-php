<?php

use Twilio\Rest\Client as ClientTwilio;
use GuzzleHttp\Client;
use Monolog\Handler\Curl\Util;

class Utility
{
    public static $client = null;
    public static $keyString = "7pMTJERfKuAMFUdtbjePWA==";
    public static $logs = array();

    function __construct()
    {
    }

    public static function readTemplateFile($FileName){
        $fp = fopen($FileName, "r") or exit("Unable to open File " . $FileName);
        $str = "";
        while (!feof($fp)) {
            $str .= fread($fp, 1024);
        }
        return $str;
    }

    public static function getStdClassByObject($d){
        if (is_array($d)) {
            /*
             * Return array converted to object
             * Using __FUNCTION__ (Magic constant)
             * for recursive call
             */
            return (object) array_map(__FUNCTION__, $d);
        } else {
            // Return object
            return $d;
        }
    }

    public static function getHoraPorDecimal($num_hours){
        $hours = floor($num_hours);
        $mins = round(($num_hours - $hours) * 60);
        if ($mins <= 9) {
            $mins = "0" . $mins;
        }
        return $hours . ":" . $mins . ":00";
    }

    public static function getMesFullCalendarToPhp($mes){
        switch ($mes) {
            case 1:
                return 2;
                break;
            case 2:
                return 3;
                break;
            case 3:
                return 4;
                break;
            case 4:
                return 5;
                break;
            case 5:
                return 6;
                break;
            case 6:
                return 7;
                break;
            case 7:
                return 8;
                break;
            case 8:
                return 9;
                break;
            case 9:
                return 10;
                break;
            case 10:
                return 11;
                break;
            case 11:
                return 12;
                break;
            case 12:
                return 1;
                break;
            default:
                return 1;
                break;
        }
    } 

    public static function getPrettyDate($date){
        return date('d/m/Y', strtotime(str_replace('-', '/', $date)));
    }

    public static function getPrettyDateTime($datetime){
        return date('d-m-Y H:i:s', strtotime(str_replace('-', '/', $datetime)));
    }

    public static function subirArchivo($rutaDestino){
        $data = array();
        $tipo = SUCCESS;
        $mensajes = array();
        $datos = "";
        if (empty($mensajes)) {
            /**
             * obtengo la ruta relativa
             */

            $rutaDestinoRelativa = str_replace("./", "", $rutaDestino);
            $rutaDestinoRelativa = str_replace(".", "", $rutaDestinoRelativa);

            /**
             * valido si el directorio existe, sino lo creo
             */
            if (!is_dir($rutaDestino)) {
                mkdir($rutaDestino, 0777, true);
            }
            if (isset($_FILES["archivo"]["error"])) {
                if ($_FILES["archivo"]["error"] > 0) {
                    /**
                     * si es mayor a 0, existe un error
                     */
                    $tipo = ERROR;
                    $mensajes[] = $_FILES["archivo"]["error"];
                } else {
                    /**
                     * obtenemos valores de la imagen
                     */
                    $nombre = strtolower($_FILES["archivo"]["name"]);
                    $type = $_FILES["archivo"]["type"];
                    $tamanio = ($_FILES["archivo"]["size"] / 1024);
                    $nombreTemporal = $_FILES["archivo"]["tmp_name"];
                    $array_explode = explode(".", $nombre);
                    $extension = end($array_explode);
                    $extensiones = array("jpg", "png", "gif", "jpeg", "bmp");
                    if (in_array($extension, $extensiones)) {
                        /**
                         * Si el archivo tiene una extension valida, procedo a mover la imagen a una ubicacion deseada
                         */
                        $nombreNuevo = md5(date("Y-h-m-i") . rand());
                        $filename_normal = $nombreNuevo . '-normal.' . $extension;
                        if (move_uploaded_file($nombreTemporal, $rutaDestino . "/" . $filename_normal)) {
                            /**
                             * Guardo la ruta de la imagen original
                             */
                            $datos = $rutaDestinoRelativa . "/" . $filename_normal;
                            /**
                             * Ahora procedo a crear los thumbnails
                             */
                            try {
                                $filename_thumb = $nombreNuevo . "-thumb." . $extension;
                                $image = new ImageResize($rutaDestino . "/" . $filename_normal);
                                $image->resizeToBestFit(400, 300);
                                $image->save($rutaDestino . "/" . $filename_thumb);
                                $datos = $rutaDestinoRelativa . "/" . $filename_thumb;


                                $filename_medium = $nombreNuevo . "-medium." . $extension;
                                $image->resizeToBestFit(1024, 768);
                                $image->save($rutaDestino . "/" . $filename_medium);
                            } catch (Exception $e) {
                            }
                        } else {
                            $tipo = ERROR;
                            $mensajes[] = "Se produjo un error moviendo el archivo";
                        }
                    } else {
                        $tipo = ERROR;
                        $mensajes[] = "El archivo tiene una extensio no permitida";
                    }
                }
            } else {
                $tipo = WARNING;
                $mensajes[] = "Debe seleccionar una imagen";
            }
        }
        $data["mensajes"] = $mensajes;
        $data["tipo"] = $tipo;
        $data["data"] = $datos;
        return $data;
    }

    function dias_transcurridos($fecha_i, $fecha_f){
        $dias = (strtotime($fecha_i) - strtotime($fecha_f)) / 86400;
        $dias = abs($dias);
        $dias = floor($dias);
        return $dias;
    }

    /*
     * 
     */

    public static function sendErrorCatch($body){
        if (SEND_ERRORS) {
            self::enviarEmailError("Desarrollo", "desarrollo@gmail.com", "ERROR " . APP_NAME . " " . date("Y-m-d"), "", $body, 'mailgun');
        }
    }

    public static function enviarEmailError($nombreDestinatario, $emailDestinatario, $asunto, $text_body, $html_body, $metodo, $attachments = null, $attachments_name = null){
        $resultado = false;
        switch ($metodo) {
            case 'amazon':
                $ses = new SimpleEmailService(SES_ACCESSKEY, SES_SECRETKEY);
                $m = new SimpleEmailServiceMessage();

                $m->addTo($nombreDestinatario . ' <' . $emailDestinatario . '>');
                $m->setFrom('Sistema de Reporte de Errores <notificaciones@kirudental.com>');
                $m->setSubject($asunto);
                $m->setMessageFromString($text_body, $html_body);
                $resultado = $ses->sendEmail($m);
                break;

            case 'mailgun':
                $mail = new PHPMailer;
                $mail->CharSet = 'UTF-8';

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = '';  // Specify main and backup server
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = '';                            // SMTP username
                $mail->Password = '5kb6sd19mzm0';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
                //$mail->Port       = 587;

                $mail->From = '';
                $mail->FromName = '';
                $mail->addAddress($emailDestinatario, $nombreDestinatario);  // Add a recipient
                //$mail->addAddress('ellen@example.com');               // Name is optional
                //$mail->addReplyTo('notificaciones@kirudental.com', 'KiruDental');
                //$mail->addCC('cc@example.com');
                //$mail->addBCC('bcc@example.com');

                $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
                //
                if ($attachments != null) {
                    if ($attachments_name != null) {
                        $mail->addAttachment($attachments, $attachments_name);    // Optional name
                    } else {
                        $mail->addAttachment($attachments);         // Add attachments
                    }
                }

                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = $asunto;
                $mail->Body = $html_body;
                $mail->AltBody = $text_body;

                $resultado = $mail->send();
                break;
        }
        return $resultado;
    }

    public static function sendErrorEntity($e)
    {
        if (SEND_ERRORS) {
            $error_json = json_encode(array("ERROR" => $e, "MESSAGE" => $e->getMessage(), "CODE" => $e->getCode(), "FILE" => $e->getFile(), "LINE" => $e->getLine()));
            $request_json = json_encode(array("REQUEST" => $_REQUEST));
            $session_json = json_encode(array("SESSION" => $_SESSION));
            $server_json = json_encode(array("SERVER" => $_SERVER));

            $body = $error_json . "<br>" . $request_json . "<br>" . $session_json . "<br>" . $server_json;
            if (array_key_exists("HTTP_REFERER", $_SERVER)) {
                self::enviarEmailError("Ruben Sedano", "@m", "ERROR " . APP_NAME . " (" . $_SERVER["HTTP_REFERER"] . ") " . date("Y-m-d H:i:s"), "", $body, 'mailgun');
            } else {
                self::enviarEmailError("Ruben Sedano", "@gmail.com", "ERROR " . APP_NAME . " " . date("Y-m-d H:i:s"), "", $body, 'mailgun');
            }
        }
    }

    public static function enviarEmail($nombreDestinatario, $emailDestinatario, $asunto, $text_body, $html_body, $metodo, $attachments = null, $attachments_name = null)
    {
        $resultado = false;
        switch ($metodo) {
            case 'amazon':
                $ses = new SimpleEmailService(SES_ACCESSKEY, SES_SECRETKEY);
                $m = new SimpleEmailServiceMessage();

                $m->addTo($nombreDestinatario . ' <' . $emailDestinatario . '>');
                $m->setFrom('Listas Promotor - ITDAST PERU <notificaciones@kirudental.com>');
                $m->setSubject($asunto);
                $m->setMessageFromString($text_body, $html_body);
                $resultado = $ses->sendEmail($m);
                break;

            case 'mailgun':
                $mail = new PHPMailer;
                $mail->CharSet = 'UTF-8';

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = '';  // Specify main and backup server
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = '';                            // SMTP username
                $mail->Password = '';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
                //$mail->Port       = 587;

                $mail->From = '';
                $mail->FromName = '';
                $mail->addAddress($emailDestinatario, $nombreDestinatario);  // Add a recipient
                //$mail->addAddress('ellen@example.com');               // Name is optional
                //$mail->addReplyTo('notificaciones@kirudental.com', 'KiruDental');
                //$mail->addCC('cc@example.com');
                //$mail->addBCC('bcc@example.com');

                $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
                //
                if ($attachments != null) {
                    if ($attachments_name != null) {
                        $mail->addAttachment($attachments, $attachments_name);    // Optional name
                    } else {
                        $mail->addAttachment($attachments);         // Add attachments
                    }
                }

                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = $asunto;
                $mail->Body = $html_body;
                $mail->AltBody = $text_body;

                $resultado = $mail->send();
                break;
        }
        return $resultado;
    }

    public static function formatErrorOutput($e)
    {
        $mensajeError = $e->getMessage();
        $error_json = json_encode(array("ERROR" => $e, "MESSAGE" => $mensajeError, "CODE" => $e->getCode(), "FILE" => $e->getFile(), "LINE" => $e->getLine()));
        $request_json = json_encode(array("REQUEST" => $_REQUEST));
        $session_json = json_encode(array("SESSION" => $_SESSION));
        $server_json = json_encode(array("SERVER" => $_SERVER));

        $body = $error_json . "<br>" . $request_json . "<br>" . $session_json . "<br>" . $server_json;

        $data = array();

        $tipo = ERROR;
        $palabraABuscar = 'PROCEDURE';

        $coincidenciasBusqueda = strstr($mensajeError, $palabraABuscar);
        if ($coincidenciasBusqueda) {
            $tipo = "501"; //ERROR_PROCEDURE;
        }

        $palabraABuscar = 'checksum_UNIQUE';

        $coincidenciasBusqueda = strstr(strtolower($mensajeError), strtolower($palabraABuscar));
        if ($coincidenciasBusqueda) {
            $tipo = "501"; //ERROR_PROCEDURE;
        }


        $mensajes = array();
        $datos = "";

        $mensajes[] = $body;

        $data["tipo"] = $tipo;
        $data["mensajes"] = $mensajes;
        $data["data"] = $datos;
        return $data;
    }

    public static function setChecksumIfNeeded($obj, $tableName)
    {
        if (!isset($obj)) return;
        if (!isset($tableName) || strlen(trim($tableName)) == 0) return;

        if (!isset($obj->{strtolower($tableName) . "_checksum"}) || empty(trim($obj->{strtolower($tableName) . "_checksum"}))) {
            // setear campo
            $obj->{strtolower($tableName) . "_checksum"} = self::getUniqueChecksum();
        }
    }

    /**
     * Genera una codigo en base a local_id, caja_id, un ramdon y la fecha actual
     */
    public static function getUniqueChecksum()
    {
        return uniqid('HYS-') . "-" . round(microtime(true) * 1000);
    }

    public static function getUUID($id = "")
    {
        if (isset($id) && is_string($id) && $id != "") {
            //$id = str_pad($id, 10, "0", STR_PAD_LEFT);
            $id .= "-";
        } else {
            $id = "";
        }
        return uniqid($id);
    }

    public static function getRangoFechas($start, $end)
    {
        $range = array();

        if (is_string($start) === true)
            $start = strtotime($start);
        if (is_string($end) === true)
            $end = strtotime($end);
        do {
            $range[] = date('Y-m-d', $start);
            $start = strtotime("+ 1 day", $start);
        } while ($start <= $end);

        return $range;
    }

    public static function validarLapsoParaReporteria($f1, $f2, $lapsoDeValidacion = LAPSO_MAXIMO_PARA_REPORTERIA)
    {

        //Validamos que las fechas tengan hora y segundo

        if (strlen($f1) == 10) {
            $f1 = $f1 . " 00:00:01";
        }

        if (strlen($f2) == 10) {
            $f2 = $f2 . " 23:59:59";
        }

        return Utility::obtenerDiasTranscurridos($f1, $f2) > $lapsoDeValidacion;
    }

    public static function obtenerDiasTranscurridos($fechaInicio, $fechaFin)
    {
        return Utility::differenceMilliseconds($fechaInicio, $fechaFin)["days"];
    }

    public static function differenceMilliseconds($dateStart, $dateEnd)
    {
        $start = Utility::sqlInt($dateStart);
        $end = Utility::sqlInt($dateEnd);
        $difference = $end - $start;
        $result = array();
        $result['ms'] = $difference;
        $result['hours'] = $difference / 3600;
        $result['minutes'] = $difference / 60;
        $result['days'] = $difference / 86400;
        return $result;
    }

    public static function sqlInt($date)
    {
        $date = Utility::sqlArray($date);
        return mktime($date['hour'], $date['minutes'], 0, $date['month'], $date['day'], $date['year']);
    }

    public static function sqlArray($date, $trim = true)
    {
        $result = array();
        $result['day'] = ($trim == true) ? ltrim(substr($date, 8, 2), '0') : substr($date, 8, 2);
        $result['month'] = ($trim == true) ? ltrim(substr($date, 5, 2), '0') : substr($date, 5, 2);
        $result['year'] = substr($date, 0, 4);
        $result['hour'] = substr($date, 11, 2);
        $result['minutes'] = substr($date, 14, 2);
        return $result;
    }

    public static function obtenerDescripcionEvento($evento)
    {
        switch ($evento) {
            case 'purchase':
                return 'purchase';
                break;
            case 'add_valorization':
                return 'add_valorization';
                break;
            case 'click_type_business':
                return 'click_type_business';
                break;
            case 'add_delivery_error':
                return 'add_delivery_error';
                break;
            case 'coupon_to_validate':
                return 'coupon_to_validate';
                break;
            case 'search_item':
                return 'search_item';
                break;
            case 'delete_item_to_cart':
                return 'delete_item_to_cart';
                break;
            case 'clear_cart':
                return 'clear_cart';
                break;
            case 'view_search_results':
                return 'view_search_results';
                break;
            case 'search_item_of_establishment':
                return 'search_item_of_establishment';
                break;
            case 'search_empty':
                return 'search_empty';
                break;
            case 'click_banner':
                return 'click_banner';
                break;
            case 'sign_up':
                return 'sign_up';
                break;
            case 'click_typecard_to_pay':
                return 'click_typecard_to_pay';
                break;
            case 'add_delivery':
                return 'add_delivery';
                break;
            case 'ecommerce_purchase':
                return 'ecommerce_purchase';
                break;
            case 'click_way_to_pay':
                return 'click_way_to_pay';
                break;
            case 'add_payment_info':
                return 'add_payment_info';
                break;
            case 'begin_checkout':
                return 'begin_checkout';
                break;
            case 'add_to_cart':
                return 'add_to_cart';
                break;
            case 'view_item':
                return 'view_item';
                break;
            case 'click_product':
                return 'click_product';
                break;
            case 'click_product_general':
                return 'click_product_general';
                break;
            case 'click_type_delivery':
                return 'click_type_delivery';
                break;
            case 'click_establishment':
                return 'click_establishment';
                break;
            case 'open_app':
                return 'open_app';
                break;
            case 'log_out':
                return 'log_out';
                break;
            case 'click_navigation':
                return 'click_navigation';
                break;
            case 'result_establishments':
                return 'result_establishments';
                break;
            case 'click_type_bussines':
                return 'click_type_bussines';
                break;
            case 'login':
                return 'login';
                break;
            case 'result_typebussiness':
                return 'result_typebussiness';
                break;
            case 'click_sugerencia':
                return 'click_sugerencia';
                break;
            case 'coupon_add':
                return 'coupon_add';
                break;
            case 'screen_view':
                return 'screen_view';
                break;
            case 'open_chat':
                return 'open_chat';
                break;
            case 'close_chat':
                return 'close_chat';
                break;
            default:
                return "";
                break;
        }
    }

    /**
     * Quitar las comillas al inicio y al final
     * @param $texto
     * @return bool|string
     */
    public static function limpiarComillasTextoJson($texto)
    {

        if (!Utility::stringContiene($texto, '"')) {
            return $texto;
        }

        $texto = substr($texto, 1);
        $texto = substr($texto, 0, -1);

        return $texto;
    }

    public static function getFechaSegunFormato($fecha, $formato)
    {
        try {
            $fecha = new DateTime($fecha);
            return $fecha->format($formato);
        } catch (Exception $er) {
            return "";
        }
    }

    public static function getFechaCortaFormateadaBD($stringFecha)
    {
        if (!isset($stringFecha) || $stringFecha == null) return "";
        try {
            $date = new DateTime($stringFecha);
            return $date->format('Y-m-d');
        } catch (Exception $err) {
            return null;
        }
    }

    public static function stringContiene($string, $substring)
    {
        $string = Utility::normalizarString(mb_strtolower($string));
        $substring = Utility::normalizarString(mb_strtolower($substring));

        $contiene = false;

        if (strpos($string, $substring) !== false) {
            $contiene = true;
        }
        return $contiene;
    }

    public static function normalizarString($cadena)
    {

        $originales  = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);
        return utf8_encode($cadena);
    }


    public static function getFechaPrimerDiasMesActual()
    {
        return date("Y-m-01");
    }

    public static function getFechaActual()
    {
        return date("Y-m-d");
    }

    public static function getFechaHoraActual()
    {
        date_default_timezone_set('America/Lima');
        return (Date("Y-m-d H:i:s"));
    }

    public static function getFechaHoraMilisegundosActual($zonaHoraria = "")
    {
        $now = DateTime::createFromFormat('U.u', microtime(true));
        if ($now) {
            $now->setTimezone(new DateTimeZone(date_default_timezone_get()));
            return $now->format("Y-m-d H:i:s.u");
        } else {
            return date("Y-m-d H:i:s.u");
        }
    }

    public static function addTimeToDate($stringFecha, $valor, $format = "Y-m-d H:i:s")
    {
        return date($format, strtotime($valor, strtotime($stringFecha)));
    }

    public static function valorEstaVacio($valor)
    {
        return ($valor == null || $valor == "");
    }

    public static function getTextoAPP()
    {

        return TEXT_APLICACION;
    }

    public static function capture($exception){
        if (Security::esVersionOnline()) {
            Sentry\configureScope(function (Sentry\State\Scope $scope): void {
                //if (Security::getToken()) {
                $scope->setExtra("VERSIONAPP", Security::getVersion());
                $scope->setExtra("PLATFORM", Security::getPlatform());
                $scope->setExtra("FECHA", Utility::getFechaHoraActual());
                $scope->setExtra("SERVER", json_encode(array("SERVER" => $_SERVER)));
                $scope->setExtra("SESSION", json_encode(array("SESSION" => $_SESSION)));
                $scope->setExtra("REQUEST", json_encode(array("REQUEST" => $_REQUEST)));
                $scope->setExtra("OBJECT", json_encode(array("OBJECT" => file_get_contents('php://input'))));
                //}
            });
            Sentry\captureException($exception);
        }
    }

    public static function enviarSMS($numero, $mensaje){
        $envio = false;
        try {
            $client = new ClientTwilio(TWILIO_SID, TWILIO_TOKEN);
            $client->messages->create(
                // Where to send a text message (your cell phone?)
                $numero,
                array(
                    'from' => TWILIO_PHONE_NUMBER,
                    'body' => $mensaje
                )
            );
            $envio = true;
        } catch (Exception $e) {
            Utility::capture($e);
        }
        return $envio;
    }

    public static function peticion($url, $metodo, $body = array()){

        $tipo = SUCCESS;
        $mensajes = array();
        $data = array();
        $response = new stdClass();
        try {
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
            ));
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);
            $response = json_decode($resp);
        } catch (Exception $e) {
            $mensajes[] = "Ups, problemas en la petición (02).";
            $tipo = ERROR;
        }

        $data["data"] = $response;
        $data["mensajes"] = $mensajes;
        $data["tipo"] = $tipo;
        return $data;
    }

    public static function setLower($nombre){
        return mb_strtolower($nombre, "UTF-8");
    }

    public static function getNombreMes($mes){

        $txt = "";
        switch ($mes) {
            case '01':
                $txt = "Enero";
                break;
            case '02':
                $txt = "Febrero";
                break;
            case '03':
                $txt = 'Marzo';
                break;
            case '04':
                $txt = 'Abril';
                break;
            case '05':
                $txt = 'Mayo';
                break;
            case '06':
                $txt = 'Junio';
                break;
            case '07':
                $txt = 'Julio';
                break;
            case '08':
                $txt = 'Agosto';
                break;
            case '09':
                $txt = 'Setiembre';
                break;
            case '10':
                $txt = 'Octubre';
                break;
            case '11':
                $txt = 'Noviembre';
                break;
            case '12':
                $txt = 'Diciembre';
                break;
        }
        return $txt;
    }

    public static function getNombreMesCorto($mes){

        $txt = "";
        switch ($mes) {
            case '01':
                $txt = "Enero";
                break;
            case '02':
                $txt = "Febrero";
                break;
            case '03':
                $txt = 'Marzo';
                break;
            case '04':
                $txt = 'Abril';
                break;
            case '05':
                $txt = 'Mayo';
                break;
            case '06':
                $txt = 'Junio';
                break;
            case '07':
                $txt = 'Julio';
                break;
            case '08':
                $txt = 'Agosto';
                break;
            case '09':
                $txt = 'Setiembre';
                break;
            case '10':
                $txt = 'Octubre';
                break;
            case '11':
                $txt = 'Noviembre';
                break;
            case '12':
                $txt = 'Diciembre';
                break;
        }
        if (strlen($txt) > 0) {
            $txt = substr($txt, 0, 3);
        }
        return $txt;
    }

    public static function obtenerFechaLeible($fecha, $mostrarHoras = false){

        if (!isset($fecha) || $fecha == null) return $fecha;

        try {
            $date = strtotime($fecha);

            $mes = Utility::getNombreMes(date('m', $date));
            if ($mostrarHoras) {
                return date('d', $date) . " de " . $mes . " del " . date('Y', $date) . " a las " . date('H:i:s', $date);
            } else {
                return date('d', $date) . " de " . $mes . " del " . date('Y', $date);
            }
        } catch (Exception $err) {
            return $fecha;
        }
    }

    public static function formatearNumeroSimbolo($value, $country_id = PARAM_TODOS, $sindecimales = false){

        $symbol = Security::getCurrentSimboloMoneda();

        switch ($country_id) {

            case PAIS_PERU:

                //Por cambios en la ley de redondeo se debe usar este nuevo metodo que redondea al decimal más bajo (a favor del cliente)

                if ($sindecimales) {
                    return $symbol . " " . number_format($value, 0);
                } else {
                    return $symbol . " " . number_format($value, 2);
                }


            default:

                if ($sindecimales) {
                    return $symbol . " " . number_format($value, 0);
                } else {
                    return $symbol . " " . number_format($value, 2);
                }
        }
    }

    public static function obtenerNombreDiaSemana($day){
        $txt = "";
        switch ($day) {
            case '1':
                $txt = "Lunes";
                break;
            case '2':
                $txt = "Martes";
                break;
            case '3':
                $txt = 'Miercoles';
                break;
            case '4':
                $txt = 'Jueves';
                break;
            case '5':
                $txt = 'Viernes';
                break;
            case '6':
                $txt = 'Sabado';
                break;
            case '7':
                $txt = 'Domingo';
                break;
        }
        return $txt;
    }

    public static function divideTwoNumbers($dividend, $divider) {
        if ($divider > 0) {
            return $dividend / $divider;
        } else {
            return 0;
        }
    }

    public static function formatearNumero($value, $country_id = PARAM_TODOS){

        if ($country_id == PARAM_TODOS) {
            $country_id = Security::getCountry_id();
        }

        switch ($country_id) {

            case PAIS_PERU:

                //Por cambios en la ley de redondeo se debe usar este nuevo metodo que redondea al decimal más bajo (a favor del cliente)
                return number_format($value, 2);

            default:

                return number_format($value, 2);
        }
    }

    /**
     * Retorna true si es un valor positivo en caso contrario o cuando ocurra un error retorna false
     * @param $valor
     * @return bool
     */
    public static function validarEnteroPositivo($valor){
        try {

            if ($valor == null) return false;
            if ($valor == "") return false;

            return (is_numeric($valor) && $valor * 1 > 0);
        } catch (Exception $er) {

            return false;
        }
    }

    public static function getDiferenciaTiempo($fecha1, $fecha2){
        if ($fecha1 == null || !isset($fecha1) || $fecha2 == null || !isset($fecha2)) {
            return "";
        }
        try {
            $fecha1 = new DateTime($fecha1);
            $fecha2 = new DateTime($fecha2);
            if ($fecha2 < $fecha1)
                return "0";
            $fecha = $fecha1->diff($fecha2);
            $tiempo = ($fecha->d * 24 + $fecha->h * 60 + ($fecha->i));
            return $tiempo / 24;
        } catch (Exception $err) {
            return "0";
        }
    }

    public static function getFechaFormateada($stringFecha, $format = 'd-m-Y '){
        if (!isset($stringFecha) || $stringFecha == null) return "";
        try {
            $date = new DateTime($stringFecha);
            return $date->format($format);
        } catch (Exception $err) {
            return "";
        }
    }

    public static function obtenerExtensionArchivo($archivo){
        $extension = strtolower(pathinfo($archivo, PATHINFO_EXTENSION));

        return $extension;
    }

    public static function _group_by($obj, $fldName){
        $groups = array();
        foreach ($obj as $rec) {
            $groups[$rec->$fldName] = $rec;
        }
        return $groups;
    }

    public static function _group_byWithValue($array, $key){
        $return = array();
        foreach ($array as $val) {
            $return[$val->$key][] = $val;
        }
        return $return;
    }

    public static function getNumeroMesByNombre($mes){

        $txt = "";
        switch ($mes) {
            case 'Enero':
                $txt = "01";
                break;
            case 'Febrero':
                $txt = "02";
                break;
            case 'Marzo':
                $txt = '03';
                break;
            case 'Abril':
                $txt = '04';
                break;
            case 'Mayo':
                $txt = '05';
                break;
            case 'Junio':
                $txt = '06';
                break;
            case 'Julio':
                $txt = '07';
                break;
            case 'Agosto':
                $txt = '08';
                break;
            case 'Setiembre':
                $txt = '09';
                break;
            case 'Octubre':
                $txt = '10';
                break;
            case 'Noviembre':
                $txt = '11';
                break;
            case 'Diciembre':
                $txt = '12';
                break;
        }
        return $txt;
    }

    public static function _enviarEmail($nombreDestinatario, $emailDestinatario, $asunto, $text_body, $html_body){
        $resultado = false;
        /**
         * Comentado por error 502 en servidor
         */
        //return $resultado;
        /* $mail = new PHPMailer;
            $mail->CharSet = 'UTF-8';

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup server
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'antonycotos@gmail.com';                            // SMTP username
            $mail->Password = 'Acotosy_9703';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
            $mail->Port = 587;

            $mail->From = 'antonycotos@gmail.com';
            $mail->FromName = 'Peticíon de Programacion ' . date("Y-m-d H:i:s"); */
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = '';  // Specify main and backup server
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = '';
        // SMTP username
        $mail->Password = '';
        $mail->SMTPSecure = '';                            // Enable encryption, 'ssl' also accepted
        $mail->Port = 587;

        $mail->From = '';
        $mail->FromName = $asunto;

        $mail->addAddress($emailDestinatario, $nombreDestinatario);  // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        //$mail->addReplyTo('notificaciones@kirudental.com', 'KiruDental');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        // $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $asunto;
        $mail->Body = $html_body;
        $mail->AltBody = $text_body;

        $resultado = @$mail->send();

        return $resultado;
    }

    public static function fecha1EsMenorFecha2($f1, $f2){
        try {
            $fecha1 = new DateTime($f1);
            $fecha2 = new DateTime($f2);
            if ($fecha1 <= $fecha2) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $err) {
            return false;
        }
    }

    public static function fechaAesMayorFechab($fechaa, $fechab = null, $mayorOIgual = true){

        try {

            if ($fechab == null) {
                $fechab = date("d-m-Y H:i:00");
            }

            //$fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
            //$fecha_entrada = strtotime("19-11-2008 21:00:00");

            $fecha_a = strtotime(date($fechaa));
            $fecha_b = strtotime($fechab);

            if ($fecha_a > $fecha_b) {
                return true;
            } else if ($mayorOIgual) {

                if ($fecha_a >= $fecha_b) {
                    return true;
                }
            }

            return false;
        } catch (Exception $er) {
            Utility::capture($er);
        }

        return true;
    }

    public static function getTxtEvidenciaPlanAccionEsDocumentoOImagen($url){
        $tipo = "image";
        if (Utility::stringContiene($url, 'pdf')) {
            $tipo = "pdf";
        } else if (Utility::stringContiene($url, 'rar')) {
            $tipo = "rar";
        } else if (Utility::stringContiene($url, 'zip')) {
            $tipo = "zip";
        }
        return $tipo;
    }

    public static function evidenciaPlanAccionEsImagen($url){

        return Utility::getTxtEvidenciaPlanAccionEsDocumentoOImagen($url) == 'image' ? true : false;
    }

    public static function trimestre($datetime){
        $mes = date("m", strtotime($datetime));
        $mes = is_null($mes) ? date('m') : $mes;
        $trim = floor(($mes - 1) / 3) + 1;
        return $trim;
    }

    public static function base64ToImage($imageData){
        // $data = 'data:image/png;base64,AAAFBfj42Pj4';
        list($type, $imageData) = explode(';', $imageData);
        list(, $extension) = explode('/', $type);
        list(, $imageData)      = explode(',', $imageData);
        $fileName = uniqid() . '.' . $extension;
        $imageData = base64_decode($imageData);
        file_put_contents($fileName, $imageData);
    }

    public static function snakeToCamel($str){
        // Remove underscores, capitalize words, squash, lowercase first.
        $i = array("-", "_");
        $str = preg_replace('/([a-z])([A-Z])/', "\\1 \\2", $str);
        $str = preg_replace('@[^a-zA-Z0-9\-_ ]+@', '', $str);
        $str = str_replace($i, ' ', $str);
        $str = str_replace(' ', '', ucwords(strtolower($str)));
        $str = strtolower(substr($str, 0, 1)) . substr($str, 1);
        return $str;
    }

    public static function camelCaseIam($str) {
        // Remove underscores, capitalize words, squash, lowercase first.
        if ($str == null || strlen($str) == 0) {
            return $str;
        }

        $palabras = explode(" ", $str);

        $nuevaspalabras = array();

        foreach ($palabras as $value) {
            if (strlen(trim($value)) > 0) {
                $value = ucfirst(strtolower($value));
                $nuevaspalabras[] = $value;
            }
        }

        //return ucfirst(strtolower($str));
        return implode(" ", $nuevaspalabras);
        //return $nuevaspalabras;
    }

    public static function getFechaAnioMesFormatoEjecucionReporte($fecha){
        try {

            return Utility::getFechaSegunFormato($fecha, "Y") . " / " . Utility::getFechaSegunFormato($fecha, "m");
        } catch (Exception $er) {
            return "";
        }
    }

    public static function getFechaFormatoEjecucionReporte($fecha){
        try {

            return Utility::getFechaSegunFormato($fecha, "d-m-Y");
        } catch (Exception $er) {
            return "";
        }
    }

    public static function  minutosASegundos($minutos){
        return $minutos * 60;
    }

    public static function convertirTiempo($valor, $minutosSegundos){
        if ($minutosSegundos === 0) {
            $segundos = $valor * 60;
        } elseif ($minutosSegundos === 1) {
            $segundos = $valor;
        } else {
            return "Opción no válida para minutosSegundos.";
        }

        $horas = floor($segundos / 3600);
        $segundos %= 3600;
        $minutos = floor($segundos / 60);
        $segundos %= 60;

        $resultado = "";

        if ($horas == 1) {
            $resultado .= $horas . " hora ";
        } elseif ($horas > 1) {
            $resultado .= $horas . " horas ";
        }

        if ($minutos == 1) {
            $resultado .= $minutos . " min ";
        } elseif ($minutos > 1) {
            $resultado .= $minutos . " mins ";
        }

        if ($segundos == 1) {
            $resultado .= $segundos . " seg ";
        } elseif ($segundos > 1) {
            $resultado .= $segundos . " segs ";
        }

        return trim($resultado);
    }

    public static function getYoutubeVideoId($url){
        $videoId = parse_url($url, PHP_URL_QUERY);
        parse_str($videoId, $videoIdArray);

        if (isset($videoIdArray['v'])) {
            return $videoIdArray['v'];
        } else {
            return "";
        }
    }

    public static function createSlug($str, $delimiter = '-'){
        if ($str != null && $str != '') {
            $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
            return $slug;
        }
        return "";
    }

    public static function getClient(){
        if (Utility::$client == null) {
            Utility::$client = new GuzzleHttp\Client([
                'curl' => [
                    CURLOPT_RESOLVE => [
                        '1.1.1.1'
                    ],
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ],
            ]);
        }
        return Utility::$client;
    }

    public static function peticionPublica($url, $metodo, $body = array(), $tiempoEspera = -1){

        $tipo = SUCCESS;
        $mensajes = array();
        $data = array();

        try {
            $timeout = 0;
            $connectTimeout = 30;
            if ($tiempoEspera != -1) {
                $timeout = 60;
                $connectTimeout = 30;
            }
            $body = json_encode($body);

            $headers = [
                'Content-Type'  => 'application/json',
            ];

            $client = Utility::getClient();

            if ($metodo == "POST") {
                $response = $client->post($url, [
                    'headers' => $headers,
                    "body" => $body,
                    'timeout' => $timeout, // Response timeout
                    'connect_timeout' => $connectTimeout, // Connection timeout
                ]);
                $response = $response->getBody();
            } else {
                $response = $client->request('GET', $url, ['timeout' => $timeout, 'connect_timeout' => $connectTimeout]);
                $response = $response->getBody();
            }


            $response = json_decode($response, true);

            $data = $response;

            if (!isset($data["tipo"])) {
                $mensajes[] = "Ups, problemas en la petición (01).";
                $tipo = ERROR;
            } else {
                $tipo = $data["tipo"];
                if (!isset($data["mensajes"])) {

                    $mensajes[] = "Ups, problemas en la petición (03).";
                } else {

                    if (is_array($data["mensajes"])) {

                        $mensajes = $data["mensajes"];
                    } else {

                        $mensajes[] = $data["mensajes"];
                    }
                }
            }
        } catch (Exception $e) {
            $mensajes[] = "Ups, problemas en la petición (02).";
            $tipo = ERROR;
            Utility::capture($e);
        }

        if ($tipo == ERROR) {
            $data["data"] = new stdClass();
            $data["mensajes"] = $mensajes;
            $data["tipo"] = $tipo;
        }

        return $data;
    }

    public static function obtenerCategoriaList(){
        $categoriaList = array();
        $categoriaList[] = (Object) array('categoria_id' => 1, 'categoria_nombre' => 'Comida');
        // $categoriaList[] = (Object) array('category_id' => 1, 'name' => 'Comida');
        return $categoriaList;
    }

    public static function obtenerCateriaByID($categoria_id){
        $categoriaList = Utility::obtenerCategoriaList();
        $categoria = null;
        foreach ($categoriaList as $value) {
            if ($value->categoria_id == $categoria_id) {
                $categoria = $value;
                break;
            }
        }
        return $categoria;
    }


    /**
     * Retorna un objeto Categoria con el formato de yape para la integracion
     * @param  { categoria_id : string , categoria_nombre : string };
     * @return { category_id : string , name : string } : Category;
     */
    public static function obtenerCategoriaYape($categoria){
        $categoriaYape = (Object) array('category_id' => null, 'name' => null);
        if($categoria){
            $categoriaYape->category_id = $categoria->categoria_id;
            $categoriaYape->name = $categoria->categoria_nombre;
        }else{
            return null;
        };
    }

    /**
    * Retorna un objeto Local con el formato de yape para la integracion
    * @param  { local_id : string , local_nombre : string } : local;
    * @return { company_id : string , name : string , imgUrl : string } : Company;
    */
    public static function obtenerLocalYape($local){
        $companyYape = (Object) array('company_id' => null, 'name' => null , 'imgUrl' => null);
        if($local){
            $companyYape->company_id = $local->local_id;
            $companyYape->name = $local->local_nombre;
            $companyYape->imgUrl = $local->local_logo;
        }else{
            return null;
        };
    }

    /**
    * Retorna un objeto Product con el formato de yape para la integracion
    * @param  { producto_id : string , producto_presentacion : string } : producto;
    * @return { product_id : string , promotion_price : float , original_price : float , commission : float } : product;
    */
    public static function obtenerProductoYape($producto){
        $productoYape = (Object) array('product_id' => null, 'promotion_price' => null , 'original_price' => null , 'commission' => null);
        if($producto){
            $productoYape->product_id = $producto->producto_id;
            $productoYape->original_price = $producto->producto_precio;
            $productoYape->promotion_price = $producto->producto_preciopromocion;
            $productoYape->commission = $producto->producto_comision;
        }else{
            return null;
        };
    }

    /**
    * Retorna un objeto Promotion con el formato de yape para la integracion
    * @param  { promotion_id : string , local_id : string , categoria_id : string } : Promotion;
    * @return { promotion_id : string , category : category , company : company } : PromotionYape;
    */
    public static function obtenerPromotionYape($promotion , $productos){
        $promotionYape = (Object) array(
            'promotion_id' => null      , // string - "e79a0b74-3aba-4149-9f74-0bb5791a"
            'category'     => null      , // Category
            'company'      => null      , // Company
            'title'        => null      , // string - "Cine 2x1"
            'description'  => null      , // string - "Cine 2x1"
            'imgUrl'       => null      , // string - "https://test.jpg"
            'products'     => []        , // Product[]
            'terms_conditions' => null  , // string - "Términos y condicones"
            'start_date'     => null    , // string - "2024-03-14T00:00:00.000Z"            
            'end_date'       => null    , // string - "2024-03-15T00:00:00.000Z"
            'redemption_description'    => null      , // string - "Pasos para canjear tu promo"            
            'total_stock'               => null      , // number - 100
            'stock_user'                => null      , // number - 1
            'expiration_date'           => null      , // string - "2024-03-20T00:00:00.000Z"
        );

        if(isset($promotion) && isset($productos) && is_array($productos) && sizeof($productos) > 0){

            $promotionYape->promotion_id = $promotion->promotion_id;
            $promotionYape->category = Utility::obtenerCategoriaYape($promotion->categoria);
            $promotionYape->company = Utility::obtenerLocalYape($promotion->local);
            $promotionYape->title = $promotion->title;
            $promotionYape->description = $promotion->description;
            $promotionYape->imgUrl = $promotion->imageUrl;
            $promotionYape->terms_conditions = $promotion->terms_conditions;
            $promotionYape->start_date = $promotion->start_date;
            $promotionYape->end_date = $promotion->end_date;
            $promotionYape->redemption_description = $promotion->redemption_description;
            $promotionYape->total_stock = $promotion->total_stock;
            $promotionYape->stock_user = $promotion->stock_user;
            $promotionYape->expiration_date = $promotion->expiration_date;

            foreach ($productos as $producto) {
                $promotionYape->products[] = Utility::obtenerProductoYape($producto);
            };

        }else{
            return null;
        };
    }

    /* PETICIONES CURL A YAPE */

    // Obtenemos token
    public static function getTokenYape(){
        $url = yp_url;
        $client_id = yp_client_id;
        $client_secret = yp_client_secret;
        $data = array();

        $data = array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'scope' => 'eventgateway',
            'grant_type' => 'client_credentials'
        );

        try {

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "/v1/token",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded"
                ),
            ));

            $response = curl_exec($curl);
            $data = null;
            try{
                $data = json_decode($response, true);
            } catch (Exception $e) {
                $data = null;
            }

                 
            curl_close($curl);
            if( $data == null ){
                $data = base64_encode($response);
            }

        }catch (Exception $e) {
            $mensajes[] = "Ups, problemas en la petición (02).";
            $tipo = ERROR;
            Utility::$logs[] = $e;
        }

        if ($tipo == ERROR) {
            $data["data"] = new stdClass();
            $data["mensajes"] = $mensajes;
            $data["tipo"] = $tipo;
        }

        return $data;
    }

    // Creamos promocion
    public static function createPromotionYape($token , $event_gateway ,$agregator , $data){

        $url = "http://{$event_gateway}/v1/events/{$agregator}/create";
        $data = array();  

        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$token}",
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $data = null;
            try{
                $data = json_decode($response, true);
            } catch (Exception $e) {
                $data = null;
            }

                 
            curl_close($curl);
            if( $data == null ){
                $data = base64_encode($response);
            }

        }catch (Exception $e) {
            $mensajes[] = "Ups, problemas en la petición (02).";
            $tipo = ERROR;
            Utility::$logs[] = $e;
        }

        if ($tipo == ERROR) {
            $data["data"] = new stdClass();
            $data["mensajes"] = $mensajes;
            $data["tipo"] = $tipo;
        }

        return $data;

    }

    // Actualizamos promocion
    public static function updatePromotionYape($token , $event_gateway ,$agregator , $data){
        $url = "http://{$event_gateway}/v1/events/{$agregator}/update";
        $data = array();  

        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$token}",
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $data = null;
            try{
                $data = json_decode($response, true);
            } catch (Exception $e) {
                $data = null;
            }

                 
            curl_close($curl);
            if( $data == null ){
                $data = base64_encode($response);
            }

        }catch (Exception $e) {
            $mensajes[] = "Ups, problemas en la petición (02).";
            $tipo = ERROR;
            Utility::$logs[] = $e;
        }

        if ($tipo == ERROR) {
            $data["data"] = new stdClass();
            $data["mensajes"] = $mensajes;
            $data["tipo"] = $tipo;
        }

        return $data;

    }

    // Eliminamos promocion
    public static function deletePromotionYape($token , $event_gateway ,$agregator , $data){
        $url = "http://{$event_gateway}/v1/events/{$agregator}/disable";
        $data = array();  

        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$token}",
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $data = null;
            try{
                $data = json_decode($response, true);
            } catch (Exception $e) {
                $data = null;
            }

                 
            curl_close($curl);
            if( $data == null ){
                $data = base64_encode($response);
            }

        }catch (Exception $e) {
            $mensajes[] = "Ups, problemas en la petición (02).";
            $tipo = ERROR;
            Utility::$logs[] = $e;
        }

        if ($tipo == ERROR) {
            $data["data"] = new stdClass();
            $data["mensajes"] = $mensajes;
            $data["tipo"] = $tipo;
        }

        return $data;

    }

}
