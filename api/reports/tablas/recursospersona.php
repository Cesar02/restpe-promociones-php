<?php
	//Agregamos la libreria para genera códigos QR
	require './phpqrcode/qrlib.php';
	
	$tipoRecurso = -1;
	$recursoId = -1;
	$value = null;
	if(isset($_GET["recursoId"])){
		$recursoId = $_GET["recursoId"];
		$value = Persona::getById($recursoId);
	}

	//Declaramos una carpeta temporal para guardar la imagenes generadas
	$dir = 'temp/';
	
	//Si no existe la carpeta la creamos
	if (!file_exists($dir))
        mkdir($dir);

	//Parametros de Condiguración
	$tamaño = 10; //Tamaño de Pixel
	$level = 'L'; //Precisión Baja
	$framSize = 3; //Tamaño en blanco
	$contenidoBase = "/recurso/"; //Texto
	
	if($value!=false && $value!=null){

		$empresa = null;
		$concesion = null;
		$etapa = null;

		if(isset($value->empresa_id) && $value->empresa_id>0){
			$empresa = Empresa::getById($value->empresa_id);

			$etapa = Etapa::getById($empresa->etapa_id);

			$concesion = Concesion::getById($etapa->concesion_id);
			
		}
		else if(isset($value->unidadtrabajo_id) && $value->unidadtrabajo_id>0){
			$value->unidadtrabajo = Unidadtrabajo::getById($value->unidadtrabajo_id);

			$proceso = Proceso::getById($value->unidadtrabajo->proceso_id);
			
			$area = Area::getById($proceso->area_id);

			$empresa = Empresa::getById($area->empresa_id);
			
			$etapa = Etapa::getById($empresa->etapa_id);

			$concesion = Concesion::getById($etapa->concesion_id);

		}
		$filename = $dir.'recursopersona'.$value->persona_id.'.png';
		$contenido = $contenidoBase.TRV_PERSONAS."/".$value->persona_id;
		echo '<div class="col-md-12" style="text-align: center;">';
		//Enviamos los parametros a la Función para generar código QR 
		QRcode::png($contenido, $filename, $level, $tamaño, $framSize); 
		//Mostramos la imagen generada
		echo '<img src="'.$dir.basename($filename).'">';
		echo '<br>';
		echo '<div style="text-align: center;">';
			echo '<span class="text-center">'.$value->persona_nombres.' '.$value->persona_apellidopaterno.' '.$value->persona_apellidomaterno.'</span>';
			echo '<br>';
			if($concesion!=null){
				echo '<span>Concesion: '.$concesion->concesion_nombre.'</span>';
				echo '<br>';
			}
			if($etapa!=null){
				echo '<span>Proyecto: '.$etapa->etapa_nombre.'</span>';
				echo '<br>';
			}
			if($empresa!=null){
				echo '<span>Empresa: '.$empresa->empresa_nombre.'</span>';
			}
		echo '</div>';
	
		echo '</div>';
	
	}

	
?>
