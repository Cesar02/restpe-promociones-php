<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/
$empresaId = PARAM_TODOS;

if(isset($_GET["empresa_id"])){
    $empresaId = $_GET["empresa_id"];
}

$docente = new PersonaController();
$array = $docente->listarPorPaginacion(0,0,'-1','-1','-1',$empresaId,NULL);
$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
		<th style="text-align: center">N°</th>
		<th style="text-align: center">Ap. Paterno</th>
		<th style="text-align: center">Ap. Materno</th>
		<th style="text-align: center">Nombres</th>
		<th style="text-align: center">DNI</th>		
		<!--th style="text-align: center">Cargo</th-->			
		<th style="text-align: center">Fecha Nacimiento</th>			
		<th style="text-align: center">Sexo</th>			
		<th style="text-align: center">Fecha Ingreso Aunor</th>			
		<th style="text-align: center">Fecha Ingreso Puesto Trabajo</th>			
		<th style="text-align: center">Acceso Sistema</th>			
		<th style="text-align: center">Email Acceso</th>			
		<th style="text-align: center">Clave</th>			
		<th style="text-align: center">Puesto Trabajo</th>
		<th style="text-align: center">Concesion </th>			
		<th style="text-align: center">Proyecto</th>			
		<th style="text-align: center">Empresa </th>			
		<th style="text-align: center">Gerencia</th>			
		<th style="text-align: center">Area </th>			
		<th style="text-align: center">Unidad Trabajo</th>			
		<th style="text-align: center">Estado</th>	
		<th style="text-align: center">Borrado</th>			
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {

		$unidadtrabajo_nombre = "";
		
		$proceso_nombre = "";

		$area_id = "";

		$area_nombre = "";

		$empresa_nombre = "";

		$etapa_nombre = "";

		$concesion_nombre = "";

		$empresa_id = null;


		
		if(isset($item->unidadtrabajo_id)){
			
			$unidadTrabajoObj = Unidadtrabajo::getById($item->unidadtrabajo_id);
			
			if(isset($unidadTrabajoObj->unidadtrabajo_id)){
				
				$unidadtrabajo_nombre = $unidadTrabajoObj->unidadtrabajo_nombre;

				if(isset($unidadTrabajoObj->proceso_id)){
					
					$procesoObj = Proceso::getById($unidadTrabajoObj->proceso_id);
					
					if(isset($procesoObj->proceso_id)){
						
						$proceso_nombre = $procesoObj->proceso_nombre;

						if(isset($procesoObj->area_id)){
							
							$areaObj = Area::getById($procesoObj->area_id);

							if(isset($areaObj->area_id)){

								$area_nombre = $areaObj->area_nombre;

								$empresaObj = Empresa::getById($areaObj->empresa_id);
								
								if(isset($empresaObj->empresa_id)){

									$empresa_nombre = $empresaObj->empresa_nombre;
	
									$etapaObj = Etapa::getById($empresaObj->etapa_id);
									
									$etapa_nombre = $etapaObj->etapa_nombre;

									$concesionObj = Concesion::getById($etapaObj->concesion_id);

									$concesion_nombre = $concesionObj->concesion_nombre;
	
								}

							}

						}
					}
				}
			}
		}
		
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			echo '<td class="td_css">'.++$con.'</td>';
			echo '<td class="td_css">'.$item->persona_apellidopaterno.' '.'</td>';
			echo '<td class="td_css">'.$item->persona_apellidomaterno.'</td>';
			echo '<td class="td_css">'.$item->persona_nombres.'</td>';

			echo '<td class="td_css">'.$item->persona_dni.'</td>';
			//echo '<td class="td_css">'.$item->cargo->cargo_descripcion.'</td>';
			echo '<td class="td_css">'.$item->persona_fechanacimiento.'</td>';
			echo '<td class="td_css">'.$item->persona_sexo.'</td>';
			echo '<td class="td_css">'.$item->persona_fechaingresoaunor.'</td>';
			echo '<td class="td_css">'.$item->persona_fechaingresopuestotrabajo.'</td>';
			echo '<td class="td_css">';
				if($item->persona_accesosistema==='1'){
					echo 'Si';
				}else{
					echo 'No';
				}
			echo '</td>';
			echo '<td class="td_css">'.$item->persona_emailacceso.'</td>';
			echo '<td class="td_css">'.$item->persona_clave.'</td>';
			echo '<td class="td_css">'.$item->puestotrabajo->puestotrabajo_nombre.'</td>';
			
			echo '<td class="td_css">';	
			echo  $concesion_nombre;
			echo '</td>';
			
			echo '<td class="td_css">';	
			echo  $etapa_nombre;
			echo '</td>';
			
			echo '<td class="td_css">';	
			echo  $empresa_nombre;
			echo '</td>';
			
			echo '<td class="td_css">';	
			echo  $area_nombre;
			echo '</td>';
			
			echo '<td class="td_css">';
			echo  $proceso_nombre;
			echo '</td>';	
			
			echo '<td class="td_css">';	
			echo  $unidadtrabajo_nombre;
			echo '</td>';
			
			echo '<td class="td_css">';
					if ($item->persona_estado==='1'){
						echo "ACTIVO";
					}else{
						echo "INACTIVO";
					}
			echo '</td>';
			echo '<td class="td_css">';
					if ($item->persona_isdelete==='1'){
						echo "ELIMINADO";
					}else{
						echo "NO ELIMINADO";
					}
			echo '</td>';
        

        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="15">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>