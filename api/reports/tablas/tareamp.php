<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/

$actividadmptareamp = new ActividadmptareampController();

$procesoactividadmp_id = PARAM_TODOS;
$esriesgo = "0";

if(isset($_GET["procesoactividadmp_id"]) && is_numeric($_GET["procesoactividadmp_id"]) && intval($_GET["procesoactividadmp_id"]) > 0){
    $procesoactividadmp_id = $_GET["procesoactividadmp_id"];
}
if(isset($_GET["esriesgo"]) && is_numeric($_GET["esriesgo"]) && intval($_GET["esriesgo"]) > 0){
    $esriesgo = $_GET["esriesgo"];
}
//$array = array("lista"=>array());
if($esriesgo == "1"){
    $array = $actividadmptareamp->listarPorPaginacionRiesgo(1,2000,'-1',$procesoactividadmp_id);
}
if($esriesgo == "0"){
    $array = $actividadmptareamp->listarPorPaginacion(1,2000,'-1',$procesoactividadmp_id);
}
$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>
<div class="col-12">
<img class="img img-responsive" src="http://aunor.portafolioitdast.com/aunor/assets/images/logo-light.png" alt="logo">
</div>
<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
<tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>    
<tr>
		<td class="td_css">N°</td>
		<td class="td_css">ID</td>
		<td class="td_css">Tarea</td>		
		<td class="td_css">Descripcion</td>		
        <td class="td_css">Orden</td>
        <?php
        if($esriesgo=="1"){
            echo "<td class='td_css'>Riesgo</td>";
        }
        ?>
    </tr>
    <tbody>
    <?php
    $con=0;$sw=0;
    if(isset($List) && sizeof($List)>0){
        foreach ($List as $item) {
        
            $color = "#FFF";
            if($sw==0){
                $sw=1;
            }else{
                $color = "#F7F7F7";
                $sw=0;
            }
    
            echo '<tr style="background:'.$color.'">';
                echo '<td class="td_css">'.++$con.'</td>';
                echo '<td class="td_css">'.$item->actividadmptareamp_id.'</td>';
                echo '<td class="td_css">';
                    if (isset($item->tareamp) && isset($item->tareamp->tareamp_nombre)){
                        echo $item->tareamp->tareamp_nombre;
                    }else{
                        echo "";
                    }
                echo '</td>';
    
                echo '<td class="td_css">';
                    if (isset($item->tareamp) && isset($item->tareamp->tareamp_descripcion)){
                        echo $item->tareamp->tareamp_descripcion;
                    }else{
                        echo "";
                    }
                echo '</td>';
    
                echo '<td class="td_css">'.$item->actividadmptareamp_orden.'</td>';
    
                echo '<td class="td_css">';
                    if (isset($item->riesgos) && is_array($item->riesgos) && sizeof($item->riesgos)>0){
                        foreach($item->riesgos as $value){
                            echo "".$value->riesgo_nombre." <br>";    
                        }
                    }else{
                        echo "";
                    }
                echo '</td>';
            
    
            echo '</tr>';
        }
    }
    if (isset($List) && sizeof($List)==0){
        echo '<tr><th colspan="3">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>