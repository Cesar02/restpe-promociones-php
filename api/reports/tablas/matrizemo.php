<?php

$ctrl = new ProgramacionemopersonaresultadoController();

$objParams = array();

if(isset($_GET["empresa_id"])){
    $objParams["empresa_id"] = $_GET["empresa_id"];
}

if(isset($_GET["puestotrabajo_id"])){
    $objParams["puestotrabajo_id"] = $_GET["puestotrabajo_id"];
}

if(isset($_GET["clinica_id"])){
    $objParams["clinica_id"] = $_GET["clinica_id"];
}

if(isset($_GET["clinicasede_id"])){
    $objParams["clinicasede_id"] = $_GET["clinicasede_id"];
}

if(isset($_GET["tipoexamenocupacional_id"])){
    $objParams["tipoexamenocupacional_id"] = $_GET["tipoexamenocupacional_id"];
}

if(isset($_GET["fechainicio"])){
    $objParams["fechainicio"] = $_GET["fechainicio"];
}

if(isset($_GET["fechafin"])){
    $objParams["fechafin"] = $_GET["fechafin"];
}


$array = $ctrl->postPersonasConEvidenciaYSeaValidadOOberservadaByClinicaSedeProtocoloUsuario(0,0,$objParams);

$List =  $array["lista"];

$atributoList = array();
		
$examenesList =  Examenso::getByFields(
    array(
        array('field' => 'examenso_isdelete', 'value' =>NO, 'operator' => '='),
        array('field' => 'examenso_estado', 'value' =>ACTIVO, 'operator' => '=')
    ),
    array(
        array('field' => 'examenso_orden', 'order' => 'asc')
    )
)["examenso_array"];

array_multisort(array_column($examenesList, 'examenso_orden'), SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $examenesList);

foreach($examenesList as $eso){
    $eso->atributoList = Atributoexamenso::getByFields(array(
        array('field' => 'atributoexamenso_isdelete', 'value' =>NO, 'operator' => '='),
        array('field' => 'atributoexamenso_estado', 'value' =>ACTIVO, 'operator' => '='),
        array('field' => 'examenso_id', 'value' =>$eso->examenso_id, 'operator' => '=')
    ))['atributoexamenso_array'];

    array_multisort(array_column($eso->atributoList, 'atributoexamenso_orden'), SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $eso->atributoList);

    
    foreach($eso->atributoList as $attr){
        $atributoList[] = $attr;
    }

}

$cantidadCampos = sizeof($atributoList);

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
            <th>#</th>
            <th>Codigo</th>
            <th>Apellido y Nombres</th>
            <th>Empresa</th>
            <th>Puesto</th>
            <th>Tipo Examen</th>
            <th>Protoclo</th>
            <th>Fecha Atencion</th>
            <th>Clinica</th>
            <th>Sede</th>

            <?php
            foreach($atributoList as $attr){
                echo "<th>".$attr->atributoexamenso_nombre."</th>";
            }
            ?>
            
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			echo '<td class="td_css">'.++$con.'</td>';
			
            echo '<td class="td_css">';
            echo $item->programacionemopersona_id;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->persona_fullname;
            echo '</td>';

            echo '<td class="td_css">';
            if($item->empresa->empresa_nombre){
                echo $item->empresa->empresa_nombre;
            }else{
                echo "";
            }
            echo '</td>';

            echo '<td class="td_css">';
            if($item->puestotrabajo->puestotrabajo_nombre){
                echo $item->puestotrabajo->puestotrabajo_nombre;
            }else{
                echo "";
            }
            echo '</td>';

            echo '<td class="td_css">';
            if($item->tipoexamenocupacional_nombre){
                echo $item->tipoexamenocupacional_nombre;
            }else{
                echo "";
            }
            echo '</td>';


            echo '<td class="td_css">';
            if($item->protocolo_nombre){
                echo $item->protocolo_nombre;
            }else{
                echo "";
            }
            echo '</td>';


            echo '<td class="td_css">';
            if($item->programacionemopersonaresultado_fecharegistrosistema){
                echo $item->programacionemopersonaresultado_fecharegistrosistema;
            }else{
                echo "";
            }
            echo '</td>';

            echo '<td class="td_css">';
            if($item->clinica->clinica_nombre){
                echo $item->clinica->clinica_nombre;
            }else{
                echo "";
            }
            echo '</td>';

            echo '<td class="td_css">';
            if($item->clinicasede->clinicasede_nombre){
                echo $item->clinicasede->clinicasede_nombre;
            }else{
                echo "";
            }
            echo '</td>';

            foreach($atributoList as $attr){
                echo '<td class="td_css">';
                echo "";
                echo '</td>';
            }

        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="4">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>