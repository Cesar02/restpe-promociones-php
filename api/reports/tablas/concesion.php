<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/

$docente = new ConcesionController();
$array_herramienta = $docente->listarPorPaginacion(0,0,'-1');
$List = $array_herramienta["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
		<th style="text-align: center">N°</th>
		<th style="text-align: center">Concesion</th>
		<th style="text-align: center">Pais</th>
		<th style="text-align: center">Persona</th>	
		<th style="text-align: center">Estado</th>			
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			echo '<td class="td_css">'.++$con.'</td>';
			echo '<td class="td_css">'.$item->concesion_nombre.'</td>';
			echo '<td class="td_css">'.$item->concesion_pais.'</td>';
			echo '<td class="td_css">'.$item->persona->persona_nombres.' '.
									   $item->persona->persona_apellidopaterno.' '.
									   $item->persona->persona_apellidomaterno.
									   '</td>';
			echo '<td class="td_css">';
					if ($item->concesion_estado==='1'){
						echo "ACTIVO";
					}else{
						echo "INACTIVO";
					}
			echo '</td>';
        

        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="5">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>