<?php
	//Agregamos la libreria para genera códigos QR
	require './phpqrcode/qrlib.php';
	
	$tipoRecurso = -1;
	$recursoId = -1;
	$value = null;
	if(isset($_GET["recursoId"])){
		$recursoId = $_GET["recursoId"];
		$value = Unidadtrabajo::getById($recursoId);
	}

	//Declaramos una carpeta temporal para guardar la imagenes generadas
	$dir = 'temp/';
	
	//Si no existe la carpeta la creamos
	if (!file_exists($dir))
        mkdir($dir);

	//Parametros de Condiguración
	$tamaño = 10; //Tamaño de Pixel
	$level = 'L'; //Precisión Baja
	$framSize = 3; //Tamaño en blanco
	$contenidoBase = "/recurso/"; //Texto
	
	if($value!=false && $value!=null){

		$proceso = Proceso::getById($value->proceso_id);

		$area = Area::getById($proceso->area_id);

		$empresa = Empresa::getById($area->empresa_id);
			
		$etapa = Etapa::getById($empresa->etapa_id);

		$concesion = Concesion::getById($etapa->concesion_id);

		
		$filename = $dir.'recursounidadtrabajo'.$value->unidadtrabajo_id.'.png';
		$contenido = $contenidoBase.TRV_AREA."/".$value->unidadtrabajo_id;
		echo '<div class="col-md-12" style="text-align: center;">';
		//Enviamos los parametros a la Función para generar código QR 
		QRcode::png($contenido, $filename, $level, $tamaño, $framSize); 
		//Mostramos la imagen generada
		echo '<img src="'.$dir.basename($filename).'">';
		echo '<br>';
		echo '<div style="text-align: center;">';
			echo '<span class="text-center">UT: '.$value->unidadtrabajo_nombre.'</span>';
			echo '<br>';

			if($area!=false && $area!=null){
				echo '<span>Area: '.$area->area_nombre.'</span>';
				echo '<br>';
			}
			if($concesion!=false && $concesion!=null){
				echo '<span>Concesion: '.$concesion->concesion_nombre.'</span>';
				echo '<br>';
			}
			if($etapa!=false && $etapa!=null){
				echo '<span>Proyecto: '.$etapa->etapa_nombre.'</span>';
				echo '<br>';
			}
			if($empresa!=false && $empresa!=null){
				echo '<span>Empresa: '.$empresa->empresa_nombre.'</span>';
			}
			/* if($area!=false && $area!=null){
				echo '<span>Gerencia: '.$area->area_nombre.'</span>';
			} */
		echo '</div>';
	
		echo '</div>';
	
	}

	
?>
