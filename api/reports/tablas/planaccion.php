<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/

$docente = new PlanaccionController();
$obj = new stdClass();
$array = $docente->listarPorPaginacion(0,0,'-1','-1');
$List =  $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
            <th>#</th>
            <th>Lista Verificacion</th>
                  <th>Item</th>
                  <th>Descripcion</th>
                  <th>Accion</th>
                  <th>Observacion</th>
                  <th>Persona</th>
                  <th>Plazo</th>
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			echo '<td class="td_css">'.++$con.'</td>';
			echo '<td class="td_css">';
            if($item->listaverificacion->listaverificacion_id){
                echo $item->listaverificacion->listaverificacion_id." ".$item->listaverificacion->listaverificacion_nombre;
            }else{
                echo "";
            }
            echo '</td>';
            echo '<td class="td_css">';
            if($item->listaverificacionitem->listaverificacionitem_id){
                echo $item->listaverificacionitem->listaverificacionitem_id." ".$item->listaverificacionitem->listaverificacionitem_descripcion;
            }else{
                echo "";
            }
            echo '</td>';
            echo '<td class="td_css">'.$item->planaccion_descripcionnc.'</td>';
            echo '<td class="td_css">'.$item->planaccion_accion.'</td>';
            echo '<td class="td_css">'.$item->planaccion_observacion.'</td>';
            echo '<td class="td_css">'.$item->persona->persona_nombres." ".$item->persona->persona_apellidopaterno." ".$item->persona->persona_apellidomaterno.'</td>';
            echo '<td class="td_css">'.$item->planaccion_plazo.'</td>';
        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="4">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>