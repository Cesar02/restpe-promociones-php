<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/

$procesoactividadmp = new ProcesoactividadmpController();

$procesoactividadmp_id = PARAM_TODOS;
$esriesgo = "0";
$empresaId = PARAM_TODOS;
$estadoactividad = PARAM_TODOS;
$tipeView = '1';
$actividadId = PARAM_TODOS;
$esriesgo = "0";
if(isset($_GET["estado"]) && is_numeric($_GET["estado"]) && intval($_GET["estado"]) > 0){
    $estadoactividad = $_GET["estado"];
}

if(isset($_GET["empresa_id"]) && is_numeric($_GET["empresa_id"]) && intval($_GET["empresa_id"]) > 0){
    $empresaId = $_GET["empresa_id"];
}

if(isset($_GET["typeView"]) && is_numeric($_GET["typeView"]) && intval($_GET["typeView"]) > 0){
    $tipeView = $_GET["typeView"];
}

if(isset($_GET["actividadId"]) && is_numeric($_GET["actividadId"]) && intval($_GET["actividadId"]) > 0){
    $actividadId = $_GET["actividadId"];
}

$array = $procesoactividadmp->listarPorPaginacion(1,2000,'-1',$empresaId,$estadoactividad,$actividadId);
$List = $array["lista"];


?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>
<!-- <table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
<tr>
    <img src="../../../logo-light.png" alt="">
</tr>
</table> -->

<div class="col-12">
<img class="img img-responsive" src="http://aunor.portafolioitdast.com/aunor/assets/images/logo-light.png" alt="logo">
</div>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr>
		<td class="td_css">N°</td>
		<td class="td_css">Actividad / Tarea</td>		
		<td class="td_css">Tipo Riesgo</td>		
        <td class="td_css">Riesgo</td>
        <td class="td_css">Marco Legal</td>
        <td class="td_css">Causas</td>
        <td class="td_css">Eval 1</td>
        <td class="td_css">Controles</td>
        <td class="td_css">Control Administrativo</td>
        <td class="td_css">Eval 2</td>
    </tr>
    <tbody>
    <?php
    $con=0;$sw=0;
    $todasTareas = array();
        
    if(isset($List) && sizeof($List)>0){

        $actividadmptareamp = new ActividadmptareampController();


        foreach ($List as $actividiadItem) {

            $tareaList = $actividadmptareamp->listarPorPaginacionRiesgo(1,2000,'-1',$actividiadItem->procesoactividadmp_id);
            
           
    $tareaList = $tareaList["lista"];

            if(sizeof($tareaList)>0){
                foreach ($tareaList as $tareaobj) {
           
                    $tareaobj->actividadmp = $actividiadItem->actividadmp;
                    $todasTareas[] = $tareaobj;
                }

            }
        }

    }

    
        
        

        $con=0;$sw=0;
        if(isset($todasTareas) && sizeof($todasTareas)>0){

            foreach ($todasTareas as $item) {
            
                $color = "#FFF";
                if($sw==0){
                    $sw=1;
                }else{
                    $color = "#F7F7F7";
                    $sw=0;
                }

                $totalRiesgos = 0;

                if (isset($item->riesgos) && is_array($item->riesgos) && sizeof($item->riesgos)>0){
                    
                    $totalRiesgos = sizeof($item->riesgos);
                }else{
                    $item->riesgos = array();
                }

        
                echo '<tr style="background:'.$color.'">';
                echo "<td class='td_css' rowspan='".$totalRiesgos."' >".++$con."</td>";
                echo "<td class='td_css' rowspan='".$totalRiesgos."' >";
                if (isset($item->tareamp) && isset($item->tareamp->tareamp_nombre)){
                    echo $item->actividadmp->actividadmp_nombre." <br> ".$item->tareamp->tareamp_nombre;
                }else{
                    echo "";
                }
                echo "</td>";

                if ($totalRiesgos>0){
                    $countRiesgo = 0;
                    foreach ($item->riesgos as $riesgo) {

                        $riesgo->gestorriesgo_evaluacioninicial = json_decode($riesgo->gestorriesgo_evaluacioninicial);
                        $riesgo->gestorriesgo_evaluacionresidual = json_decode($riesgo->gestorriesgo_evaluacionresidual);

                        $riesgo->danos = json_decode($riesgo->danos);
                        $riesgo->peligro = json_decode($riesgo->peligro);
                        $riesgo->control = json_decode($riesgo->control);

                        if($countRiesgo >0){
                            echo '<tr>';
                        }

                        echo '<td class="td_css"></td>';

                        echo "<td class='td_css'>";
                        if (isset($riesgo->riesgo_nombre)){
                            echo $riesgo->riesgo_nombre;
                        }else{
                            echo "";
                        }
                        echo "</td>";

                        echo "<td class='td_css'>";
                        if (isset($riesgo->marcolegal) && is_array($riesgo->marcolegal) ){
                            foreach($riesgo->marcolegal as $value){
                                echo " ".$value->marcolegal_nombre." <br>";
                            }
                        }else{
                            echo "";
                        }
                        echo "</td>";


                        echo "<td class='td_css'>";
                        if (isset($riesgo->danos) && is_array($riesgo->danos) ){
                            foreach($riesgo->danos as $value){
                                echo " ".$value->dano_nombre." <br>";
                            }
                        }else{
                            echo "";
                        }
                        echo "</td>";


                        echo "<td class='td_css'>";
                        if (isset($riesgo->gestorriesgo_evaluacioninicial) && $riesgo->gestorriesgo_evaluacioninicial!=null && $riesgo->gestorriesgo_evaluacioninicial!="null" && $riesgo->gestorriesgo_evaluacioninicial!=""  ){
                            if(isset($riesgo->gestorriesgo_evaluacioninicial->niveldeprobabilidad050)){
                                echo "Nivel Probabilidad : ".$riesgo->gestorriesgo_evaluacioninicial->niveldeprobabilidad050->descripcion." <br>";
                            }else{
                                echo "Nivel Probabilidad : - <br>";
                            }
                            if(isset($riesgo->gestorriesgo_evaluacioninicial->gravedad) && isset($riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion) && is_array($riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion)){
                                echo "Nivel Gravedad : ".$riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion[0]->criterio." <br>";
                            }else{
                                echo "Nivel Gravedad : - <br>";
                            }
                            if(isset($riesgo->gestorriesgo_evaluacioninicial->nivelriesgoinicial050)){
                                echo "Nivel Riesgo : ".$riesgo->gestorriesgo_evaluacioninicial->nivelriesgoinicial050->descripcion." <br>";
                            }else{
                                echo "Nivel Riesgo : - <br>";
                            }
                        }else{
                            echo "Nivel Probabilidad : - <br>";
                            echo "Nivel Gravedad : - <br>";
                            echo "Nivel Riesgo : -";
                        }
                        echo "</td>";

                        echo '<td class="td_css">';
                        if (isset($riesgo->control) && is_array($riesgo->control) ){
                            foreach($riesgo->control as $value){
                                if(isset($value->critico) && $value->critico == 1 ){
                                    echo " ".$value->control_nombre."(Critico) <br>";
                                }else{
                                    echo " ".$value->control_nombre." <br>";
                                }
                            }
                        }else{
                            echo "";
                        }
                        echo '</td>';


                        echo '<td class="td_css">';
                        if (isset($riesgo->control) && is_array($riesgo->control) ){
                            $capacitacion = array();
                            $listaverificacion = array();
                            $documento = array();
                            $epps = array();
                            $epcs = array();
                            
                            foreach($riesgo->control as $value){
                                if(isset($value->capacitacion) && is_array($value->capacitacion) ){
                                    foreach($value->capacitacion as $c){
                                        $capacitacion[] = $c;
                                    }        
                                }
                                if(isset($value->listaverificacion) && is_array($value->listaverificacion) ){
                                    foreach($value->listaverificacion as $c){
                                        $listaverificacion[] = $c;
                                    }        
                                }
                                if(isset($value->documento) && is_array($value->documento) ){
                                    foreach($value->documento as $c){
                                        $documento[] = $c;
                                    }        
                                }
                                if(isset($value->epps) && is_array($value->epps) ){
                                    foreach($value->epps as $c){
                                        $epps[] = $c;
                                    }        
                                }
                                if(isset($value->epcs) && is_array($value->epcs) ){
                                    foreach($value->epcs as $c){
                                        $epcs[] = $c;
                                    }        
                                }
                            }

                            echo "-Capacitacion:  <br>";
                            foreach($capacitacion as $value){
                                if(isset($value->actividad_nombre)){
                                    echo $value->actividad_nombre."<br>";
                                }                                
                            }

                            echo "-Lista:  Verificacion <br>";
                            foreach($listaverificacion as $value){
                                echo $value->listaverificacion_nombre."<br>";
                            }

                            echo "-Documento:  <br>";
                            foreach($documento as $value){
                                echo $value->documentacion_nombre."<br>";
                            }

                            echo "-EPP:  <br>";
                            foreach($epps as $value){
                                echo $value->controlepp_nombre   ."<br>";
                            }

                            echo "-EPP:  <br>";
                            foreach($epcs as $value){
                                echo $value->controlepc_nombre   ."<br>";
                            }
                            

                        }else{
                            echo "";
                        }
                        echo '</td>';

                        echo "<td class='td_css'>";
                        if (isset($riesgo->gestorriesgo_evaluacionresidual) && $riesgo->gestorriesgo_evaluacionresidual!=null && $riesgo->gestorriesgo_evaluacionresidual!="null" && $riesgo->gestorriesgo_evaluacionresidual!=""  ){
                            if(isset($riesgo->gestorriesgo_evaluacionresidual->niveldeprobabilidad0502)){
                                echo "Nivel Probabilidad : ".$riesgo->gestorriesgo_evaluacionresidual->niveldeprobabilidad0502->descripcion." <br>";
                            }else{
                                echo "Nivel Probabilidad : - <br>";
                            }
                            if(isset($riesgo->gestorriesgo_evaluacionresidual->gravedad2) && isset($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion) && is_array($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion) && sizeof($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion)>0){
                                echo "Nivel Gravedad : ".$riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion[0]->criterio." <br>";
                            }else{
                                echo "Nivel Gravedad : - <br>";
                            }
                            if(isset($riesgo->gestorriesgo_evaluacionresidual->nivelriesgoinicial0502)){
                                echo "Nivel Riesgo : ".$riesgo->gestorriesgo_evaluacionresidual->nivelriesgoinicial0502->descripcion." <br>";
                            }else{
                                echo "Nivel Riesgo : - <br>";
                            }
                        }else{
                            echo "Nivel Probabilidad : - <br>";
                            echo "Nivel Gravedad : - <br>";
                            echo "Nivel Riesgo : -";
                        }
                        echo "</td>";
                

                        
                        if($countRiesgo>0){
                            echo '</tr>';
                        }


                        $countRiesgo = $countRiesgo+1;
                    }
                    
                }else{

                    echo '<td class="td_css">'."".'</td>';
                    echo '<td class="td_css">'."".'</td>';
                    echo '<td class="td_css">'."".'</td>';
                    echo '<td class="td_css">'."".'</td>';
                    echo '<td class="td_css">'."".'</td>';
                    echo '<td class="td_css">'."".'</td>';
                    echo '<td class="td_css">'."".'</td>';
                    echo '<td class="td_css">'."".'</td>';
                }
                
        
                if($totalRiesgos<2){
                    echo '</tr>';
                }
            }
        }
        if (isset($todasTareas) && sizeof($todasTareas)==0){
            echo '<tr><th colspan="3">No hay datos</th></tr>';
        }

        
    

    

    ?>
    </tbody>
</table>

</body>
</html>