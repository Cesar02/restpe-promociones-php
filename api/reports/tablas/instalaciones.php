<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/

$docente = new InstalacionesController();
$array = $docente->listarPorPaginacion(0,0,'-1','-1');
$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
		<th style="text-align: center">Id</th>
        <th style="text-align: center">Unidad Trabajo</th>
		<th style="text-align: center">Tipo Instalacion</th>	
        <th style="text-align: center">Nombre</th>		
		<th style="text-align: center">Persona</th>	
		<th style="text-align: center">Estado</th>			
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			echo '<td class="td_css">'.$item->instalaciones_id.'</td>';
            echo '<td class="td_css">'.$item->unidadtrabajo->unidadtrabajo_nombre.'</td>';
            echo '<td class="td_css">'.$item->tipoinstalacion->tipoinstalacion_nombre.'</td>';
			echo '<td class="td_css">'.$item->instalaciones_nombre.'</td>';
			echo '<td class="td_css">'.$item->persona->persona_nombres.' '.
									   $item->persona->persona_apellidopaterno.' '.
									   $item->persona->persona_apellidomaterno.
									   '</td>';
			
			echo '<td class="td_css">';
					if ($item->instalaciones_estado==='1'){
						echo "ACTIVO";
					}else{
						echo "INACTIVO";
					}
			echo '</td>';
        

        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="5">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>