<?php

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;
/**
 * Estilos
 */

$estiloCabecera = (new StyleBuilder())
    ->setFontBold()
    ->setFontSize(14)
    ->setFontColor(Color::WHITE)
    ->setShouldWrapText()
    ->setBackgroundColor("273948")
    ->build();

$estiloGeneral = (new StyleBuilder())
    ->setFontName('Calibri')
    ->setFontSize(9)
    ->build();

$estiloGeneralBold = (new StyleBuilder())
    ->setFontBold()
    ->setFontName('Calibri')
    ->setFontSize(9)
    ->build();

$borderHeaderTable = (new BorderBuilder())
    ->setBorderBottom("273948", Border::WIDTH_MEDIUM, Border::STYLE_SOLID)
    ->setBorderLeft("273948", Border::WIDTH_MEDIUM, Border::STYLE_SOLID)
    ->setBorderRight("273948", Border::WIDTH_MEDIUM, Border::STYLE_SOLID)
    ->setBorderTop("273948", Border::WIDTH_MEDIUM, Border::STYLE_SOLID)
    ->build();

$estiloHeaderTabla = (new StyleBuilder())
    ->setFontName('Calibri')
    ->setFontSize(9)
    ->setFontBold()
    ->setBorder($borderHeaderTable)
    ->build();

// Parametros url
$protocolo_id = "-1";
$clinicasede_id = "-1";
$programacionemopersona_id = null;

if (isset($_GET["protocolo_id"])) {
    $protocolo_id = $_GET["protocolo_id"];
}
if (isset($_GET["clinicasede_id"])) {
    $clinicasede_id = $_GET["clinicasede_id"];
}
if (isset($_GET["programacionemopersona_id"])) {
    $programacionemopersona_id = $_GET["programacionemopersona_id"];
}

// ... and a writer to create the new file
$writer = WriterFactory::create(Type::XLSX);
$writer->setDefaultRowStyle($estiloGeneral);
$nombreArchivo = 'php:/Examenso_' . date("d.m.Y") . '_' . date("H:i:s") . '.xlsx';
$writer->openToBrowser($nombreArchivo);


$filtro_text = "Listado de precios al " . date("d.m.Y") . '_' . date("H:i:s");


$protocoloCtrl = new ProtocoloController();
$protocoloObj = null;
$examenesList = array();
$atributoList = array();

// Columnas del excel
$array_header_table = ["Identificador","Documento","Nombres"];
$two_array_header_table =  ["#","",""];

$personaListRows = array();

/**
 * obtengo el arreglo de impuestos
 */

$personasList = Programacionemopersona::getPersonasByProgramacionPersonaEmoId($programacionemopersona_id)["programacionemopersona_array"];



$array_localesPermitidos = array();

$examenesList =  Examenso::getByFields(
    array(
        array('field' => 'examenso_isdelete', 'value' =>NO, 'operator' => '='),
        array('field' => 'examenso_estado', 'value' =>ACTIVO, 'operator' => '=')
    ),
    array(
        array('field' => 'examenso_orden', 'order' => 'asc')
    )
)["examenso_array"];
array_multisort(array_column($examenesList, 'examenso_orden'), SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $examenesList);

foreach($examenesList as $eso){
    $eso->atributoList = Atributoexamenso::getByFields(array(
        array('field' => 'atributoexamenso_isdelete', 'value' =>NO, 'operator' => '='),
        array('field' => 'atributoexamenso_estado', 'value' =>ACTIVO, 'operator' => '='),
        array('field' => 'examenso_id', 'value' =>$eso->examenso_id, 'operator' => '=')
    ))['atributoexamenso_array'];
    
    if(sizeof($eso->atributoList) >0){
        // $two_array_header_table[] = $eso->examenso_nombre;
    }

    array_multisort(array_column($eso->atributoList, 'atributoexamenso_orden'), SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $eso->atributoList);

    foreach($eso->atributoList as $attr){
        $array_header_table[] = "{$attr->atributoexamenso_id}-".$attr->atributoexamenso_nombre;
        $two_array_header_table[] = $attr->atributoexamenso_nombre;
        $atributoList[] = $attr;
    }

}

if($protocolo_id != PARAM_TODOS){
    
    /*
    $protocoloObj = Protocolo::getById($protocolo_id);
    if(isset($protocoloObj) && isset($protocoloObj->protocolo_id)){
        $examenesList =  Examenso::getByProtcoloId($protocoloObj->protocolo_id);

        array_multisort(array_column($examenesList, 'examenso_orden'), SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $examenesList);

        foreach($examenesList as $eso){
            $eso->atributoList = Atributoexamenso::getByFields(array(
                array('field' => 'atributoexamenso_isdelete', 'value' =>NO, 'operator' => '='),
                array('field' => 'atributoexamenso_estado', 'value' =>ACTIVO, 'operator' => '='),
                array('field' => 'examenso_id', 'value' =>$eso->examenso_id, 'operator' => '=')
            ))['atributoexamenso_array'];
            
            if(sizeof($eso->atributoList) >0){
                // $two_array_header_table[] = $eso->examenso_nombre;
            }

            array_multisort(array_column($eso->atributoList, 'atributoexamenso_orden'), SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $eso->atributoList);

            foreach($eso->atributoList as $attr){
                $array_header_table[] = "{$attr->atributoexamenso_id}-".$attr->atributoexamenso_nombre;
                $two_array_header_table[] = $attr->atributoexamenso_nombre;
                $atributoList[] = $attr;
            }

        }
    }
    */
}

$writer->addRowWithStyle($array_header_table, $estiloHeaderTabla);



//$writer->addRowWithStyle($two_array_header_table, $estiloHeaderTabla);

if(sizeof($atributoList)>0){

    foreach($personasList as $p){
        $arrayp = array();
        $arrayp[] = $p->programacionemopersona_id;
        $arrayp[] = $p->persona_dni;
        $arrayp[] = Utility::camelCaseIam($p->persona_apellidopaterno)." ".Utility::camelCaseIam($p->persona_apellidomaterno).", ".Utility::camelCaseIam($p->persona_nombres);

        foreach($atributoList as $attr){
            $arrayp[] = "";
        }

        $writer->addRowWithStyle($arrayp, $estiloHeaderTabla);
    }

}


$data = array();


/**
 * Fin del while
 */

$writer->close();