<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/

$actividadmptareamp = new ActividadmptareampController();

$procesoactividadmp_id = PARAM_TODOS;
$esriesgo = "0";

if(isset($_GET["procesoactividadmp_id"]) && is_numeric($_GET["procesoactividadmp_id"]) && intval($_GET["procesoactividadmp_id"]) > 0){
    $procesoactividadmp_id = $_GET["procesoactividadmp_id"];
}
if(isset($_GET["esriesgo"]) && is_numeric($_GET["esriesgo"]) && intval($_GET["esriesgo"]) > 0){
    $esriesgo = $_GET["esriesgo"];
}
//$array = array("lista"=>array());
$array = $actividadmptareamp->listarPorPaginacionRiesgo(1,2000,'-1',$procesoactividadmp_id);


$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>
<!-- <div class="col-12">
<img class="img img-responsive" src="http://aunor.portafolioitdast.com/aunor/assets/images/logo-light.png" alt="logo">
</div> -->
<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">

<?php
    if($procesoactividadmp_id != PARAM_TODOS){
        
        $actividad = Procesoactividadmp::getByIdDetallado($procesoactividadmp_id);
        
    
?>
        <tr>
            <td rowspan="3" class="td_css">
                <img height="60" width="60" style="width:60px !important; height:60px !important"  class="img img-responsive" src="http://aunor.portafolioitdast.com/aunor/assets/images/logo-light.png" alt="logo">
            </td>
            <td rowspan="3" colspan="14" class="td_css">
            <?php
                echo "Matriz evaluación de riesgos";
            ?>
            </td>
            <td  class="td_css">
                Codigo:
            </td>	
        </tr>
        <tr>
            <td class="td_css">
                Version:
            </td>	
        </tr>
        <tr>
            <td class="td_css">
                Fecha: 
            </td>	
        </tr>
        

        

        <tr>
            <td colspan="8" class="td_css" style="text-align: initial;">Concesion:
            <?php
                if(isset($actividad) && isset($actividad->concesion) && isset($actividad->concesion->concesion_nombre)){
                    echo "".$actividad->concesion->concesion_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>
            <td colspan="8" class="td_css" style="text-align: initial;">Empresa:
            <?php
                if(isset($actividad) && isset($actividad->empresa) && isset($actividad->empresa->empresa_nombre)){
                    echo "".$actividad->empresa->empresa_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>	
        </tr>

        <!-- INCIIO -->
        <tr>
            <td colspan="4" class="td_css" style="text-align: initial;">Gerencia:
            <?php
                if(isset($actividad) && isset($actividad->gerencia) && isset($actividad->gerencia->area_nombre)){
                    echo "".$actividad->gerencia->area_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>
            <td colspan="4" class="td_css" style="text-align: initial;">Area:
            <?php
                if(isset($actividad) && isset($actividad->proceso) && isset($actividad->proceso->proceso_nombre)){
                    echo "".$actividad->proceso->proceso_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>
            <td colspan="4" class="td_css" style="text-align: initial;">Proceso:
            <?php
                if(isset($actividad->subprocesomp) && isset($actividad->subprocesomp) && isset($actividad->subprocesomp->subprocesomp_nombre)){
                    echo "".$actividad->subprocesomp->subprocesomp_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>
            <td colspan="4" class="td_css" style="text-align: initial;">Actividad:
            <?php
                if(isset($actividad->actividadmp) && isset($actividad->actividadmp) && isset($actividad->actividadmp->actividadmp_nombre)){
                    echo "".$actividad->actividadmp->actividadmp_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>
        </tr>
        <!-- FIN -->

<?php
    
    }
?>
    <tr>
		<td class="td_css" colspan="16"></td>
    </tr>
    <tr>
		<td class="td_css" colspan="3"></td>
        <td class="td_css" colspan="4">Factores de riesgo</td>
        <td class="td_css" ></td>
        <td class="td_css" colspan="3">Evaluación inicial</td>
        <td class="td_css" colspan="2"></td>
        <td class="td_css" colspan="3">Evaluación final</td>
    </tr>
    
    <tr>
		<td class="td_css">ID</td>
		<td class="td_css">Orden</td>		
        <td class="td_css">Tarea</td>		
		<td class="td_css">Riesgo</td>
        <td class="td_css">Causas</td>
        <td class="td_css">Efectos</td>
        <td class="td_css">Daños</td>	
        <td class="td_css">Marco Legal</td>
        <td class="td_css">Probabilidad</td>
        <td class="td_css">Gravedad</td>	
        <td class="td_css">Nivel de Riesgo</td>
        
        <td class="td_css">Controles</td>
        <td class="td_css">Control especifico</td>
        
        <td class="td_css">Probabilidad</td>
        <td class="td_css">Gravedad</td>	
        <td class="td_css">Nivel de Riesgo</td>
        
    </tr>

    <tbody>
    <?php
    $con=0;$sw=0;
    if(isset($List) && sizeof($List)>0){

        foreach ($List as $item) {
        
            $color = "#FFF";
            if($sw==0){
                $sw=1;
            }else{
                $color = "#F7F7F7";
                $sw=0;
            }

            $totalRiesgos = 0;

            if (isset($item->riesgos) && is_array($item->riesgos) && sizeof($item->riesgos)>0){
                
                $totalRiesgos = sizeof($item->riesgos);
            }else{
                $item->riesgos = array();
            }

    
            echo '<tr style="background:'.$color.'">';
            //echo "<td class='td_css' rowspan='".$totalRiesgos."' >".++$con."</td>";
            echo "<td class='td_css' rowspan='".$totalRiesgos."' >".$item->actividadmptareamp_id."</td>";
            echo "<td class='td_css' rowspan='".$totalRiesgos."' >".$item->actividadmptareamp_orden."</td>";
            echo "<td class='td_css' rowspan='".$totalRiesgos."' >";
            if (isset($item->tareamp) && isset($item->tareamp->tareamp_nombre)){
                echo $item->tareamp->tareamp_nombre;
            }else{
                echo "";
            }
            echo "</td>";

            if ($totalRiesgos>0){
                $countRiesgo = 0;
                foreach ($item->riesgos as $riesgo) {

                    $riesgo->gestorriesgo_evaluacioninicial = json_decode($riesgo->gestorriesgo_evaluacioninicial);
                    $riesgo->gestorriesgo_evaluacionresidual = json_decode($riesgo->gestorriesgo_evaluacionresidual);

                    $riesgo->danos = json_decode($riesgo->danos);
                    $riesgo->peligro = json_decode($riesgo->peligro);
                    $riesgo->control = json_decode($riesgo->control);

                    if($countRiesgo >0){
                        echo '<tr>';
                    }

                    /* echo '<td class="td_css"></td>'; */

                    echo "<td class='td_css'>";
                    if (isset($riesgo->riesgo_nombre)){
                        echo $riesgo->riesgo_nombre;
                    }else{
                        echo "";
                    }
                    echo "</td>";


                    echo "<td class='td_css'>";
                    if (isset($riesgo->danos) && is_array($riesgo->danos) ){
                        foreach($riesgo->danos as $value){
                            echo " ".$value->dano_nombre." <br>";
                        }
                    }else{
                        echo "";
                    }
                    echo "</td>";

                    echo '<td class="td_css"></td>';
                    echo '<td class="td_css"></td>';

                    echo "<td class='td_css'>";
                    if (isset($riesgo->marcolegal) && is_array($riesgo->marcolegal) ){
                        foreach($riesgo->marcolegal as $value){
                            echo " ".$value->marcolegal_nombre." <br>";
                        }
                    }else{
                        echo "";
                    }
                    echo "</td>";


                    /* echo "<td class='td_css'>";
                    if (isset($riesgo->gestorriesgo_evaluacioninicial) && $riesgo->gestorriesgo_evaluacioninicial!=null && $riesgo->gestorriesgo_evaluacioninicial!="null" && $riesgo->gestorriesgo_evaluacioninicial!=""  ){
                        if(isset($riesgo->gestorriesgo_evaluacioninicial->niveldeprobabilidad050)){
                            echo "Nivel Probabilidad : ".$riesgo->gestorriesgo_evaluacioninicial->niveldeprobabilidad050->descripcion." <br>";
                        }else{
                            echo "Nivel Probabilidad : - <br>";
                        }
                        if(isset($riesgo->gestorriesgo_evaluacioninicial->gravedad) && isset($riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion) && is_array($riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion)){
                            echo "Nivel Gravedad : ".$riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion[0]->criterio." <br>";
                        }else{
                            echo "Nivel Gravedad : - <br>";
                        }
                        if(isset($riesgo->gestorriesgo_evaluacioninicial->nivelriesgoinicial050)){
                            echo "Nivel Riesgo : ".$riesgo->gestorriesgo_evaluacioninicial->nivelriesgoinicial050->descripcion." <br>";
                        }else{
                            echo "Nivel Riesgo : - <br>";
                        }
                    }else{
                        echo "Nivel Probabilidad : - <br>";
                        echo "Nivel Gravedad : - <br>";
                        echo "Nivel Riesgo : -";
                    }
                    echo "</td>"; */

                    echo "<td class='td_css'>";
                    if (isset($riesgo->gestorriesgo_evaluacioninicial) && $riesgo->gestorriesgo_evaluacioninicial!=null && $riesgo->gestorriesgo_evaluacioninicial!="null" && $riesgo->gestorriesgo_evaluacioninicial!=""  ){
                        if(isset($riesgo->gestorriesgo_evaluacioninicial->niveldeprobabilidad050)){
                            echo "".$riesgo->gestorriesgo_evaluacioninicial->niveldeprobabilidad050->descripcion."";
                        }else{
                            echo "-";
                        }
                    }else{
                        echo "-";
                    }
                    echo "</td>";

                    echo "<td class='td_css'>";
                    if (isset($riesgo->gestorriesgo_evaluacioninicial) && $riesgo->gestorriesgo_evaluacioninicial!=null && $riesgo->gestorriesgo_evaluacioninicial!="null" && $riesgo->gestorriesgo_evaluacioninicial!=""  ){
                        if(isset($riesgo->gestorriesgo_evaluacioninicial->gravedad) && isset($riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion) && is_array($riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion)){
                            echo "".$riesgo->gestorriesgo_evaluacioninicial->gravedad->descripcion[0]->criterio."";
                        }else{
                            echo "-";
                        }
                    }else{
                        echo "-";
                    }
                    echo "</td>";

                    echo "<td class='td_css'>";
                    if (isset($riesgo->gestorriesgo_evaluacioninicial) && $riesgo->gestorriesgo_evaluacioninicial!=null && $riesgo->gestorriesgo_evaluacioninicial!="null" && $riesgo->gestorriesgo_evaluacioninicial!=""  ){
                        if(isset($riesgo->gestorriesgo_evaluacioninicial->nivelriesgoinicial050)){
                            echo "".$riesgo->gestorriesgo_evaluacioninicial->nivelriesgoinicial050->descripcion."";
                        }else{
                            echo "-";
                        }
                    }else{
                        echo "-";
                    }
                    echo "</td>";

                    echo '<td class="td_css">';
                    if (isset($riesgo->control) && is_array($riesgo->control) ){
                        foreach($riesgo->control as $value){
                            if(isset($value->critico) && $value->critico == 1 ){
                                echo " ".$value->control_nombre."(Critico) <br>";
                            }else{
                                echo " ".$value->control_nombre." <br>";
                            }
                        }
                    }else{
                        echo "";
                    }
                    echo '</td>';


                    echo '<td class="td_css">';
                    if (isset($riesgo->control) && is_array($riesgo->control) ){
                        $capacitacion = array();
                        $listaverificacion = array();
                        $documento = array();
                        $epps = array();
                        $epcs = array();
                        
                        foreach($riesgo->control as $value){
                            if(isset($value->capacitacion) && is_array($value->capacitacion) ){
                                foreach($value->capacitacion as $c){
                                    $capacitacion[] = $c;
                                }        
                            }
                            if(isset($value->listaverificacion) && is_array($value->listaverificacion) ){
                                foreach($value->listaverificacion as $c){
                                    $listaverificacion[] = $c;
                                }        
                            }
                            if(isset($value->documento) && is_array($value->documento) ){
                                foreach($value->documento as $c){
                                    $documento[] = $c;
                                }        
                            }
                            if(isset($value->epps) && is_array($value->epps) ){
                                foreach($value->epps as $c){
                                    $epps[] = $c;
                                }        
                            }
                            if(isset($value->epcs) && is_array($value->epcs) ){
                                foreach($value->epcs as $c){
                                    $epcs[] = $c;
                                }        
                            }
                        }

                        echo "-Capacitacion:  <br>";
                        foreach($capacitacion as $value){
                            if(isset($value->actividad_nombre)){
                                echo $value->actividad_nombre."<br>";
                            }

                        }

                        echo "-Lista:  Verificacion <br>";
                        foreach($listaverificacion as $value){
                            echo $value->listaverificacion_nombre."<br>";
                        }

                        echo "-Documento:  <br>";
                        foreach($documento as $value){
                            echo $value->documentacion_nombre."<br>";
                        }

                        echo "-EPP:  <br>";
                        foreach($epps as $value){
                            echo $value->controlepp_nombre   ."<br>";
                        }

                        echo "-EPP:  <br>";
                        foreach($epcs as $value){
                            echo $value->controlepc_nombre   ."<br>";
                        }
                        

                    }else{
                        echo "";
                    }
                    echo '</td>';

                    /* echo "<td class='td_css'>";
                    if (isset($riesgo->gestorriesgo_evaluacionresidual) && $riesgo->gestorriesgo_evaluacionresidual!=null && $riesgo->gestorriesgo_evaluacionresidual!="null" && $riesgo->gestorriesgo_evaluacionresidual!=""  ){
                        if(isset($riesgo->gestorriesgo_evaluacionresidual->niveldeprobabilidad0502)){
                            echo "Nivel Probabilidad : ".$riesgo->gestorriesgo_evaluacionresidual->niveldeprobabilidad0502->descripcion." <br>";
                        }else{
                            echo "Nivel Probabilidad : - <br>";
                        }
                        if(isset($riesgo->gestorriesgo_evaluacionresidual->gravedad2) && isset($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion) && is_array($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion) && sizeof($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion)){
                            echo "Nivel Gravedad : ".isset($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion[0]->criterio) ? $riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion[0]->criterio : ''." <br>";
                        }else{
                            echo "Nivel Gravedad : - <br>";
                        }
                        if(isset($riesgo->gestorriesgo_evaluacionresidual->nivelriesgoinicial0502)){
                            echo "Nivel Riesgo : ".$riesgo->gestorriesgo_evaluacionresidual->nivelriesgoinicial0502->descripcion." <br>";
                        }else{
                            echo "Nivel Riesgo : - <br>";
                        }
                    }else{
                        echo "Nivel Probabilidad : - <br>";
                        echo "Nivel Gravedad : - <br>";
                        echo "Nivel Riesgo : -";
                    }
                    echo "</td>"; */
            
                    echo "<td class='td_css'>";
                    if (isset($riesgo->gestorriesgo_evaluacionresidual) && $riesgo->gestorriesgo_evaluacionresidual!=null && $riesgo->gestorriesgo_evaluacionresidual!="null" && $riesgo->gestorriesgo_evaluacionresidual!=""  ){
                        if(isset($riesgo->gestorriesgo_evaluacionresidual->niveldeprobabilidad0502)){
                            echo "".$riesgo->gestorriesgo_evaluacionresidual->niveldeprobabilidad0502->descripcion."";
                        }else{
                            echo "-";
                        }
                    }else{
                        echo "-";
                    }
                    echo "</td>";

                    echo "<td class='td_css'>";
                    if (isset($riesgo->gestorriesgo_evaluacionresidual) && $riesgo->gestorriesgo_evaluacionresidual!=null && $riesgo->gestorriesgo_evaluacionresidual!="null" && $riesgo->gestorriesgo_evaluacionresidual!=""  ){
                        if(isset($riesgo->gestorriesgo_evaluacionresidual->gravedad2) && isset($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion) && is_array($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion) && sizeof($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion)){
                            echo "".isset($riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion[0]->criterio) ? $riesgo->gestorriesgo_evaluacionresidual->gravedad2->descripcion[0]->criterio : ''."";
                        }else{
                            echo "-";
                        }
                    }else{
                        echo "-";
                    }
                    echo "</td>";

                    echo "<td class='td_css'>";
                    if (isset($riesgo->gestorriesgo_evaluacionresidual) && $riesgo->gestorriesgo_evaluacionresidual!=null && $riesgo->gestorriesgo_evaluacionresidual!="null" && $riesgo->gestorriesgo_evaluacionresidual!=""  ){
                        if(isset($riesgo->gestorriesgo_evaluacionresidual->nivelriesgoinicial0502)){
                            echo "".$riesgo->gestorriesgo_evaluacionresidual->nivelriesgoinicial0502->descripcion."";
                        }else{
                            echo "-";
                        }
                    }else{
                        echo "-";
                    }
                    echo "</td>";

                    
                    if($countRiesgo>0){
                        echo '</tr>';
                    }


                    $countRiesgo = $countRiesgo+1;
                }
                
            }else{

                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
            }
            
    
            if($totalRiesgos<2){
                echo '</tr>';
            }
        }
    }
    if (isset($List) && sizeof($List)==0){
        echo '<tr><th colspan="16">No hay datos</th></tr>';
    }else{

        if($procesoactividadmp_id != PARAM_TODOS){
            echo "<tr>";
            echo "<td class='td_css' colspan='16'></td>";            
            echo "</tr>";
            
            echo "<tr>";
            echo "<td class='td_css'>Elaborado por </td>";
            echo "<td class='td_css' colspan='15'>";
            if(isset($actividad->proceso) && isset($actividad->proceso) && isset($actividad->proceso->persona) && isset($actividad->proceso->persona->persona_id)){
                echo "".$actividad->proceso->persona->persona_nombres." ".$actividad->proceso->persona->persona_apellidopaterno." ".$actividad->proceso->persona->persona_apellidomaterno."";
            }else{
                echo "";
            }
            echo "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td class='td_css'>Aprobado por </td>";
            echo "<td class='td_css' colspan='15'>";
            if(isset($actividad->gerencia) && isset($actividad->gerencia) && isset($actividad->gerencia->persona) && isset($actividad->gerencia->persona->persona_id)){
                echo "".$actividad->gerencia->persona->persona_nombres." ".$actividad->gerencia->persona->persona_apellidopaterno." ".$actividad->gerencia->persona->persona_apellidomaterno."";
            }else{
                echo "";
            }
            echo "</td>";
            echo "</tr>";

        }

    }

    

    ?>
    </tbody>
</table>

</body>
</html>