<?php

$ctrl = new ProgramacionController();

$objParams = array();

$objParams["tipo"] = VERIFICACION_CONTROL_CRITICO;

if(isset($_GET["pagina"])){
    $objParams["pagina"] = $_GET["pagina"];
}

if(isset($_GET["registros"])){
    $objParams["registros"] = $_GET["registros"];
}

if(isset($_GET["busqueda"])){
    $objParams["busqueda"] = $_GET["busqueda"];
}else{
    $objParams["busqueda"] = PARAM_TODOS;
}

if(isset($_GET["empresa_id"])){
    $objParams["empresa_id"] = $_GET["empresa_id"];
}else{
    $objParams["empresa_id"] = PARAM_TODOS;
}

if(isset($_GET["preprogramacion_id"])){
    $objParams["preprogramacion_id"] = $_GET["preprogramacion_id"];
}else{
    $objParams["preprogramacion_id"] = PARAM_TODOS;
}

if(isset($_GET["columnaOrden"])){
    $objParams["columnaOrden"] = $_GET["columnaOrden"];
}else{
    $objParams["columnaOrden"] = PARAM_TODOS;
}

if(isset($_GET["orderAscending"])){
    $objParams["orderAscending"] = $_GET["orderAscending"];
}else{
    $objParams["orderAscending"] = PARAM_TODOS;
}

$array = $ctrl->listarPorPaginacionEmpresa($objParams["pagina"],$objParams["registros"],$objParams["busqueda"],$objParams["tipo"],$objParams["empresa_id"],$objParams["preprogramacion_id"],$objParams["orderAscending"],$objParams["columnaOrden"]);
$List =  $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
            <th>#</th>
            <th>ID</th>
            <th>Tipo</th>
            <th>Grupo Lv</th>
            <th>Publico Objetivo</th>
            <th>Encargado</th>
            <th>Meses</th>		
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
        
        echo '<td class="td_css">'.++$con.'</td>';
        
        echo '<td class="td_css">'.$item->programacion_id.'</td>';
        
        echo '<td class="td_css">';
        if(isset($item->tarea->actividad->actividad_nombre))
            echo $item->tarea->actividad->actividad_nombre;
        echo'</td>';

        echo '<td class="td_css">';
        if(isset($item->tarea->tarea_nombre))
            echo $item->tarea->tarea_nombre;
        echo'</td>';

        echo '<td class="td_css">';
        if($item->programacion_esgrupoobjetivo=='0'){
            if($item->programacion_esparatodos=='0'){
                foreach($item->procesos as $proceso){
                    echo $proceso->proceso->proceso_nombre."<br>";
                }
            }
            if($item->programacion_esparatodos=='1'){
                echo "Todas las Areas";
            }
        }
        if($item->programacion_esgrupoobjetivo=='1'){
            foreach($item->programaciontipogrupopersona as $pp){
                echo $pp->tipogrupopersona->tipogrupopersona_nombre."<br>";
            }
        }
        echo'</td>';


        echo '<td class="td_css">';
        if(isset($item->programacion_tipo) && $item->programacion_tipo=='1'){
            if($item->programacion_tiporesponsable=='2'){
                echo "Encargado Seguridad";
            }
            if($item->programacion_tiporesponsable=='3'){
                echo "Salud Ocupacional";
            }
            if($item->programacion_tiporesponsable=='4'){
                echo "Jefe Area";
            }
            if($item->programacion_tiporesponsable=='5'){
                echo "Jefe UT";
            }
            if($item->programacion_tiporesponsable=='1'){
                foreach($item->responsables as $responsable){
                    echo $responsable->persona->persona_apellidopaterno." ".$responsable->persona->persona_apellidomaterno.", ".$responsable->persona->persona_nombres."<br>";
                }
            }
        }
        if(isset($item->programacion_tipo) && $item->programacion_tipo=='0'){
            echo $item->programacion_empresanombre;
        }
        echo'</td>';

        echo '<td class="td_css">';
        foreach($item->fechas as $fecha){
            echo Utility::getNombreMes(Utility::getFechaSegunFormato($fecha->programacionfecha_fechainicio,'m'));
        }
        echo'</td>';
                
        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="4">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>