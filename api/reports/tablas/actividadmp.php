<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/

$procesoactividadmp = new ProcesoactividadmpController();

$empresaId = PARAM_TODOS;
$estadoactividad = PARAM_TODOS;
$tipeView = '1';
$actividadId = PARAM_TODOS;
$esriesgo = "0";
if(isset($_GET["estado"]) && is_numeric($_GET["estado"]) && intval($_GET["estado"]) > 0){
    $estadoactividad = $_GET["estado"];
}

if(isset($_GET["empresa_id"]) && is_numeric($_GET["empresa_id"]) && intval($_GET["empresa_id"]) > 0){
    $empresaId = $_GET["empresa_id"];
}

if(isset($_GET["typeView"]) && is_numeric($_GET["typeView"]) && intval($_GET["typeView"]) > 0){
    $tipeView = $_GET["typeView"];
}

if(isset($_GET["actividadId"]) && is_numeric($_GET["actividadId"]) && intval($_GET["actividadId"]) > 0){
    $actividadId = $_GET["actividadId"];
}

if(isset($_GET["esriesgo"]) && is_numeric($_GET["esriesgo"]) && intval($_GET["esriesgo"]) > 0){
    $esriesgo = $_GET["esriesgo"];
}

$array = $procesoactividadmp->listarPorPaginacion(1,2000,'-1',$empresaId,$estadoactividad,$actividadId);
$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>
<div class="col-12">
<img class="img img-responsive" src="http://aunor.portafolioitdast.com/aunor/assets/images/logo-light.png" alt="logo">
</div>

<?php
    if($tipeView == "1"){
?>
    
    <table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
        <tr>
            <td class="td_css">N°</td>
            <td class="td_css">ID</td>
            <td class="td_css">Gerencia</td>		
            <td class="td_css">Area</td>
            <td class="td_css">Proceso</td>
            <td class="td_css">Actividad</td>
            <td class="td_css">Estado</td>			
        </tr>
        <tbody>
        <?php
        $con=0;$sw=0;
        foreach ($List as $item) {
            
            $color = "#FFF";
            if($sw==0){
                $sw=1;
            }else{
                $color = "#F7F7F7";
                $sw=0;
            }        

            echo '<tr style="background:'.$color.'">';
                echo '<td class="td_css">'.++$con.'</td>';
                echo '<td class="td_css">'.$item->procesoactividadmp_id.'</td>';
                echo '<td class="td_css">';
                    if (isset($item->gerencia) && isset($item->gerencia->area_nombre)){
                        echo $item->gerencia->area_nombre;
                    }else{
                        echo "";
                    }
                echo '</td>';

                echo '<td class="td_css">';
                    if (isset($item->proceso) && isset($item->proceso->proceso_nombre)){
                        echo $item->proceso->proceso_nombre;
                    }else{
                        echo "";
                    }
                echo '</td>';

                echo '<td class="td_css">';
                    if (isset($item->subprocesomp) && isset($item->subprocesomp->subprocesomp_nombre)){
                        echo $item->subprocesomp->subprocesomp_nombre;
                    }else{
                        echo "";
                    }
                echo '</td>';

                echo '<td class="td_css">';
                    if (isset($item->actividadmp) && isset($item->actividadmp->actividadmp_nombre)){
                        echo $item->actividadmp->actividadmp_nombre;
                    }else{
                        echo "";
                    }
                echo '</td>';
                
                echo '<td class="td_css">';
                        if ($item->procesoactividadmp_estadomp==='1'){
                            echo "EN PROCESO";
                        }else if ($item->procesoactividadmp_estadomp==='2'){
                            echo "REVISION";
                        }else if ($item->procesoactividadmp_estadomp==='3'){
                            echo "OBSERVADA";
                        }else if ($item->procesoactividadmp_estadomp==='4'){
                            echo "APROBADA";
                        }else{
                            echo "";
                        }
                echo '</td>';
            

            echo '</tr>';
        }
        if (sizeof($List)==0){
            echo '<tr><th colspan="3">No hay datos</th></tr>';
        }

        ?>
        </tbody>
    </table>

<?php
   }else{
    $actividad = null;
    if (sizeof($List)==0){
        echo '<tr><th colspan="3">No hay datos</th></tr>';
    }else{
?>
<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
<tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
<?php
if(sizeof($List)==1){
        $actividadmptareamp = new ActividadmptareampController(); 
        $actividad = $List[0];
        if($esriesgo == "1"){
            $tareas = $actividadmptareamp->listarPorPaginacionRiesgo(1,2000,'-1',$actividad->procesoactividadmp_id);
        }
        if($esriesgo == "0"){
            $tareas = $actividadmptareamp->listarPorPaginacion(1,2000,'-1',$actividad->procesoactividadmp_id);
        }
        $tareaList = $tareas["lista"];
?>
        
        <tr>
            <td class="td_css">Concesion</td>
            <td class="td_css" colspan="8">
            <?php
                if(isset($actividad) && isset($actividad->concesion) && isset($actividad->concesion->concesion_nombre)){
                    echo "".$actividad->concesion->concesion_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>		
        </tr>
        <tr>
            <td class="td_css">Empresa</td>	
            <td class="td_css" colspan="8">
            <?php
                if(isset($actividad) && isset($actividad->empresa) && isset($actividad->empresa->empresa_nombre)){
                    echo "".$actividad->empresa->empresa_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>	
        </tr>
        <tr>
            <td class="td_css">Gerencia</td>
            <td class="td_css" colspan="8">
            <?php
                if(isset($actividad) && isset($actividad->gerencia) && isset($actividad->gerencia->area_nombre)){
                    echo "".$actividad->gerencia->area_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>		
        </tr>
        <tr>
            <td class="td_css">Area</td>
            <td class="td_css" colspan="8">
            <?php
                if(isset($actividad) && isset($actividad->proceso) && isset($actividad->proceso->proceso_nombre)){
                    echo "".$actividad->proceso->proceso_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>			
        </tr>
        <tr>
            <td class="td_css">Proceso</td>
            <td class="td_css" colspan="8">
            <?php
                if(isset($actividad->subprocesomp) && isset($actividad->subprocesomp) && isset($actividad->subprocesomp->subprocesomp_nombre)){
                    echo "".$actividad->subprocesomp->subprocesomp_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>		
        </tr>
        <tr>
            <td class="td_css">Acitividad</td>
            <td class="td_css" colspan="8">
            <?php
                if(isset($actividad->actividadmp) && isset($actividad->actividadmp) && isset($actividad->actividadmp->actividadmp_nombre)){
                    echo "".$actividad->actividadmp->actividadmp_nombre."";
                }else{
                    echo "";
                }
            ?>
            </td>	
        </tr>
        <tr>
        </tr>
        <tr>
            <td class="td_css">N°</td>
            <td class="td_css">ID</td>
            <td class="td_css">Tarea</td>		
            <td class="td_css">Descripcion</td>		
            <td class="td_css">Orden</td>
            <td class="td_css">Personas</td>
            <td class="td_css">Vehiculos</td>
            <td class="td_css">Equipos Movil</td>
            <td class="td_css">Equipos Menores</td>
            <td class="td_css">Equipo Levante</td>
            <td class="td_css">Herramienta</td>
            <td class="td_css">Equipo Emergencia</td>
            <td class="td_css">Altura</td>
            <td class="td_css">Instalacion</td>
            <?php
                if($esriesgo=="1"){
                    echo "<td class='td_css'>Riesgo</td>";
                }
            ?>		
        </tr>
        <tbody>
        <?php
        $con=0;$sw=0;

        if(isset($tareaList) && sizeof($tareaList)>0){
            foreach ($tareaList as $item) {
            
                $color = "#FFF";
                if($sw==0){
                    $sw=1;
                }else{
                    $color = "#F7F7F7";
                    $sw=0;
                }
                $item->actividadmptareamp_puestos = json_decode($item->actividadmptareamp_puestos);
                $item->actividadmptareamp_recursos = json_decode($item->actividadmptareamp_recursos);

                if(isset($item->actividadmptareamp_recursos) && $item->actividadmptareamp_recursos!=""){
                }else{
                    $item->actividadmptareamp_recursos = new stdClass();
                    $item->actividadmptareamp_recursos->tipovehiculo_id = array();
                    $item->actividadmptareamp_recursos->tipoequipomovil_id = array();
                    $item->actividadmptareamp_recursos->tipoequipomenor_id = array();
                    $item->actividadmptareamp_recursos->tipoequipolevante_id = array();
                    $item->actividadmptareamp_recursos->tipoherramienta_id = array();
                    $item->actividadmptareamp_recursos->tipoequipamientoemergencia_id = array();
                    $item->actividadmptareamp_recursos->tipoelementotrabajoaltura_id = array();
                    $item->actividadmptareamp_recursos->tipoinstalacion_id = array();
                }

        
                    echo '<tr style="background:'.$color.'">';
                    echo '<td class="td_css">'.++$con.'</td>';
                    echo '<td class="td_css">'.$item->actividadmptareamp_id.'</td>';
                    echo '<td class="td_css">';
                        if (isset($item->tareamp) && isset($item->tareamp->tareamp_nombre)){
                            echo $item->tareamp->tareamp_nombre;
                        }else{
                            echo "";
                        }
                    echo '</td>';
        
                    echo '<td class="td_css">';
                        if (isset($item->tareamp) && isset($item->tareamp->tareamp_descripcion)){
                            echo $item->tareamp->tareamp_descripcion;
                        }else{
                            echo "";
                        }
                    echo '</td>';

                    echo '<td class="td_css">'.$item->actividadmptareamp_orden.'</td>';
        
                    echo '<td class="td_css">';
                        if (isset($item->actividadmptareamp_puestos) && is_array($item->actividadmptareamp_puestos)){
                            foreach($item->actividadmptareamp_puestos as $puesto){
                                echo $puesto->cantidad." ".$puesto->puestotrabajo_nombre." <br>";    
                            }
                        }else{
                            echo "";
                        }
                    echo '</td>';

                    if(isset($item->actividadmptareamp_recursos) && $item->actividadmptareamp_recursos!=""){

                        //
                        if(isset($item->actividadmptareamp_recursos->tipovehiculo_id) && $item->actividadmptareamp_recursos->tipovehiculo_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipovehiculo_id as $value){
                            echo " ".$value->tipovehiculo_nombre." <br>";
                        }
                        echo '</td>';
                        }else{
                                echo '<td class="td_css">';
                                echo '</td>';
                        }
                        if(isset($item->actividadmptareamp_recursos->tipoequipomovil_id) && $item->actividadmptareamp_recursos->tipoequipomovil_id!=""){
                                    echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoequipomovil_id as $value){
                            echo " ".$value->tipoequipomovil_nombre." <br>";
                        }
                        echo '</td>';
                        }else{
                                echo '<td class="td_css">';
                                echo '</td>';
                        }
                        if(isset($item->actividadmptareamp_recursos->tipoequipomenor_id) && $item->actividadmptareamp_recursos->tipoequipomenor_id!=""){
                                    echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoequipomenor_id as $value){
                            echo " ".$value->tipoequipomenor_nombre." <br>";
                        }
                        echo '</td>';
                        }else{
                                echo '<td class="td_css">';
                                echo '</td>';
                        }
                        if(isset($item->actividadmptareamp_recursos->tipoequipolevante_id) && $item->actividadmptareamp_recursos->tipoequipolevante_id!=""){
                                    echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoequipolevante_id as $value){
                            echo " ".$value->tipoequipolevante_nombre." <br>";
                        }
                        echo '</td>';
                        }else{
                                echo '<td class="td_css">';
                                echo '</td>';
                        }
                        if(isset($item->actividadmptareamp_recursos->tipoherramienta_id) && $item->actividadmptareamp_recursos->tipoherramienta_id!=""){
                                    echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoherramienta_id as $value){
                            echo " ".$value->tipoherramienta_nombre." <br>";
                        }
                        echo '</td>';
                        }else{
                                echo '<td class="td_css">';
                                echo '</td>';
                        }
                        if(isset($item->actividadmptareamp_recursos->tipoequipamientoemergencia_id) && $item->actividadmptareamp_recursos->tipoequipamientoemergencia_id!=""){
                                    echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoequipamientoemergencia_id as $value){
                            echo " ".$value->tipoequipamientoemergencia_nombre." <br>";
                        }
                        echo '</td>';
                        }else{
                                echo '<td class="td_css">';
                                echo '</td>';
                        }
                        if(isset($item->actividadmptareamp_recursos->tipoelementotrabajoaltura_id) && $item->actividadmptareamp_recursos->tipoelementotrabajoaltura_id!=""){
                                    echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoelementotrabajoaltura_id as $value){
                            echo " ".$value->tipoelementotrabajoaltura_nombre." <br>";
                        }
                        echo '</td>';
                        }else{
                                echo '<td class="td_css">';
                                echo '</td>';
                        }
                        if(isset($item->actividadmptareamp_recursos->tipoinstalacion_id) && $item->actividadmptareamp_recursos->tipoinstalacion_id!=""){
                                    echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoinstalacion_id as $value){
                            echo " ".$value->tipoinstalacion_nombre." <br>";
                        }
                        echo '</td>';
                        }else{
                                echo '<td class="td_css">';
                                echo '</td>';
                        }
                        //

                    }else{
                        echo '<td class="td_css">'."".'</td>';
                        echo '<td class="td_css">'."".'</td>';
                        echo '<td class="td_css">'."".'</td>';
                        echo '<td class="td_css">'."".'</td>';
                        echo '<td class="td_css">'."".'</td>';
                        echo '<td class="td_css">'."".'</td>';
                    }

        
                    if (isset($item->riesgos) && is_array($item->riesgos) && sizeof($item->riesgos)>0){
                            echo '<td class="td_css">';
                            foreach($item->riesgos as $value){
                                echo "".$value->riesgo_nombre." <br>";    
                            }
                            echo '</td>';
                    }
                
        
                echo '</tr>';
            }
        }
        if (isset($List) && sizeof($List)==0){
            echo '<tr><th colspan="3">No hay datos</th></tr>';
        }else{

            echo "<tr>";
            echo "<td class='td_css'></td>";            
            echo "</tr>";
            
            echo "<tr>";
            echo "<td class='td_css'>Elaborado por </td>";
            echo "<td class='td_css' colspan='8'>";
            if(isset($actividad->proceso) && isset($actividad->proceso) && isset($actividad->proceso->persona) && isset($actividad->proceso->persona->persona_id)){
                echo "".$actividad->proceso->persona->persona_nombres." ".$actividad->proceso->persona->persona_apellidopaterno." ".$actividad->proceso->persona->persona_apellidomaterno."";
            }else{
                echo "";
            }
            echo "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td class='td_css'>Aprobado por </td>";
            echo "<td class='td_css' colspan='8'>";
            if(isset($actividad->gerencia) && isset($actividad->gerencia) && isset($actividad->gerencia->persona) && isset($actividad->gerencia->persona->persona_id)){
                echo "".$actividad->gerencia->persona->persona_nombres." ".$actividad->gerencia->persona->persona_apellidopaterno." ".$actividad->gerencia->persona->persona_apellidomaterno."";
            }else{
                echo "";
            }
            echo "</td>";
            echo "</tr>";

        }
        ?>
        </tbody>
        <?php
        
        }else
        {
        ?>
        <tr>
            <td class="td_css">N°</td>
            <td class="td_css">ID</td>
            <td class="td_css">Actividad</td>
            <td class="td_css">Orden</td>
            <td class="td_css">Tarea</td>		
            <td class="td_css">Personas</td>
            <td class="td_css">Vehiculos</td>
            <td class="td_css">Equipos Movil</td>
            <td class="td_css">Equipos Menores</td>
            <td class="td_css">Equipo Levante</td>
            <td class="td_css">Herramienta</td>
            <td class="td_css">Equipo Emergencia</td>
            <td class="td_css">Altura</td>
            <td class="td_css">Instalacion</td>	
        </tr>
        <!-- <tbody> -->
        <?php
        $con=0;$sw=0;
        $actividadmptareamp = new ActividadmptareampController(); 
                
        foreach ($List as $itemActividad) {
            $actividad = $itemActividad;
            if($esriesgo == "1"){
                $tareas = $actividadmptareamp->listarPorPaginacionRiesgo(1,2000,'-1',$actividad->procesoactividadmp_id);
            }
            if($esriesgo == "0"){
                $tareas = $actividadmptareamp->listarPorPaginacion(1,2000,'-1',$actividad->procesoactividadmp_id);
            }
            $tareaList = $tareas["lista"];

            $color = "#FFF";
            if($sw==0){
                $sw=1;
            }else{
                $color = "#F7F7F7";
                $sw=0;
            }
            $contadorrtareas = sizeof($tareaList);
            $rowspan = 1;
            if($contadorrtareas > 1){
                $rowspan = $contadorrtareas;
            }
            echo '<tr style="background:'.$color.'">';
            echo "<td class='td_css' rowspan=' ".$rowspan."' >".++$con."</td>";
            echo "<td class='td_css' rowspan=' ".$rowspan."' >".$itemActividad->procesoactividadmp_id."</td>";
            
            if(isset($actividad->actividadmp) && isset($actividad->actividadmp) && isset($actividad->actividadmp->actividadmp_nombre)){
                echo "<td class='td_css' rowspan=' ".$rowspan."' >".$actividad->actividadmp->actividadmp_nombre."</td>";
            }else{
                echo "<td class='td_css' rowspan=' ".$rowspan."' ></td>";
            }

            if(isset($tareaList) && sizeof($tareaList)>0){
                $countTarea = 0;
                foreach ($tareaList as $item) {
                
                    $item->actividadmptareamp_puestos = json_decode($item->actividadmptareamp_puestos);
                    $item->actividadmptareamp_recursos = json_decode($item->actividadmptareamp_recursos);

                    if(isset($item->actividadmptareamp_recursos) && $item->actividadmptareamp_recursos!=""){
                    }else{
                        $item->actividadmptareamp_recursos = new stdClass();
                        $item->actividadmptareamp_recursos->tipovehiculo_id = array();
                        $item->actividadmptareamp_recursos->tipoequipomovil_id = array();
                        $item->actividadmptareamp_recursos->tipoequipomenor_id = array();
                        $item->actividadmptareamp_recursos->tipoequipolevante_id = array();
                        $item->actividadmptareamp_recursos->tipoherramienta_id = array();
                        $item->actividadmptareamp_recursos->tipoequipamientoemergencia_id = array();
                        $item->actividadmptareamp_recursos->tipoelementotrabajoaltura_id = array();
                        $item->actividadmptareamp_recursos->tipoinstalacion_id = array();
                    }

                    if($countTarea >0){
                        echo '<tr>';
                    }

                    echo '<td class="td_css">'.$item->actividadmptareamp_orden.'</td>';
            
                    echo '<td class="td_css">';
                    if (isset($item->tareamp) && isset($item->tareamp->tareamp_nombre)){
                        echo $item->tareamp->tareamp_nombre;
                    }else{
                        echo "";
                    }
                    echo '</td>';

                    echo '<td class="td_css">';
                    if (isset($item->actividadmptareamp_puestos) && is_array($item->actividadmptareamp_puestos)){
                        foreach($item->actividadmptareamp_puestos as $puesto){
                            echo $puesto->cantidad." ".$puesto->puestotrabajo_nombre." <br>";    
                        }
                    }else{
                        echo "";
                    }
                    echo '</td>';

                    if(isset($item->actividadmptareamp_recursos) && $item->actividadmptareamp_recursos!=""){

                            //
                        if(isset($item->actividadmptareamp_recursos->tipovehiculo_id) && $item->actividadmptareamp_recursos->tipovehiculo_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipovehiculo_id as $value){
                                echo " ".$value->tipovehiculo_nombre." <br>";
                            }
                            echo '</td>';
                        }else{
                            echo '<td class="td_css"></td>';
                        }

                        if(isset($item->actividadmptareamp_recursos->tipoequipomovil_id) && $item->actividadmptareamp_recursos->tipoequipomovil_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoequipomovil_id as $value){
                                echo " ".$value->tipoequipomovil_nombre." <br>";
                            }
                            echo '</td>';
                        }else{
                            echo '<td class="td_css"></td>';
                        }

                        if(isset($item->actividadmptareamp_recursos->tipoequipomenor_id) && $item->actividadmptareamp_recursos->tipoequipomenor_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoequipomenor_id as $value){
                                echo " ".$value->tipoequipomenor_nombre." <br>";
                            }
                            echo '</td>';
                        }else{
                            echo '<td class="td_css"></td>';
                        }
                        if(isset($item->actividadmptareamp_recursos->tipoequipolevante_id) && $item->actividadmptareamp_recursos->tipoequipolevante_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoequipolevante_id as $value){
                                echo " ".$value->tipoequipolevante_nombre." <br>";
                            }
                            echo '</td>';
                        }else{
                            echo '<td class="td_css"></td>';
                        }

                        if(isset($item->actividadmptareamp_recursos->tipoherramienta_id) && $item->actividadmptareamp_recursos->tipoherramienta_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoherramienta_id as $value){
                                echo " ".$value->tipoherramienta_nombre." <br>";
                            }
                            echo '</td>';
                        }else{
                            echo '<td class="td_css"></td>';
                        }

                        if(isset($item->actividadmptareamp_recursos->tipoequipamientoemergencia_id) && $item->actividadmptareamp_recursos->tipoequipamientoemergencia_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoequipamientoemergencia_id as $value){
                                echo " ".$value->tipoequipamientoemergencia_nombre." <br>";
                            }
                            echo '</td>';
                        }else{
                            echo '<td class="td_css"></td>';
                        }

                        if(isset($item->actividadmptareamp_recursos->tipoelementotrabajoaltura_id) && $item->actividadmptareamp_recursos->tipoelementotrabajoaltura_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoelementotrabajoaltura_id as $value){
                                echo " ".$value->tipoelementotrabajoaltura_nombre." <br>";
                            }
                            echo '</td>';
                        }else{
                            echo '<td class="td_css"></td>';
                        }
                        
                        if(isset($item->actividadmptareamp_recursos->tipoinstalacion_id) && $item->actividadmptareamp_recursos->tipoinstalacion_id!=""){
                            echo '<td class="td_css">';
                            foreach($item->actividadmptareamp_recursos->tipoinstalacion_id as $value){
                                echo " ".$value->tipoinstalacion_nombre." <br>";
                            }
                            echo '</td>';
                        }else{
                            echo '<td class="td_css"></td>';
                        }
                            //
                        
                        

                        if($countTarea>0){
                            echo '</tr>';
                        }

                            

                    }else{
                            echo '<td class="td_css">'."".'</td>';
                            echo '<td class="td_css">'."".'</td>';
                            echo '<td class="td_css">'."".'</td>';
                            echo '<td class="td_css">'."".'</td>';
                            echo '<td class="td_css">'."".'</td>';
                            echo '<td class="td_css">'."".'</td>';
                            echo '<td class="td_css">'."".'</td>';
                            echo '<td class="td_css">'."".'</td>';
                            echo '<td class="td_css">'."".'</td>';
                    }

                    $countTarea = $countTarea+1;

                }
            }else{

                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
                echo '<td class="td_css">'."".'</td>';
            }

            if($rowspan<2){
                echo '</tr>';
            }
            //echo '</tr>';
        }
     ?>
     <!-- </tbody> -->
    <?php
        }
    ?>
    </table>

<?php
    }
   }
?>


</body>
</html>