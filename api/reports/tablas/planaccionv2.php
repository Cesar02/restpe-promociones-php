<?php

$cdnBase = URL_FILES_UPLOAD;
$objParams = array();
$pagina = 0;
$registros = 0;
$objParams["tipoconsulta"] = 1; // para reporte

if(isset($_GET["busqueda"])){
    $objParams["busqueda"] = (String)$_GET["busqueda"];
}

/* if(isset($_GET["pagina"])){
    $objParams["pagina"] = $_GET["pagina"];
    $pagina = $_GET["pagina"];
}

if(isset($_GET["registros"])){
    $objParams["registros"] = $_GET["registros"];
    $registros = $_GET["registros"];
} */

// OTHERS INICIO
if(isset($_GET["empresa_id"])){
    $objParams["empresa_id"] = $_GET["empresa_id"];
}

if(isset($_GET["area_id"])){
    $objParams["area_id"] = $_GET["area_id"];
}

if(isset($_GET["unidadtrabajo_id"])){
    $objParams["unidadtrabajo_id"] = $_GET["unidadtrabajo_id"];
}

if(isset($_GET["proceso_id"])){
    $objParams["proceso_id"] = $_GET["proceso_id"];
}

if(isset($_GET["fechainicio"])){
    $objParams["fechainicio"] = $_GET["fechainicio"];
}

if(isset($_GET["fechafin"])){
    $objParams["fechafin"] = $_GET["fechafin"];
}

if(isset($_GET["encargado"])){
    $objParams["encargado"] = $_GET["encargado"];
}

if(isset($_GET["grupolistaverificacion_id"])){
    $objParams["grupolistaverificacion_id"] = $_GET["grupolistaverificacion_id"];
}

if(isset($_GET["listaverificacion_id"])){
    $objParams["listaverificacion_id"] = $_GET["listaverificacion_id"];
}

if(isset($_GET["estado"])){
    $objParams["estado"] = $_GET["estado"];
}

if(isset($_GET["ejecucion_tipo"])){
    $objParams["ejecucion_tipo"] = $_GET["ejecucion_tipo"];
}

// OTHERS FIN


$ctrl = new PlanaccionController();
$obj = new stdClass();
$array = $ctrl->listarPorPaginacionFiltros($pagina,$registros,$objParams);
$List =  $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
            <th>#</th>
            <th>ID</th>
            <th>Origen</th>
            <th>Descripcion / Accion</th>
            <th>Datos Empresa</th>
            <th>Grupo / Lista</th>
            <th>Recurso</th>
            <th>Responsable Levantamiento</th>
            <th>Fechas</th>
            <th>Estado</th>
            <th>Evidencia antes</th>
            <th>Evidencia despues</th>
            
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			
            echo '<td class="td_css">'.++$con.'</td>';
			
            echo '<td class="td_css">'.$item->planaccion_id.'</td>';
            
            echo '<td class="td_css">';
            echo $item->origen."<br>";
            if(isset($item->ejecucion_id))
                echo "ID : ".$item->ejecucion_id;
            echo '</td>';

            echo '<td class="td_css">';
            echo "Descrición: ".$item->planaccion_descripcionnc."<br>";
            echo "Acción: ".$item->planaccion_accion;
            echo '</td>';

            echo '<td class="td_css">';
            echo "Empresa ".(isset($item->datosempresa->empresa->empresa_nombre) ?  $item->datosempresa->empresa->empresa_nombre : '')."<br>";
            echo "Gerencia: ".(isset($item->datosempresa->area->area_nombre) ?  $item->datosempresa->area->area_nombre : '')."<br>";
            echo "Área: ".(isset($item->datosempresa->proceso->proceso_nombre) ?  $item->datosempresa->proceso->proceso_nombre : '')."<br>";
            echo "UT: ".(isset($item->datosempresa->unidadtrabajo->unidadtrabajo_nombre) ?  $item->datosempresa->unidadtrabajo->unidadtrabajo_nombre : '')."<br>";
            echo '</td>';

            echo '<td class="td_css">';
            echo "Grupo: ".$item->grupolistaverificacion_nombre."<br>";
            echo "Lista: ".$item->listaverificacion_nombre;
            echo '</td>';

            echo '<td class="td_css">';
            if(isset($item->recurso)){
                echo "Nombre: ".$item->recurso->recurso_label."<br>";
                echo "Tipo: ".$item->recurso->recurso_nombretipo."<br>";
                echo "ID: ".$item->recurso->recurso_id;
            }else{
                echo "Nombre:- <br>";
                echo "Tipo:-<br>";
                echo "ID:-";
            }
            echo '</td>';

            echo '<td class="td_css">';
            echo "Empresa: ".(isset($item->persona->empresa->empresa_nombre) ?  $item->persona->empresa->empresa_nombre : '')."<br>";
            echo "Área: ".(isset($item->persona->proceso->proceso_nombre) ?  $item->persona->proceso->proceso_nombre : '')."<br>";
            echo "Responsable: ".isset($item->persona->persona_nombres) ?  "{$item->persona->persona_apellidopaterno} {$item->persona->persona_apellidomaterno} {$item->persona->persona_nombres}" : ''."<br>";
            echo '</td>';

            echo '<td class="td_css">';
            echo "Registro: ".$item->planaccion_fecharegistrosistema."<br>";
            echo "Plazo: ".$item->planaccion_plazo."<br>";
            echo "Cierre: ".$item->planaccion_culminacion."<br>";
            echo '</td>';

            echo '<td class="td_css">';
            if($item->planaccion_estadoejecucion=='0'){
                echo "Pendiente <br>";
                echo $item->dentrofueraplazo;
            }
            if($item->planaccion_estadoejecucion=='2'){
                echo "Revision <br>";
                echo $item->dentrofueraplazo;
            }
            if($item->planaccion_estadoejecucion=='3'){
                echo "Observación <br>";
                echo $item->dentrofueraplazo;
            }
            if($item->planaccion_estadoejecucion=='1'){
                echo "Culminado <br>";
                echo $item->dentrofueraplazo;
            }
            echo '</td>';


            echo '<td class="td_css">';
            if(isset($item->planaccion_evidencia)){
                if(Utility::evidenciaPlanAccionEsImagen($item->planaccion_evidencia)){
                    echo "<img class='img-responsive' src='{$cdnBase}/{$item->planaccion_evidencia}' alt='{$item->planaccion_evidencia}' width='50 'height='50'>";
                }else{
                    echo "Archivo <br>";
                    echo "{$cdnBase}/{$item->planaccion_evidencia}";
                }
            }else{
                echo "Sin evidencia";
            }
            echo '</td>';

            echo '<td class="td_css">';
            if(isset($item->evidenciacierre->planaccionevidencia_urlevidencia)){
                if(Utility::evidenciaPlanAccionEsImagen($item->evidenciacierre->planaccionevidencia_urlevidencia)){
                    echo "<img class='img-responsive' src='{$cdnBase}/{$item->evidenciacierre->planaccionevidencia_urlevidencia}' alt='{$item->evidenciacierre->planaccionevidencia_urlevidencia}' width='50 'height='50'>";
                }else{
                    echo "Archivo <br>";
                    echo "{$cdnBase}/{$item->planaccion_evidencia}";
                }
            }else{
                echo "Sin evidencia";
            }
            echo '</td>';


        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="4">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>