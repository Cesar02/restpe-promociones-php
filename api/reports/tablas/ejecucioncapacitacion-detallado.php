<?php

$ctrl = new EjecucionController();

$objParams = array();


if(isset($_GET["persona_id"])){
    $objParams["persona_id"] = $_GET["persona_id"];
}

if(isset($_GET["fecha"])){
    $objParams["fecha"] = $_GET["fecha"];
}

if(isset($_GET["fechafin"])){
    $objParams["fechafin"] = $_GET["fechafin"];
}

if(isset($_GET["tarea_id"])){
    $objParams["tarea_id"] = $_GET["tarea_id"];
}

if(isset($_GET["empresa_id"])){
    $objParams["empresa_id"] = $_GET["empresa_id"];
}

if(isset($_GET["ejecucion_esprogramada"])){
    $objParams["ejecucion_esprogramada"] = $_GET["ejecucion_esprogramada"];
}

$array = $ctrl->poslistarPorPaginacionTipo(0,0,"",CAPACITACION,$objParams);


$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
		<th style="text-align: center">#</th>
		<th style="text-align: center">Empresa</th>
		<th style="text-align: center">ID</th>
		<th style="text-align: center">Programada</th>
		<th style="text-align: center">Año</th>		
		<th style="text-align: center">Mes</th>		
		<th style="text-align: center">Fecha</th>			
		<th style="text-align: center">Capacitador</th>			
		<th style="text-align: center">Tipo</th>			
		<th style="text-align: center">Tema</th>
		<th style="text-align: center">Modalidad</th>			
		<th style="text-align: center">Duracion (Minutos)</th>
		
		<th style="text-align: center">Nombres</th>
		<th style="text-align: center">DNI</th>
		<th style="text-align: center">Puesto</th>
		<th style="text-align: center">Área</th>
		<th style="text-align: center">Unidad de trabajo</th>
		<th style="text-align: center">Nota</th>

    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

		
		$item->totalRowSpanHtml = "";

		$item->personas = Ejecucionpersona::getPersonasByEjecucionIDv2($item->ejecucion_id);

		if(is_array($item->personas) && sizeof($item->personas)>0){	
			foreach($item->personas as $value){

				$value->persona_fullname = Utility::camelCaseIam($value->persona_apellidopaterno)." ".Utility::camelCaseIam($value->persona_apellidomaterno).", ".Utility::camelCaseIam($value->persona_nombres);
				
				$value->unidadtrabajo = Unidadtrabajo::getById($value->unidadtrabajo_id);

				$value->puestotrabajo = Puestotrabajo::getById($value->puestotrabajo_id);

				$value->area = Proceso::getById($value->proceso_id);

			}
		}

		$item->totalRowSpan = sizeof($item->personas);

		if($item->totalRowSpan>0){
			$item->totalRowSpanHtml = " rowspan='".$item->totalRowSpan."' ";
		}

        echo '<tr style="background:'.$color.'">';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.++$con.'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.$item->empresa_nombre.'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.$item->ejecucion_id.'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if($item->ejecucion_esprogramada==SI)
			echo "SI";
		else
			echo "NO";
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.Utility::getFechaSegunFormato(isset($item->programacionfecha->programacionfecha_fechainicio) ? $item->programacionfecha->programacionfecha_fechainicio : $item->ejecucion_fechahorainicio,'Y').'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.Utility::getFechaSegunFormato(isset($item->programacionfecha->programacionfecha_fechainicio) ? $item->programacionfecha->programacionfecha_fechainicio : $item->ejecucion_fechahorainicio,'m').'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.Utility::getFechaFormatoEjecucionReporte($item->ejecucion_fechahorainicio).'</td>';
		
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if(isset($item->capacitador->persona_id))
			echo Utility::camelCaseIam($item->capacitador->persona_apellidopaterno)." ".Utility::camelCaseIam($item->capacitador->persona_apellidomaterno).",".Utility::camelCaseIam($item->capacitador->persona_nombres);
		echo '</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if(isset($item->actividad->actividad_id))
			echo $item->actividad->actividad_nombre;
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if(isset($item->tarea->tarea_id))
			echo $item->tarea->tarea_nombre;
		echo '</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if($item->ejecucion_modalidad=="1") // INTERNA
			echo "Interna";
		else
			echo "Externa";
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		echo $item->ejecucion_duracion;
		echo '</td>';

		
		if($item->totalRowSpan>0){

			$contadorRows = 0;

			foreach($item->personas as $persona){

				if($contadorRows >0){
					echo '<tr>';
				}

				echo '<td class="td_css">';
				echo $persona->persona_fullname;
				echo'</td>';
				
				echo '<td class="td_css">';
				echo $persona->persona_dni;
				echo'</td>';
				
				echo '<td class="td_css">';
				if(isset($persona->puestotrabajo->puestotrabajo_nombre))
					echo $persona->puestotrabajo->puestotrabajo_nombre;
				echo'</td>';
				
				echo '<td class="td_css">';
				if(isset($persona->area->proceso_nombre))
					echo $persona->area->proceso_nombre;
				echo'</td>';
				
				echo '<td class="td_css">';
				if(isset($persona->unidadtrabajo->unidadtrabajo_nombre))
					echo $persona->unidadtrabajo->unidadtrabajo_nombre;
				echo'</td>';
				
				echo '<td class="td_css">';
				echo $persona->nota;
				echo'</td>';
				
				if($contadorRows>0){
					echo '</tr>';
				}

				$contadorRows = $contadorRows+1;

			}
		}else{
			echo '<td class="td_css">'."".'</td>';
            echo '<td class="td_css">'."".'</td>';
            echo '<td class="td_css">'."".'</td>';
            echo '<td class="td_css">'."".'</td>';
            echo '<td class="td_css">'."".'</td>';
            echo '<td class="td_css">'."".'</td>';
		}

	
		// echo '</tr>';
		if($item->totalRowSpan<2){
			echo '</tr>';
		}
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="11">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>