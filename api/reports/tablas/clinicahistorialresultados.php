<?php

$ctrl = new CargaarchivoprogramacionemoController();

$objParams = array();

if(isset($_GET["usuario_id"])){
    $objParams["usuario_id"] = $_GET["usuario_id"];
}

if(isset($_GET["protocolo_id"])){
    $objParams["protocolo_id"] = $_GET["protocolo_id"];
}

if(isset($_GET["clinica_id"])){
    $objParams["clinica_id"] = $_GET["clinica_id"];
}

if(isset($_GET["clinicasede_id"])){
    $objParams["clinicasede_id"] = $_GET["clinicasede_id"];
}

if(isset($_GET["busqueda"])){
    $objParams["busqueda"] = $_GET["busqueda"];
}

if(isset($_GET["fechainicio"])){
    $objParams["fechainicio"] = $_GET["fechainicio"];
}

if(isset($_GET["fechafin"])){
    $objParams["fechafin"] = $_GET["fechafin"];
}

if(isset($_GET["cargaarchivoprogramacionemo_error"])){
    $objParams["cargaarchivoprogramacionemo_error"] = $_GET["cargaarchivoprogramacionemo_error"];
}


$array = $ctrl->postSubidasArchivosByClinicaSedeProtocoloUsuario(0,0,$objParams);

$List =  $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
            <th>#</th>
            <th>Codigo</th>
            <th>Clinica</th>
            <th>Sede</th>
            <th>Usuario</th>
            <th>Fecha</th>
            <th>Personas Cargadas</th>
            <th>Personas Cargadas exitosamente</th>
            <th>Estado</th>
            
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			echo '<td class="td_css">'.++$con.'</td>';
			
            echo '<td class="td_css">';
            echo $item->cargaarchivoprogramacionemo_id;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->clinica_nombre;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->clinicasede_nombre;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->persona_fullname;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->cargaarchivoprogramacionemo_fecharegistrosistema;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->cargaarchivoprogramacionemo_totalprogramaciones;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->cargaarchivoprogramacionemo_totalprogramacionesexitosas;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->cargaarchivoprogramacionemo_error=='1' ? 'Error' : 'Exitoso';
            echo '</td>';

        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="4">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>