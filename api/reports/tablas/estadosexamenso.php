<?php

$ctrl = new ProgramacionemopersonaController();

$objParams = array();

if(isset($_GET["empresa_id"])){
    $objParams["empresa_id"] = $_GET["empresa_id"];
}

if(isset($_GET["puestotrabajo_id"])){
    $objParams["puestotrabajo_id"] = $_GET["puestotrabajo_id"];
}

if(isset($_GET["clinica_id"])){
    $objParams["clinica_id"] = $_GET["clinica_id"];
}

if(isset($_GET["clinicasede_id"])){
    $objParams["clinicasede_id"] = $_GET["clinicasede_id"];
}

if(isset($_GET["tipoexamenocupacional_id"])){
    $objParams["tipoexamenocupacional_id"] = $_GET["tipoexamenocupacional_id"];
}

if(isset($_GET["fechainicio"])){
    $objParams["fechainicio"] = $_GET["fechainicio"];
}

if(isset($_GET["fechafin"])){
    $objParams["fechafin"] = $_GET["fechafin"];
}

if(isset($_GET["programacionemopersona_programacionemopersonaestado"])){
    $objParams["programacionemopersona_programacionemopersonaestado"] = $_GET["programacionemopersona_programacionemopersonaestado"];
}

if(isset($_GET["busqueda"])){
    $objParams["busqueda"] = $_GET["busqueda"];
}

$array = $ctrl->postlistarPorPaginacionAll(0,0,$objParams);

$List =  $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
            <th>#</th>
            <th>Codigo</th>
            <th>Nombres</th>
            <th>Empresa</th>
            <th>Puesto Trabajo</th>
            <th>Protocolo</th>
            <th>Clinica</th>
            <th>Sede de Atención</th>
            <th>Fecha de Solicitud</th>
            <th>Fecha de Atención</th>
            <th>Fecha de Reprogramación</th>
            <th>Fecha de Ejecución</th>
            <th>Estado</th>
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			echo '<td class="td_css">'.++$con.'</td>';
			
            echo '<td class="td_css">';
            echo $item->programacionemopersona_id;
            echo '</td>';

            echo '<td class="td_css">';
            if(isset($item->persona->persona_fullname))
                echo $item->persona->persona_fullname;
            echo '</td>';

            echo '<td class="td_css">';
            if(isset($item->empresa->empresa_nombre))
                echo $item->empresa->empresa_nombre;
            echo '</td>';

            echo '<td class="td_css">';
            if(isset($item->puestotrabajo->puestotrabajo_nombre))
                echo $item->puestotrabajo->puestotrabajo_nombre;
            echo '</td>';

            echo '<td class="td_css">';
            if(isset($item->protocolo->protocolo_nombre))
                echo $item->protocolo->protocolo_nombre;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->clinica_nombre;
            echo '</td>';

            echo '<td class="td_css">';
            echo $item->clinicasede_nombre;
            echo '</td>';

            echo '<td class="td_css">';
            echo Utility::getFechaSegunFormato($item->programacionemo_fecharegistro,'d-m-Y');
            echo '</td>';

            echo '<td class="td_css">';
            echo Utility::getFechaSegunFormato($item->programacionemo_fechaatencion,'d-m-Y');
            echo '</td>';

            echo '<td class="td_css">';
            if(isset( $item->programacionemopersona_fechareprogramacion))
                echo Utility::getFechaSegunFormato($item->programacionemopersona_fechareprogramacion,'d-m-Y');
            echo '</td>';

            echo '<td class="td_css">';
            echo Utility::getFechaSegunFormato($item->programacionemopersona_fechaejecucion,'d-m-Y');
            echo '</td>';


            echo '<td class="td_css">';
            if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_SOLICITADO){
                echo "Solicitado";
            }
            else if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_PORCONFIRMAR){
                echo "Reprogramar";
            }else if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_ACEPTADO){
                echo "Programado";
            }else if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_RECHAZADO){
                echo "Rechazado";
            }else if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_REGISTROCARGADO){
                echo "Registro Cargado";
            }else if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_EVIDENCIACARGADA){
                echo "Archivo Cargado";
            }else if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_VALIDA){
                echo "EMO Valido";
            }else if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_OBSERVADA){
                echo "EMO Observado";
            }else if($item->programacionemopersona_programacionemopersonaestado == PEMOPEST_EJECUTADA){
                echo "Ejecutado";
            }
            echo '</td>';

        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="4">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>