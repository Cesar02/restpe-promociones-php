<?php

$ejecucion_id = PARAM_TODOS;

$objDatosIdentificacion = new stdClass();

$objDatosIdentificacion->empresa = "";
$objDatosIdentificacion->fecha = "";
$objDatosIdentificacion->liderarea = "";
$objDatosIdentificacion->area = "";
$objDatosIdentificacion->lidergrupo = "";
$objDatosIdentificacion->grupo = "";
$objDatosIdentificacion->identificadorrecurso = "";
$objDatosIdentificacion->ubicacion = "";

$objPlanesAccion = array();

$objCpacitadoresFirma = new stdClass();

$objCpacitadoresFirma->ejecutornombre = "";
$objCpacitadoresFirma->ejecutorurlfirma = "";
$objCpacitadoresFirma->ejecutorut = "";
$objCpacitadoresFirma->responsablenombre = "";
$objCpacitadoresFirma->responsablefirma = "";
$objCpacitadoresFirma->responsableut = "";

if(isset($_GET["ejecucion_id"])){
    $ejecucion_id = (String)$_GET["ejecucion_id"];
}else{
    exit("Ejecucion no Encontrada");
}

$ctrl = new EjecucionController();

$response = $ctrl->getById($ejecucion_id);

$datos = $response["data"];

$array_final = array();

$items = array();

if(isset($datos->empresa_id)){
    $empresa = Empresa::getById($datos->empresa_id);
    if(isset($empresa->empresa_nombre))
        $objDatosIdentificacion->empresa = $empresa->empresa_nombre;
}

$objDatosIdentificacion->fecha = $datos->ejecucion_fechahorainicio;

if(isset($datos->unidadtrabajo_id)){
    $unidadtrabajo = Unidadtrabajo::getById($datos->unidadtrabajo_id);
    if(isset($unidadtrabajo->unidadtrabajo_id)){
        $objDatosIdentificacion->grupo = $unidadtrabajo->unidadtrabajo_nombre;
        $proceso = Proceso::getById($datos->unidadtrabajo_id);
        if(isset($proceso->proceso_id)){
            // $proceso_nombre = $proceso->proceso_nombre;
            $objDatosIdentificacion->area = $proceso->proceso_nombre;

            if(isset($proceso->persona_id)){
                $procesopersona = Persona::getById($proceso->persona_id);
                if(isset($procesopersona->persona_id)){
                    $objDatosIdentificacion->liderarea = Utility::camelCaseIam($procesopersona->persona_apellidopaterno)." ".Utility::camelCaseIam($procesopersona->persona_apellidomaterno).", ".Utility::camelCaseIam($procesopersona->persona_nombres);
                }

            }
        }
    }
}


if(isset($datos->personaResponsabeUnidadTrabajo->personafullname)){

    $datos->personaResponsabeUnidadTrabajo->unidadtrabajo_nombre = "";
    $unitdatrbajoObj = Unidadtrabajo::getById($datos->personaResponsabeUnidadTrabajo->unidadtrabajo_id);
    if(isset($unitdatrbajoObj->unidadtrabajo_id)){
        $objCpacitadoresFirma->responsableut = $unitdatrbajoObj->unidadtrabajo_nombre;
    }
    $objDatosIdentificacion->lidergrupo = $datos->personaResponsabeUnidadTrabajo->personafullname;
    $objCpacitadoresFirma->responsablenombre = $datos->personaResponsabeUnidadTrabajo->personafullname;
}

if(isset($datos->capacitador->persona_fullname)){
    $datos->personaResponsabeUnidadTrabajo->unidadtrabajo_nombre = "";
    $unitdatrbajoObj = Unidadtrabajo::getById($datos->capacitador->unidadtrabajo_id);
    if(isset($unitdatrbajoObj->unidadtrabajo_id)){
        $objCpacitadoresFirma->ejecutorut = $unitdatrbajoObj->unidadtrabajo_nombre;
    }
    $objCpacitadoresFirma->ejecutornombre = $datos->capacitador->persona_fullname;
}


if(isset($datos->ejecucion_urlfirma)){
    $objCpacitadoresFirma->ejecutorurlfirma = $datos->ejecucion_urlfirma;
}

if(isset($datos->ejecucion_urlfirmaunidadtrabajo)){
    $objCpacitadoresFirma->responsablefirma = $datos->ejecucion_urlfirmaunidadtrabajo;
}

$objDatosIdentificacion->ubicacion = $datos->ejecucion_ubicacion;

$recurso_virtualtiponombre = "";
$recurso_identificador = "";
$recurso_virtualtiporecursonombre = "";
		
if(isset($datos->detalle->recurso_id) && isset($datos->detalle->tiporecurso)){
    $datos->recurso = RecursoUtilityController::getByIdReucrsoAndTipoToEjecucionProgramacionv3($datos->detalle->tiporecurso,$datos->detalle->recurso_id);
    if(isset($datos->recurso)){
        $recurso_virtualtiponombre = $datos->recurso->tiporecursovirtualnombre;
        $recurso_identificador = $datos->recurso->tiporecursovirtualidentificador;

        if(isset($datos->recurso->recurso_virtualtiporecursonombre)){
            $recurso_virtualtiporecursonombre = $datos->recurso->recurso_virtualtiporecursonombre;
        } 
    }
}

$objDatosIdentificacion->identificadorrecurso = $recurso_virtualtiponombre." - ".$recurso_virtualtiporecursonombre." - ".$recurso_identificador;

if(isset($datos->listaverficacion->items) && is_array($datos->listaverficacion->items) && sizeof($datos->listaverficacion->items)>0){
    
    foreach($datos->listaverficacion->items as $itemlv){
        $itemlv->listaverificacionitem_descripcion = "";
        $itemlv->listaverificaciongrupo_id = "";

        $listaverificacionObj = Listaverificacionitem::getById($itemlv->listaverificacionitem_id);
        
        if(isset($listaverificacionObj->listaverificacionitem_id)){
            $itemlv->listaverificacionitem_descripcion = $listaverificacionObj->listaverificacionitem_descripcion;
            $itemlv->listaverificaciongrupo_id = $listaverificacionObj->listaverificaciongrupo_id;
        }
    }
    
    $agrupados = Utility::_group_by($datos->listaverficacion->items,'listaverificaciongrupo_id');
    
    foreach ($agrupados as $grupo){
        $obj = new stdClass();
        $objGrupo = Listaverificaciongrupo::getById($grupo->listaverificaciongrupo_id);
        $obj->listaverificaciongrupo_id = $objGrupo->listaverificaciongrupo_id;
        $obj->listaverificaciongrupo_nombre = $objGrupo->listaverificaciongrupo_nombre;
        $obj->items = array();

        foreach($datos->listaverficacion->items as $item){
            if($item->listaverificaciongrupo_id == $grupo->listaverificaciongrupo_id){
                //$item->valor = '1';
                //$item->planesaccion = array();
                if(isset($item->planesAccion) && is_array($item->planesAccion) && sizeof($item->planesAccion)>0 ){
                    foreach($item->planesAccion as $pac){
                        $pac->persona_fullname = "";
                        $pac->listaverificacionitem_descripcion = $item->listaverificacionitem_descripcion;
                        if(isset($pac->persona_id->persona_fullname)){
                            $pac->persona_fullname = $pac->persona_id->persona_fullname;
                        }
                        $objPlanesAccion[] = $pac;
                    }
                }
                $obj->items[] = $item;
            }
        }
        $array_final[] = $obj;
    }

}
?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">

    <tr>
        <td rowspan="3" class="td_css">
            <img height="60" width="60" style="width:60px !important; height:60px !important"  class="img img-responsive" src="http://aunor.portafolioitdast.com/aunor/assets/images/logo-light.png" alt="logo">
        </td>
        <td rowspan="3" colspan="3" class="td_css">
            <?php
                echo "Matriz evaluación de riesgos";
            ?>
        </td>
        <td  class="td_css">
             Codigo:
        </td>	
        </tr>
        <tr>
        <td class="td_css">
            Version:
        </td>	
    </tr>
    <tr>
        <td class="td_css">
            Fecha: 
        </td>	
    </tr>

    <tr>
        <th style="text-align: center;" colspan="5"></th>
    </tr>

    <tr>
        <th style="text-align: center;background:#51c72f;color: white;" colspan="5">Identificación</th>
    </tr>

    <tr>
        <td class="td_css" style="text-align: initial;background:#80808036">Empresa</td>
        <td class="td_css" colspan="2" style="text-align: initial;"><?php echo  $objDatosIdentificacion->empresa ?></td>
        <td class="td_css" style="text-align: initial;background:#80808036">Fecha</td>
        <td class="td_css" style="text-align: initial;"><?php echo  $objDatosIdentificacion->fecha ?></td>	
    </tr>

    <tr>
        <td class="td_css" style="text-align: initial;background:#80808036">Lider de Área</td>
        <td class="td_css" colspan="2" style="text-align: initial;"><?php echo  $objDatosIdentificacion->liderarea ?></td>
        <td class="td_css" style="text-align: initial;background:#80808036">Área</td>
        <td class="td_css" style="text-align: initial;"><?php echo  $objDatosIdentificacion->area ?></td>	
    </tr>

    <tr>
        <td class="td_css" style="text-align: initial;background:#80808036">Lider de grupo</td>
        <td class="td_css" colspan="2" style="text-align: initial;"><?php echo  $objDatosIdentificacion->lidergrupo ?></td>
        <td class="td_css" style="text-align: initial;background:#80808036">Grupo</td>
        <td class="td_css" style="text-align: initial;"><?php echo  $objDatosIdentificacion->grupo ?></td>	
    </tr>

    <tr>
        <td class="td_css" style="text-align: initial;background:#80808036">Identificador de recurso</td>
        <td class="td_css" colspan="2" style="text-align: initial;"><?php echo  $objDatosIdentificacion->identificadorrecurso ?></td>
        <td class="td_css" style="text-align: initial;background:#80808036">Ubicación</td>
        <td class="td_css" style="text-align: initial;"><?php echo  $objDatosIdentificacion->ubicacion ?></td>	
    </tr>


    <tr>
        <th style="text-align: center;" colspan="5"></th>
    </tr>

    <tr>
        <th style="text-align: center;background:#51c72f;color: white;" colspan="5">ITEMS DE VERIFICACIÓN</th>
    </tr>

    <tr>
        <th style="text-align: center;background:#80808036" colspan="4">Items</th>	
        <th style="text-align: center;background:#80808036">Estado</th>	
    </tr>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach($array_final as $grupo){
        echo '<tr>';
        echo '<td class="td_css" colspan="5" style="text-align: start;background:#8080800f">Grupo: '.$grupo->listaverificaciongrupo_nombre.'</td>';
        echo '<tr>';
        foreach ($grupo->items as $item) {
            echo '<tr>';
                echo '<td class="td_css" colspan="4" style="text-align: start;">'.$item->listaverificacionitem_descripcion.'</td>';
                if($item->ejecucionlistaverificacionitem_valor == "1"){
                    echo '<td class="td_css">Cumple</td>';
                }else if($item->ejecucionlistaverificacionitem_valor == "2"){
                    echo '<td class="td_css">No cumple</td>';
                }else if($item->ejecucionlistaverificacionitem_valor == "3"){
                    echo '<td class="td_css">No aplica</td>';
                }else{
                    echo '<td class="td_css"></td>';
                }
                
            echo '</tr>';
        }
    }
    if (sizeof($array_final)==0){
        echo '<tr><th colspan="5">No hay datos</th></tr>';
    }

    ?>
    </tbody>

    <tr>
        <th style="text-align: center" colspan="5">Estado de Evaluación: Conforme (C) - Elemento en situación de regularidad/conformidad / No Conforme (NC) - Elementos en situación de regularidad parcial o no cumplidos / No aplicable (NA) Elemento no aplicable según la actividad que realiza									</th>
    </tr>

    <tr>
        <th style="text-align: center;" colspan="5"></th>
    </tr>
    <tr>
        <th style="text-align: center;background:#51c72f;color: white;" colspan="5">Resultado</th>
    </tr>

    <tr>
        <th style="text-align: center;background:#80808036" colspan="4">Evaluación</th>
        <th style="text-align: center;background:#80808036" >Acciones</th>
    </tr>
    <tr>
        <th style="text-align: initial;" colspan="4"># Items Conformes:</th>
        <th style="text-align: center" >Actividad conforme</th>
    </tr>
    <tr>
        <th style="text-align: initial;" colspan="4"># Items No Conformes - Bajo Potencia:</th>
        <th style="text-align: center" >Corrección en plazo</th>
    </tr>
    <tr>
        <th style="text-align: initial;" colspan="4"># Items No Conformes - Medio Potencia de pérdida:</th>
        <th style="text-align: center" >Correción inmediata</th>
    </tr>
    <tr>
        <th style="text-align: initial;" colspan="4"># Items No Conformes - Alto Potencial de pérdida:</th>
        <th style="text-align: center" >Paralización</th>
    </tr>

    <tr>
        <th style="text-align: center;" colspan="5"></th>
    </tr>

    <tr>
        <th style="text-align: center;background:#51c72f;color: white;" colspan="5">Acciones Correctivas</th>
    </tr>
    <tr>
        <th style="text-align: center;background:#80808036" >Item NC</th>
        <th style="text-align: center;background:#80808036" >Descripcion de no conformidad</th>
        <th style="text-align: center;background:#80808036" >Acción Correctiva</th>
        <th style="text-align: center;background:#80808036" >Responsable</th>
        <th style="text-align: center;background:#80808036" >Plazo</th>
    </tr>

    <?php
        foreach($objPlanesAccion as $pc){
            echo '<tr>';

            echo '<td class="td_css">';
		    echo $pc->listaverificacionitem_descripcion;
		    echo '</td>';

            echo '<td class="td_css">';
		    echo $pc->planaccion_descripcionnc;
		    echo '</td>';

            echo '<td class="td_css">';
		    echo $pc->planaccion_accion;
		    echo '</td>';

            echo '<td class="td_css">';
		    echo $pc->persona_fullname;
		    echo '</td>';

            echo '<td class="td_css">';
		    echo $pc->planaccion_plazo;
		    echo '</td>';


            echo '<tr>';
        }
    ?>



    <tr>
        <th style="text-align: center;" colspan="5"></th>
    </tr>

    <tr>
        <th style="text-align: center;background:#51c72f;color: white;" colspan="5">Evaluación</th>
    </tr>
    
    <tr>
        <th style="text-align: center;background:#80808036;;" colspan="3">Nombre</th>
        <th style="text-align: center;background:#80808036;;" >Puesto</th>
        <th style="text-align: center;background:#80808036;;" >Firma</th>
    </tr>
    
    <tr>
        <th style="text-align: center;height:80px !important;" colspan="3"><?php echo  $objCpacitadoresFirma->ejecutornombre ?></th>
        <th style="text-align: center;height:80px !important;" ><?php echo  $objCpacitadoresFirma->ejecutorut ?></th>
        <th style="text-align: center;height:80px !important;" >
            <img height="60" width="60" style="width:60px !important; height:60px !important"  class="img img-responsive" src="<?php echo  URL_FILES.$objCpacitadoresFirma->ejecutorurlfirma ?>" alt="firmacap">
        </th>
    </tr>

    <tr>
        <th style="text-align: center;height:80px !important;" colspan="3"><?php echo  $objCpacitadoresFirma->responsablenombre ?></th>
        <th style="text-align: center;height:80px !important;" ><?php echo  $objCpacitadoresFirma->responsableut ?></th>
        <th style="text-align: center;height:80px !important;" >
            <img height="60" width="60" style="width:60px !important; height:60px !important"  class="img img-responsive" src="<?php echo  URL_FILES.$objCpacitadoresFirma->responsablefirma ?>" alt="firmaresp">
        </th>
    </tr>

</table>

</body>
</html>