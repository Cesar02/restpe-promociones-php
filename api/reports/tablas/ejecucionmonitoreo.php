<?php

$ctrl = new EjecucionController();

$objParams = array();

if(isset($_GET["ejecucion_modalidad"])){
    $objParams["ejecucion_modalidad"] = $_GET["ejecucion_modalidad"];
}

if(isset($_GET["persona_id"])){
    $objParams["persona_id"] = $_GET["persona_id"];
}

if(isset($_GET["fecha"])){
    $objParams["fecha"] = $_GET["fecha"];
}

if(isset($_GET["fechafin"])){
    $objParams["fechafin"] = $_GET["fechafin"];
}

if(isset($_GET["tarea_id"])){
    $objParams["tarea_id"] = $_GET["tarea_id"];
}

if(isset($_GET["empresa_id"])){
    $objParams["empresa_id"] = $_GET["empresa_id"];
}

if(isset($_GET["ejecucion_esprogramada"])){
    $objParams["ejecucion_esprogramada"] = $_GET["ejecucion_esprogramada"];
}

if(isset($_GET["busqueda"])){
    $objParams["busqueda"] = $_GET["busqueda"];
}

if(isset($_GET["proceso_id"])){
    $objParams["proceso_id"] = $_GET["proceso_id"];
}

if(isset($_GET["unidadtrabajo_id"])){
    $objParams["unidadtrabajo_id"] = $_GET["unidadtrabajo_id"];
}

if(isset($_GET["listaverificacion_id"])){
    $objParams["listaverificacion_id"] = $_GET["listaverificacion_id"];
}

$array = $ctrl->poslistarPorPaginacionTipo(0,0,isset($objParams["busqueda"]) ? $objParams["busqueda"] : "",MONITOREO,$objParams);


$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
		<th style="text-align: center">#</th>
		<th style="text-align: center">Empresa</th>
		<th style="text-align: center">ID</th>
		<th style="text-align: center">Programada</th>
		<th style="text-align: center">Año</th>		
		<th style="text-align: center">Mes</th>		
		<th style="text-align: center">Fecha</th>			
		<th style="text-align: center">Ejecutor</th>			
		<th style="text-align: center">Tipo</th>			
		<th style="text-align: center">Componente</th>
		<th style="text-align: center">Area</th>
		<th style="text-align: center">Unidad Trabajo</th>
		<th style="text-align: center">Resultado</th>
		<th style="text-align: center">N° No Conformidades</th>
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
		$proceso_nombre = "";
		
		if(isset($item->unidadtrabajo->proceso_id)){
			$proceso = Proceso::getById($item->unidadtrabajo->proceso_id);
			if(isset($proceso->proceso_id)){
				$proceso_nombre = $proceso->proceso_nombre;
			}
		}

        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';

		echo '<td class="td_css">'.++$con.'</td>';
		
		echo '<td class="td_css">'.$item->empresa_nombre.'</td>';
		
		echo '<td class="td_css">'.$item->ejecucion_id.'</td>';
		
		echo '<td class="td_css">';
		if($item->ejecucion_esprogramada==SI)
			echo "SI";
		else
			echo "NO";
		echo '</td>';

		echo '<td class="td_css">'.Utility::getFechaSegunFormato(isset($item->programacionfecha->programacionfecha_fechainicio) ? $item->programacionfecha->programacionfecha_fechainicio : $item->ejecucion_fechahorainicio,'Y').'</td>';
		
		echo '<td class="td_css">'.Utility::getFechaSegunFormato(isset($item->programacionfecha->programacionfecha_fechainicio) ? $item->programacionfecha->programacionfecha_fechainicio : $item->ejecucion_fechahorainicio,'m').'</td>';
		
		echo '<td class="td_css">'.Utility::getFechaFormatoEjecucionReporte($item->ejecucion_fechahorainicio).'</td>';
		
		echo '<td class="td_css">';
		if(isset($item->capacitador->persona_id))
			echo Utility::camelCaseIam($item->capacitador->persona_apellidopaterno)." ".Utility::camelCaseIam($item->capacitador->persona_apellidomaterno).",".Utility::camelCaseIam($item->capacitador->persona_nombres);
		echo '</td>';
		
		echo '<td class="td_css">';
		if(isset($item->actividad->actividad_id))
			echo $item->actividad->actividad_nombre;
		echo '</td>';

		echo '<td class="td_css">';
		if(isset($item->tarea->tarea_id))
			echo $item->tarea->tarea_nombre;
		echo '</td>';
		

		echo '<td class="td_css">';
		echo $proceso_nombre;
		echo '</td>';

		echo '<td class="td_css">';
		if(isset($item->unidadtrabajo->unidadtrabajo_nombre))
			echo $item->unidadtrabajo->unidadtrabajo_nombre;
		echo '</td>';

		echo '<td class="td_css">';
		echo $item->itemnc*1 == 0 ? "Conforme" : "No Conforme";
		echo '</td>';
		
		echo '<td class="td_css">';
		echo $item->itemnc;
		echo '</td>';
		
		
		echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="11">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>