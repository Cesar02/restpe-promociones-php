<?php

/*if(isset($_GET["busqueda"])){
    $busqueda = (String)$_GET["busqueda"];
}else{
    exit("Matriz no Encontrada");
}*/

$docente = new ProgramacionController();
$obj = new stdClass();
$array = $docente->reporteCapacitacion($obj);
$List =  $array["ejecuciones"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
            <th>#</th>
            <th>Id Capacitacion</th>
            <th>Programada</th>
            <th>Unidad Trabajo</th>
            <th>Tema</th>
            <th>Fecha</th>
            <th>Cantidad Participantes</th>		
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';
			echo '<td class="td_css">'.++$con.'</td>';
			echo '<td class="td_css">'.$item->ejecucion_id.'</td>';
			echo '<td class="td_css">';
            if($item->ejecucion_esprogramada == '1'){
                echo "SI";
            }else{
                echo "NO";
            }
            echo'</td>';
            echo '<td class="td_css"></td>';
            echo '<td class="td_css">';
                if(isset($item->tarea->tarea_nombre)){
                    echo $item->tarea->tarea_nombre;
                }else{
                    echo "";
                }
            echo'</td>';
            echo '<td class="td_css">'.$item->ejecucion_fechahorainicio.'</td>';
            echo '<td class="td_css">'.count($item->personas).'</td>';
        echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="4">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>