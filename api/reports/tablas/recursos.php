<?php
	//Agregamos la libreria para genera códigos QR
	require './phpqrcode/qrlib.php';
	
	$tipoRecurso = -1;
	$recursoId = -1;
	$value = null;
	if(isset($_GET["recursoId"])){
		$recursoId = $_GET["recursoId"];
		$ctrl = new RecursoUtilityController();
		$value = $ctrl->getByIdReucrsoAndTipoToEjecucionProgramacionv2($_GET["tiporecurso"],$recursoId);
	}
	
	if(!isset($_GET["recursoId"]) && isset($_GET["tiporecurso"])){
		$tipoRecurso = $_GET["tiporecurso"];
		$ctrl = new RecursoUtilityController();
		$array = $ctrl->getRecursosByTipov2($tipoRecurso);
	}
	
	//Declaramos una carpeta temporal para guardar la imagenes generadas
	$dir = 'temp/';
	
	//Si no existe la carpeta la creamos
	if (!file_exists($dir))
        mkdir($dir);

	//Parametros de Condiguración
	$tamaño = 10; //Tamaño de Pixel
	$level = 'L'; //Precisión Baja
	$framSize = 3; //Tamaño en blanco
	$contenidoBase = "/recurso/"; //Texto
	
	if(!isset($_GET["recursoId"]) && $tipoRecurso != -1){
		echo '<div class="row" style="text-align: center;">' ;

		foreach($array as $value){

			//Declaramos la ruta y nombre del archivo a generar
			$filename = $dir.'recurso'.$value->tiporecursovirtual.$value->recurso_id.'.png';
			$contenido = $contenidoBase.$value->tiporecursovirtual."/".$value->recurso_id;
			echo '<div class="col-md-12 text-center" style="text-align: center;">';
			//Enviamos los parametros a la Función para generar código QR 
			QRcode::png($contenido, $filename, $level, $tamaño, $framSize); 
			//Mostramos la imagen generada
			echo '<img src="'.$dir.basename($filename).'">';
			
			// echo '<span class="text-center">'.$value->recurso_label.'</span>';
			echo RecursoUtilityController::getHtmlByTypeResource($value->tiporecursovirtual,$value);
			echo '</div>';
		}

		echo '</div>';
	}else if($value!=null){
		$filename = $dir.'recurso'.$value->tiporecursovirtual.$value->recurso_id.'.png';
		$contenido = $contenidoBase.$value->tiporecursovirtual."/".$value->recurso_id;
		echo '<div class="col-md-12 text-center" style="text-align: center;">';
		//Enviamos los parametros a la Función para generar código QR 
		QRcode::png($contenido, $filename, $level, $tamaño, $framSize); 
		//Mostramos la imagen generada
		echo '<img src="'.$dir.basename($filename).'">';
		
		// echo '<span class="text-center">'.$value->recurso_label.'</span>';
		echo RecursoUtilityController::getHtmlByTypeResource($value->tiporecursovirtual,$value);
		echo '</div>';

	}
?>
