<?php

$ctrl = new EjecucionController();

$objParams = array();

if(isset($_GET["ejecucion_modalidad"])){
    $objParams["ejecucion_modalidad"] = $_GET["ejecucion_modalidad"];
}

if(isset($_GET["persona_id"])){
    $objParams["persona_id"] = $_GET["persona_id"];
}

if(isset($_GET["fecha"])){
    $objParams["fecha"] = $_GET["fecha"];
}

if(isset($_GET["fechafin"])){
    $objParams["fechafin"] = $_GET["fechafin"];
}

if(isset($_GET["tarea_id"])){
    $objParams["tarea_id"] = $_GET["tarea_id"];
}

if(isset($_GET["empresa_id"])){
    $objParams["empresa_id"] = $_GET["empresa_id"];
}

if(isset($_GET["ejecucion_esprogramada"])){
    $objParams["ejecucion_esprogramada"] = $_GET["ejecucion_esprogramada"];
}

if(isset($_GET["busqueda"])){
    $objParams["busqueda"] = $_GET["busqueda"];
}

if(isset($_GET["proceso_id"])){
    $objParams["proceso_id"] = $_GET["proceso_id"];
}

if(isset($_GET["unidadtrabajo_id"])){
    $objParams["unidadtrabajo_id"] = $_GET["unidadtrabajo_id"];
}

if(isset($_GET["listaverificacion_id"])){
    $objParams["listaverificacion_id"] = $_GET["listaverificacion_id"];
}


if(isset($_GET["unidadtrabajo_id"])){
    $objParams["unidadtrabajo_id"] = $_GET["unidadtrabajo_id"];
}

if(isset($_GET["tipoincidente_id"])){
    $objParams["tipoincidente_id"] = $_GET["tipoincidente_id"];
}

if(isset($_GET["categoriaincidente_id"])){
    $objParams["categoriaincidente_id"] = $_GET["categoriaincidente_id"];
}

$array = $ctrl->poslistarPorPaginacionTipo(0,0,isset($objParams["busqueda"]) ? $objParams["busqueda"] : "",NEARMISS,$objParams);


$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
		<th style="text-align: center">#</th>
		<th style="text-align: center">Empresa</th>
		<th style="text-align: center">ID</th>		
		<th style="text-align: center">Fecha</th>			
		<th style="text-align: center">Reportado por</th>
		
		<th style="text-align: center">Area</th>
		<th style="text-align: center">Unidad Trabajo</th>
		
		<th style="text-align: center">Actividad</th>			
		<th style="text-align: center">Tarea</th>
		<th style="text-align: center">Ubicación</th>

		<th style="text-align: center">Hallazgo</th>
		<th style="text-align: center">Tipo A/C</th>
		<th style="text-align: center">Paralización</th>
		<th style="text-align: center">Potencial de perdida</th>
		<th style="text-align: center">N° AC</th>
		<th style="text-align: center">Estado AC</th>
    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
		$proceso_nombre = "";
		
		if(isset($item->unidadtrabajo->proceso_id)){
			$proceso = Proceso::getById($item->unidadtrabajo->proceso_id);
			if(isset($proceso->proceso_id)){
				$proceso_nombre = $proceso->proceso_nombre;
			}
		}

		$item->procesoactividadmp = null;
		$item->actividadmptareamp = null;
		$item->actividadmp = null;
		$item->tareamp = null;
		$item->actividadmp_nombre = "";
		$item->tareamp_nombre = "";

		if(isset($item->procesoactividadmp_id)){
			$item->procesoactividadmp = Procesoactividadmp::getById($item->procesoactividadmp_id);
			if(isset($item->procesoactividadmp)){
				$item->actividadmp = Actividadmp::getById($item->procesoactividadmp->actividadmp_id);
				if(isset($item->actividadmp)){
					$item->actividadmp_nombre = $item->actividadmp->actividadmp_nombre;
				}
			}
		}
		if(isset($item->actividadmptareamp_id)){
			$item->actividadmptareamp = Actividadmptareamp::getById($item->actividadmptareamp_id);
			if(isset($item->actividadmptareamp)){
				$item->tareamp = Tareamp::getById($item->actividadmptareamp->tareamp_id);
				if(isset($item->tareamp)){
					$item->tareamp_nombre = $item->tareamp->tareamp_nombre;
				}
			}
		}

        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';

		echo '<td class="td_css">'.++$con.'</td>';
		
		echo '<td class="td_css">'.$item->empresa_nombre.'</td>';
		
		echo '<td class="td_css">'.$item->ejecucion_id.'</td>';
		
		echo '<td class="td_css">'.Utility::getFechaFormatoEjecucionReporte($item->ejecucion_fechahorainicio).'</td>';
		
		echo '<td class="td_css">';
		if(isset($item->capacitador->persona_id))
			echo Utility::camelCaseIam($item->capacitador->persona_apellidopaterno)." ".Utility::camelCaseIam($item->capacitador->persona_apellidomaterno).",".Utility::camelCaseIam($item->capacitador->persona_nombres);
		echo '</td>';

		echo '<td class="td_css">';
		echo $proceso_nombre;
		echo '</td>';

		echo '<td class="td_css">';
		if(isset($item->unidadtrabajo->unidadtrabajo_nombre))
			echo $item->unidadtrabajo->unidadtrabajo_nombre;
		echo '</td>';

		echo '<td class="td_css">';
		echo $item->actividadmp_nombre;
		echo '</td>';
		
		echo '<td class="td_css">';
		echo $item->tareamp_nombre;
		echo '</td>';
		

		echo '<td class="td_css">';
		echo $item->ejecucion_ubicacion;
		echo '</td>';


		echo '<td class="td_css">';
		echo '';
		echo '</td>';

		echo '<td class="td_css">';
		echo '';
		echo '</td>';

		echo '<td class="td_css">';
		echo '';
		echo '</td>';

		echo '<td class="td_css">';
		echo '';
		echo '</td>';

		echo '<td class="td_css">';
		echo '';
		echo '</td>';		
		
		echo '<td class="td_css">';
		echo '';
		echo '</td>';
		
		echo '</tr>';
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="11">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>