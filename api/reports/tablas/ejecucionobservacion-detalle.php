<?php

$ctrl = new EjecucionController();

$objParams = array();

if(isset($_GET["ejecucion_modalidad"])){
    $objParams["ejecucion_modalidad"] = $_GET["ejecucion_modalidad"];
}

if(isset($_GET["persona_id"])){
    $objParams["persona_id"] = $_GET["persona_id"];
}

if(isset($_GET["fecha"])){
    $objParams["fecha"] = $_GET["fecha"];
}

if(isset($_GET["fechafin"])){
    $objParams["fechafin"] = $_GET["fechafin"];
}

if(isset($_GET["tarea_id"])){
    $objParams["tarea_id"] = $_GET["tarea_id"];
}

if(isset($_GET["empresa_id"])){
    $objParams["empresa_id"] = $_GET["empresa_id"];
}

if(isset($_GET["ejecucion_esprogramada"])){
    $objParams["ejecucion_esprogramada"] = $_GET["ejecucion_esprogramada"];
}

if(isset($_GET["busqueda"])){
    $objParams["busqueda"] = $_GET["busqueda"];
}

if(isset($_GET["proceso_id"])){
    $objParams["proceso_id"] = $_GET["proceso_id"];
}

if(isset($_GET["unidadtrabajo_id"])){
    $objParams["unidadtrabajo_id"] = $_GET["unidadtrabajo_id"];
}

if(isset($_GET["listaverificacion_id"])){
    $objParams["listaverificacion_id"] = $_GET["listaverificacion_id"];
}

$array = $ctrl->poslistarPorPaginacionTipo(0,0,isset($objParams["busqueda"]) ? $objParams["busqueda"] : "",OBSERVACION,$objParams);


$List = $array["lista"];

?>

<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">        
		.td_css{
			vertical-align: middle;
			text-align: center
		}
    </style>
</head>

<body>

<table style="width: 100%;font-size: 13px;" border="1" cellpadding="10" cellspacing="0">
    <thead>
		<th style="text-align: center">#</th>
		<th style="text-align: center">Empresa</th>
		<th style="text-align: center">ID</th>
		<th style="text-align: center">Programada</th>
		<th style="text-align: center">Año</th>		
		<th style="text-align: center">Mes</th>		
		<th style="text-align: center">Fecha</th>			
		<th style="text-align: center">Inspector</th>			
		<th style="text-align: center">Tipo</th>			
		<th style="text-align: center">Grupo Lv</th>
		<th style="text-align: center">Nombre Lv</th>			
		<th style="text-align: center">Recurso</th>
		<th style="text-align: center">Tipo de recurso</th>
		<th style="text-align: center">Identificador</th>
		<th style="text-align: center">Area</th>
		<th style="text-align: center">Unidad Trabajo</th>
		<th style="text-align: center">Resultado</th>
		<th style="text-align: center">N° No Conformidades</th>

		<th style="text-align: center">Item</th>
		<th style="text-align: center">Nivel</th>

    </thead>
    <tbody>
    <?php
    $con=0;$sw=0;
    foreach ($List as $item) {
        
		$proceso_nombre = "";
		
		if(isset($item->unidadtrabajo->proceso_id)){
			$proceso = Proceso::getById($item->unidadtrabajo->proceso_id);
			if(isset($proceso->proceso_id)){
				$proceso_nombre = $proceso->proceso_nombre;
			}
		}

		$recurso_virtualtiponombre = "";
		$recurso_identificador = "";
		$recurso_virtualtiporecursonombre = "";
		if(isset($item->detalle->recurso_id) && isset($item->detalle->tiporecurso)){
			$item->recurso = RecursoUtilityController::getByIdReucrsoAndTipoToEjecucionProgramacionv3($item->detalle->tiporecurso,$item->detalle->recurso_id);
			if(isset($item->recurso)){
				$recurso_virtualtiponombre = $item->recurso->tiporecursovirtualnombre;
				$recurso_identificador = $item->recurso->tiporecursovirtualidentificador;

				if(isset($item->recurso->recurso_virtualtiporecursonombre)){
					$recurso_virtualtiporecursonombre = $item->recurso->recurso_virtualtiporecursonombre;
				}
			}
		}

		// PARA LISTA DE VERIFICACION 
		
		$item->totalRowSpanHtml = "";

		$item->itemsListaVerficacionNoCumplen = array();

		$item->listaverficacion = Ejecucionlistaverificacion::getByFields(array(
			array('field' => 'ejecuciondetalle_id', 'value' =>$item->detalle->ejecuciondetalle_id , 'operator' => '='),
			array('field' => 'ejecucionlistaverificacion_estado', 'value' =>"1" , 'operator' => '='),
			array('field' => 'ejecucionlistaverificacion_isdelete', 'value' =>"0" , 'operator' => '=')
			))['ejecucionlistaverificacion_array'];

		if( isset($item->listaverficacion) && sizeof($item->listaverficacion)>0){

			$item->listaverficacion = $item->listaverficacion[0];

			$itemsListaVerificacion = Ejecucionlistaverificacionitem::getByFields(array(
				array('field' => 'ejecucionlistaverificacion_id', 'value' =>$item->listaverficacion->ejecucionlistaverificacion_id , 'operator' => '='),
				array('field' => 'ejecucionlistaverificacionitem_estado', 'value' =>"1" , 'operator' => '='),
				array('field' => 'ejecucionlistaverificacionitem_isdelete', 'value' =>"0" , 'operator' => '=')
			))['ejecucionlistaverificacionitem_array'];

			foreach($itemsListaVerificacion as $itemLista){

				$planesAccion = Planaccion::getByFields(array(
					array('field' => 'ejecucionlistaverificacionitem_id', 'value' =>$itemLista->ejecucionlistaverificacionitem_id , 'operator' => '='),
					array('field' => 'planaccion_estado', 'value' =>'1', 'operator' => '='),
					array('field' => 'planaccion_isdelete', 'value' =>'0', 'operator' => '=')
					))['planaccion_array'];

				foreach($planesAccion as $pa){
					if(isset($pa->persona_id)){
						$pa->persona_id = Persona::getById($pa->persona_id);
						if(isset($pa->persona_id->persona_id)){
							$pa->persona_id->persona_fullname = Utility::camelCaseIam($pa->persona_id->persona_apellidopaterno)." ".Utility::camelCaseIam($pa->persona_id->persona_apellidomaterno).", ".Utility::camelCaseIam($pa->persona_id->persona_nombres);
						}
					}
				}
					
				$itemLista->planesAccion = $planesAccion;
			}

			$item->listaverficacion->items = $itemsListaVerificacion;
		}else{
			$item->listaverficacion = null;
		}

		if(isset($item->listaverficacion->items) && is_array($item->listaverficacion->items) && sizeof($item->listaverficacion->items)>0 ){
			foreach($item->listaverficacion->items as $ilv){
				if($ilv->ejecucionlistaverificacionitem_valor == "2"){
					$ilvObj = Listaverificacionitem::getById($ilv->listaverificacionitem_id);
					$ilv->listaverificacionitem_descripcion = "";
					$ilv->listaverificacionitem_nivel = "";
					$ilv->listaverificacionitem_niveltxt = "";
					if(isset($ilvObj->listaverificacionitem_id)){
						$ilv->listaverificacionitem_descripcion = $ilvObj->listaverificacionitem_descripcion;
					    $ilv->listaverificacionitem_nivel = $ilvObj->listaverificacionitem_nivel;
						if($ilv->listaverificacionitem_nivel == '1'){
							$ilv->listaverificacionitem_niveltxt = "Bajo";
						}
						else if($ilv->listaverificacionitem_nivel == '2'){
							$ilv->listaverificacionitem_niveltxt = "Medio";
						}
						else if($ilv->listaverificacionitem_nivel == '3'){
							$ilv->listaverificacionitem_niveltxt = "Alto";
						}	
					}
					$item->itemsListaVerficacionNoCumplen[] = $ilv;
				}
			}
		}

		$item->totalRowSpan = sizeof($item->itemsListaVerficacionNoCumplen);

		if($item->totalRowSpan>0){
			$item->totalRowSpanHtml = " rowspan='".$item->totalRowSpan."' ";
		}

        $color = "#FFF";
        if($sw==0){
            $sw=1;
        }else{
            $color = "#F7F7F7";
            $sw=0;
        }        

        echo '<tr style="background:'.$color.'">';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.++$con.'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.$item->empresa_nombre.'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.$item->ejecucion_id.'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if($item->ejecucion_esprogramada==SI)
			echo "SI";
		else
			echo "NO";
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.Utility::getFechaSegunFormato(isset($item->programacionfecha->programacionfecha_fechainicio) ? $item->programacionfecha->programacionfecha_fechainicio : $item->ejecucion_fechahorainicio,'Y').'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.Utility::getFechaSegunFormato(isset($item->programacionfecha->programacionfecha_fechainicio) ? $item->programacionfecha->programacionfecha_fechainicio : $item->ejecucion_fechahorainicio,'m').'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>'.Utility::getFechaFormatoEjecucionReporte($item->ejecucion_fechahorainicio).'</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if(isset($item->capacitador->persona_id))
			echo Utility::camelCaseIam($item->capacitador->persona_apellidopaterno)." ".Utility::camelCaseIam($item->capacitador->persona_apellidomaterno).",".Utility::camelCaseIam($item->capacitador->persona_nombres);
		echo '</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if(isset($item->actividad->actividad_id))
			echo $item->actividad->actividad_nombre;
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if(isset($item->listaverificacion->grupolistaverificacion->grupolistaverificacion_nombre))
			echo $item->listaverificacion->grupolistaverificacion->grupolistaverificacion_nombre;
		echo '</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if(isset($item->listaverificacion->listaverificacion_nombre))
			echo $item->listaverificacion->listaverificacion_nombre;
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		echo $recurso_virtualtiponombre;
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		echo $recurso_virtualtiporecursonombre;
		echo '</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		echo $recurso_identificador;
		echo '</td>';
		

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		echo $proceso_nombre;
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		if(isset($item->unidadtrabajo->unidadtrabajo_nombre))
			echo $item->unidadtrabajo->unidadtrabajo_nombre;
		echo '</td>';

		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		echo $item->itemnc*1 == 0 ? "Conforme" : "No Conforme";
		echo '</td>';
		
		echo '<td class="td_css"'.$item->totalRowSpanHtml.'>';
		echo $item->itemnc;
		echo '</td>';
		
		
		if($item->totalRowSpan>0){

			$contadorRows = 0;

			foreach($item->itemsListaVerficacionNoCumplen as $itemNoCumple){

				if($contadorRows >0){
					echo '<tr>';
				}

				echo '<td class="td_css">';
				echo $itemNoCumple->listaverificacionitem_descripcion;
				echo'</td>';
				
				echo '<td class="td_css">';
				echo $itemNoCumple->listaverificacionitem_niveltxt;
				echo'</td>';
				

				if($contadorRows>0){
					echo '</tr>';
				}

				$contadorRows = $contadorRows+1;

			}
		}else{
			echo '<td class="td_css">'."".'</td>';
            echo '<td class="td_css">'."".'</td>';
		}

		if($item->totalRowSpan<2){
			echo '</tr>';
		}
    }
    if (sizeof($List)==0){
        echo '<tr><th colspan="11">No hay datos</th></tr>';
    }

    ?>
    </tbody>
</table>

</body>
</html>