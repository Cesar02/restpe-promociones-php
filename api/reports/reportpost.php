<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Recupero el tipo de reporte
 */
if (isset($_GET["type"]) && $_GET["type"] != "") {
    $type = $_GET["type"];
} else {
    $type = "html";
}
$name_report = "reporte.pdf";

if (isset($_GET["name"])) {
    $name_report = $_GET["name"];
}

if (isset($_GET["title"]) && $_GET["title"] != "") {
    $title = $_GET["title"];
} else {
    $title = "reporte";
}

$mode = "w"; // modo guardar sin visualizar
if (isset($_GET["mode"])) {
    $mode = $_GET["mode"];
}

$report_color = "#5484D5";
$report_color = "#10B6F4";


if ($type == "pdf") {
//    require_once('../core/libs/vendor/mpdf/mpdf/mpdf.php');
}
define("PATH", "../");
include_once("../config.php");

$security = new Security(false);
$activa = true;
$activa = false;

/**
 * SACO EL IGV DEL COSTO TOTAL
 */
//verificar si tiene un token valido
$tokenValido = false;
/*$usuario_id = Tokenusuario::getUsuarioId(Security::getToken());
if ($usuario_id != -1) {
    $tokenValido = true;
}*/
//fin de verificar si tiene un token valido
$name = $name_report;


if ($type == "pdf") {
    ob_start();
} else if ($type == "excel") {
    header("Content-Type:   application/vnd.ms-excel; charset=utf-8");

    //if ($fecha) {
        /*date_default_timezone_set*/
        ('America/Lima');
        $name = $name_report . "-" . date("Y-m-d H:i:s");
    //}


    header("Content-Disposition: attachment; filename=" . $name . ".xls");  //File name extension was wrong
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false);
}
//
$page = $_GET["page"];
/**
 * citas_citaespecialidad
 */
$report_parameter = explode('_', $page);
if (sizeof($report_parameter) <= 0) {
    exit("Error de parametros");
}
$page = $report_parameter[0];

$lang = APP_LANG;
$paper = "A4";
if (isset($_GET["paper"])) {
    $paper = $_GET["paper"];
}

/*switch ($lang) {
    case 'en':
        $lang_file = 'EN';
        break;

    case 'de':
        $lang_file = 'lang.de.php';
        break;

    case 'es':
        $lang_file = 'ES';
        break;

    default:
        $lang_file = 'ES';
}
include_once '../../languages/' . $lang_file . '/lang.php';*/

switch ($report_parameter[1]) {
    default:
    case 'login':
        include('../../pages/' . $page . '.php');
        break;
    case 'index':
	case 'capacitacion':
    case 'tablas':
        if (file_exists(dirname(__FILE__) . '/' . $report_parameter[1] . "/" . $page . '.php')) {
            include(dirname(__FILE__) . '/' . $report_parameter[1] . '/' . $page . '.php');
        }
        break;
}


// convert in PDF

try {
    /**
     * Puede ser L o P
     */
    if ($type == "pdf") {
        $or = $_GET["or"];
        $content = ob_get_clean();

        $or = "L";
        if (isset($_GET["or"])) {
            switch ($_GET["or"]) {
                default:
                    $or = "L";
                    break;
                case "P":
                    $or = "P";
                    break;
                case "L":
                    $or = "L";
                    break;
            }
        }


        //$mpdf = new mPDF('c','A4',0,'Arial',22,22,10);
        $mpdf = new \Mpdf\Mpdf( array("mode"=>"c","format"=>"A4".'-'.$or,"default_font_size"=>0,"default_font"=>'Arial','tempDir' => sys_get_temp_dir().DIRECTORY_SEPARATOR.'mpdf'));
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->list_indent_first_level = 0;
        $html = utf8_encode($content);
        $mpdf->WriteHTML($content);
        $mpdf->Output();
        //$html2pdf->Output($name . ".pdf");
    }
} catch (HTML2PDF_exception $e) {
    echo $e;
    exit;
}
?>
