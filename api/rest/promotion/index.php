<?php 
define('PATH', '../../'); 
 
require '../../config.php'; 
 
$app = new SlimApp(); 
 
/** Peticiones */ 
 $app->post('/promotion','add'); 
 $app->put('/promotion','update'); 
 $app->get('/promotion/{id}', 'getById'); 
 $app->delete('/promotion/{id}', 'delete'); 
 $app->get('/listarPorPaginacion/{pagina}/{registros}', 'listarPorPaginacion'); 
 $app->get('/getAllActivos', 'getAllActivos'); 
 // Rest para yape list
 $app->post('/promotionCreate', 'promotionCreate'); 
 $app->post('/promotionUpdate', 'promotionUpdate'); 
 $app->get('/promotionDelete/{promotion_id}', 'promotionDelete'); 
 $app->get('/promotionList', 'promotionList'); 
 $app->get('/promotionById/{promotion_id}', 'promotionById'); 
 $app->get('/producto/stockById/{producto_id}', 'productoStockById'); 
 $app->post('/venta/producto', 'venderProducto'); 
 $app->run(); 
 
 function add($request, $response, $args) { 
 	$obj = json_decode($request->getBody()); 
 	$ctrl = new PromotionController(); 
 	$array = get_object_vars($obj); 
 	return $response->withStatus(200)->withJson($ctrl->add($array));
 } 
 function update($request, $response, $args) { 
 	$obj = json_decode($request->getBody()); 
 	$ctrl = new PromotionController(); 
 	$array = get_object_vars($obj); 
 	return $response->withStatus(200)->withJson($ctrl->update($array));
 } 
 function getById($request, $response, $args) { 
 	$ctrl = new PromotionController(); 
 	return $response->withStatus(200)->withJson($ctrl->getById($args['id']));
 } 
 function delete($request, $response, $args) { 
 	$ctrl = new PromotionController(); 
 	return $response->withStatus(200)->withJson($ctrl->delete($args['id']));
 } 
 function listarPorPaginacion($request, $response, $args) { 
 	$ctrl = new PromotionController(); 
 	return $response->withStatus(200)->withJson($ctrl->listarPorPaginacion($args['pagina'], $args['registros']));
 } 
 function getAllActivos($request, $response, $args) { 
 	$ctrl = new PromotionController(); 
 	return $response->withStatus(200)->withJson($ctrl->getAllActivos()); 
 }
 // Integracipn YAPE REST
 function promotionCreate($request, $response, $args) { 
 	$obj = json_decode($request->getBody()); 
 	$ctrl = new PromotionController(); 
 	// $array = get_object_vars($obj); 
 	return $response->withStatus(200)->withJson($ctrl->promotionCreate($obj));
 }
 function promotionUpdate($request, $response, $args) { 
 	$obj = json_decode($request->getBody()); 
 	$ctrl = new PromotionController(); 
 	// $array = get_object_vars($obj); 
 	return $response->withStatus(200)->withJson($ctrl->promotionUpdate($obj));
 }
 function promotionDelete($request, $response, $args) { 
 	$ctrl = new PromotionController(); 
 	return $response->withStatus(200)->withJson($ctrl->promotionDelete($args['promotion_id'])); 
 }
 function promotionList($request, $response, $args) { 
 	$ctrl = new PromotionController(); 
 	return $response->withStatus(200)->withJson($ctrl->promotionList()); 
 }
 function promotionById($request, $response, $args) { 
 	$ctrl = new PromotionController(); 
 	return $response->withStatus(200)->withJson($ctrl->promotionById($args['promotion_id'])); 
 }
 function productoStockById($request, $response, $args) { 
 	$ctrl = new PromotionController(); 
 	return $response->withStatus(200)->withJson($ctrl->productoStockById($args['producto_id'])); 
 }
 function venderProducto($request, $response, $args) { 
 	$obj = json_decode($request->getBody()); 
 	$ctrl = new PromotionController(); 
 	// $array = get_object_vars($obj); 
 	return $response->withStatus(200)->withJson($ctrl->venderProducto($obj));
 }
 ?>