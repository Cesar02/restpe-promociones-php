<?php
define("SUCCESS", '1');
define("WARNING", '2');
define("ERROR", '3');
define("INFO", 4);
define("DANGER", 3);
define("NOPERMITIDO", 401);
//Sirve para habilitar las peticiones rest sin token
defined('JS2') || define('JS2', 1);
if (Security::esVersionOnline()) {
    defined('SEND_ERRORS') || define('SEND_ERRORS', true);
} else {
    defined('SEND_ERRORS') || define('SEND_ERRORS', false);
}
define("LIMIT_RESULT", 2000);
define("EMAIL_KEY", "xxxxxxxxxxxxxxx");

//Variables Globales de Notificaciones
define("ACTIVO", "1");
define("INACTIVO", "0");

/**
 * VARIABLE INDEFINIDA
 */
defined('UNDEFINED') || define('UNDEFINED', -1);
defined('REST_TODOS') || define('REST_TODOS', -1);
/**
 * CONSTANTES USADAS PARA TRAER ARREGLOS DEL SERVIDOR
 */
defined('PARAM_TODOS') || define('PARAM_TODOS', "-1");
defined('PARAM_ESTADO_TODOS') || define('PARAM_ESTADO_TODOS', "-1");
defined('PARAM_ESTADO_ACTIVO') || define('PARAM_ESTADO_ACTIVO', "1");
defined('PARAM_ESTADO_INACTIVO') || define('PARAM_ESTADO_INACTIVO', "0");
define('NO_DEFINIDO', -1);
defined('MODULO_VENTA') || define('MODULO_VENTA', 1);

defined('SI') || define('SI', '1');
defined('NO') || define('NO', '0');


/* PAISES */
defined('PAIS_PERU') || define('PAIS_PERU', 1);

/* YAPE ACCESS */
defined('yp_url') || define('yp_url', 'https://api.yape.pe/yape-rest');
defined('yp_client_secret') || define('yp_client_secret', '1');
defined('yp_client_id') || define('yp_client_id', '1');




?>