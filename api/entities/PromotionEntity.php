<?php 
class PromotionEntity extends EntityBase implements DBOCrud { 
     function __construct($options = array()) { 
        parent::__construct($options);
    }
    public $promotion_id; 
    public $promotion_subdominio; 
    public $promotion_domnio; 
    public $local_id; 
    public $categoria_id; 
    public $title; 
    public $description; 
    public $imageUrl; 
    public $terms_conditions; 
    public $start_date; 
    public $end_date; 
    public $redemption_description; 
    public $total_stock; 
    public $stock_user; 
    public $expiration_date; 
	public $promotion_estado;
	public $promotion_fechacreacion;

    public function getPromotion_id(){ 
        return $this->promotion_id;
    }
    public function setPromotion_id($promotion_id){ 
        $this->promotion_id = $promotion_id;
    }
    public function getPromotion_subdominio(){ 
        return $this->promotion_subdominio;
    }
    public function setPromotion_subdominio($promotion_subdominio){ 
        $this->promotion_subdominio = $promotion_subdominio;
    }
    public function getPromotion_domnio(){ 
        return $this->promotion_domnio;
    }
    public function setPromotion_domnio($promotion_domnio){ 
        $this->promotion_domnio = $promotion_domnio;
    }
    public function getLocal_id(){ 
        return $this->local_id;
    }
    public function setLocal_id($local_id){ 
        $this->local_id = $local_id;
    }
    public function getCategoria_id(){ 
        return $this->categoria_id;
    }
    public function setCategoria_id($categoria_id){ 
        $this->categoria_id = $categoria_id;
    }
    public function getTitle(){ 
        return $this->title;
    }
    public function setTitle($title){ 
        $this->title = $title;
    }
    public function getDescription(){ 
        return $this->description;
    }
    public function setDescription($description){ 
        $this->description = $description;
    }
    public function getImageUrl(){ 
        return $this->imageUrl;
    }
    public function setImageUrl($imageUrl){ 
        $this->imageUrl = $imageUrl;
    }
    public function getTerms_conditions(){ 
        return $this->terms_conditions;
    }
    public function setTerms_conditions($terms_conditions){ 
        $this->terms_conditions = $terms_conditions;
    }
    public function getStart_date(){ 
        return $this->start_date;
    }
    public function setStart_date($start_date){ 
        $this->start_date = $start_date;
    }
    public function getEnd_date(){ 
        return $this->end_date;
    }
    public function setEnd_date($end_date){ 
        $this->end_date = $end_date;
    }
    public function getRedemption_description(){ 
        return $this->redemption_description;
    }
    public function setRedemption_description($redemption_description){ 
        $this->redemption_description = $redemption_description;
    }
    public function getTotal_stock(){ 
        return $this->total_stock;
    }
    public function setTotal_stock($total_stock){ 
        $this->total_stock = $total_stock;
    }
    public function getStock_user(){ 
        return $this->stock_user;
    }
    public function setStock_user($stock_user){ 
        $this->stock_user = $stock_user;
    }
    public function getExpiration_date(){ 
        return $this->expiration_date;
    }
    public function setExpiration_date($expiration_date){ 
        $this->expiration_date = $expiration_date;
    }
	public function getPromotion_estado(){
		return $this->promotion_estado;
	}
	public function setPromotion_estado($promotion_estado){
		$this->promotion_estado = $promotion_estado;
	}
	public function getPromotion_fechacreacion(){ 
		return $this->promotion_fechacreacion;
	}
	public function setPromotion_fechacreacion($promotion_fechacreacion){
		$this->promotion_fechacreacion = $promotion_fechacreacion;
	}
 
    public function insert(){
    	try {
    		global $pdo;
    		$query = '';
    		$query2 = '';
    		if(isset($this->promotion_id))
    			$query.='promotion_id, ';
    		if(isset($this->promotion_subdominio))
    			$query.='promotion_subdominio, ';
    		if(isset($this->promotion_domnio))
    			$query.='promotion_domnio, ';
    		if(isset($this->local_id))
    			$query.='local_id, ';
    		if(isset($this->categoria_id))
    			$query.='categoria_id, ';
    		if(isset($this->title))
    			$query.='title, ';
    		if(isset($this->description))
    			$query.='description, ';
    		if(isset($this->imageUrl))
    			$query.='imageUrl, ';
    		if(isset($this->terms_conditions))
    			$query.='terms_conditions, ';
    		if(isset($this->start_date))
    			$query.='start_date, ';
    		if(isset($this->end_date))
    			$query.='end_date, ';
    		if(isset($this->redemption_description))
    			$query.='redemption_description, ';
    		if(isset($this->total_stock))
    			$query.='total_stock, ';
    		if(isset($this->stock_user))
    			$query.='stock_user, ';
    		if(isset($this->expiration_date))
    			$query.='expiration_date, ';
			if(isset($this->promotion_estado))
				$query.='promotion_estado, ';
			if(isset($this->promotion_fechacreacion))
				$query.='promotion_fechacreacion, ';

    		if(isset($this->promotion_id))
    			$query2.=':promotion_id, ';
    		if(isset($this->promotion_subdominio))
    			$query2.=':promotion_subdominio, ';
    		if(isset($this->promotion_domnio))
    			$query2.=':promotion_domnio, ';
    		if(isset($this->local_id))
    			$query2.=':local_id, ';
    		if(isset($this->categoria_id))
    			$query2.=':categoria_id, ';
    		if(isset($this->title))
    			$query2.=':title, ';
    		if(isset($this->description))
    			$query2.=':description, ';
    		if(isset($this->imageUrl))
    			$query2.=':imageUrl, ';
    		if(isset($this->terms_conditions))
    			$query2.=':terms_conditions, ';
    		if(isset($this->start_date))
    			$query2.=':start_date, ';
    		if(isset($this->end_date))
    			$query2.=':end_date, ';
    		if(isset($this->redemption_description))
    			$query2.=':redemption_description, ';
    		if(isset($this->total_stock))
    			$query2.=':total_stock, ';
    		if(isset($this->stock_user))
    			$query2.=':stock_user, ';
    		if(isset($this->expiration_date))
    			$query2.=':expiration_date, ';
			if(isset($this->promotion_estado))
				$query2.=':promotion_estado, ';
			if(isset($this->promotion_fechacreacion))
				$query2.=':promotion_fechacreacion, ';

    		$query = substr($query, 0, strlen($query) - 2);
    		$query2 = substr($query2, 0, strlen($query2) - 2);

    		$stmt = $pdo->prepare('INSERT INTO promotion('.$query.') VALUES('.$query2.')');

    		if(isset($this->promotion_id))
    			$stmt->bindParam(':promotion_id',	$this->promotion_id,	PDO::PARAM_STR);
    		if(isset($this->promotion_subdominio))
    			$stmt->bindParam(':promotion_subdominio',	$this->promotion_subdominio,	PDO::PARAM_STR);
    		if(isset($this->promotion_domnio))
    			$stmt->bindParam(':promotion_domnio',	$this->promotion_domnio,	PDO::PARAM_STR);
    		if(isset($this->local_id))
    			$stmt->bindParam(':local_id',	$this->local_id,	PDO::PARAM_STR);
    		if(isset($this->categoria_id))
    			$stmt->bindParam(':categoria_id',	$this->categoria_id,	PDO::PARAM_STR);
    		if(isset($this->title))
    			$stmt->bindParam(':title',	$this->title,	PDO::PARAM_STR);
    		if(isset($this->description))
    			$stmt->bindParam(':description',	$this->description,	PDO::PARAM_STR);
    		if(isset($this->imageUrl))
    			$stmt->bindParam(':imageUrl',	$this->imageUrl,	PDO::PARAM_STR);
    		if(isset($this->terms_conditions))
    			$stmt->bindParam(':terms_conditions',	$this->terms_conditions,	PDO::PARAM_STR);
    		if(isset($this->start_date))
    			$stmt->bindParam(':start_date',	$this->start_date,	PDO::PARAM_STR);
    		if(isset($this->end_date))
    			$stmt->bindParam(':end_date',	$this->end_date,	PDO::PARAM_STR);
    		if(isset($this->redemption_description))
    			$stmt->bindParam(':redemption_description',	$this->redemption_description,	PDO::PARAM_STR);
    		if(isset($this->total_stock))
    			$stmt->bindParam(':total_stock',	$this->total_stock,	PDO::PARAM_STR);
    		if(isset($this->stock_user))
    			$stmt->bindParam(':stock_user',	$this->stock_user,	PDO::PARAM_STR);
    		if(isset($this->expiration_date))
    			$stmt->bindParam(':expiration_date',	$this->expiration_date,	PDO::PARAM_STR);
			if(isset($this->promotion_estado))
				$stmt->bindParam(':promotion_estado',	$this->promotion_estado,	PDO::PARAM_STR);
			if(isset($this->promotion_fechacreacion))
				$stmt->bindParam(':promotion_fechacreacion',	$this->promotion_fechacreacion,	PDO::PARAM_STR);
    		$stmt->execute();
    		if($stmt->rowCount() === 1){
    			return $pdo->lastInsertId();
    		}else{
    			return false;
    		}
    	} catch (PDOException $e) {
    		echo 'Error: ' . $e->getMessage() . '\n'. $e->getTraceAsString();
    	}
    }

 
    public function update(){
    	try {
    		global $pdo;
    		$query='UPDATE promotion SET ';
    		if(isset($this->promotion_subdominio))
    			$query.='promotion_subdominio=:promotion_subdominio, ';
    		if(isset($this->promotion_domnio))
    			$query.='promotion_domnio=:promotion_domnio, ';
    		if(isset($this->local_id))
    			$query.='local_id=:local_id, ';
    		if(isset($this->categoria_id))
    			$query.='categoria_id=:categoria_id, ';
    		if(isset($this->title))
    			$query.='title=:title, ';
    		if(isset($this->description))
    			$query.='description=:description, ';
    		if(isset($this->imageUrl))
    			$query.='imageUrl=:imageUrl, ';
    		if(isset($this->terms_conditions))
    			$query.='terms_conditions=:terms_conditions, ';
    		if(isset($this->start_date))
    			$query.='start_date=:start_date, ';
    		if(isset($this->end_date))
    			$query.='end_date=:end_date, ';
    		if(isset($this->redemption_description))
    			$query.='redemption_description=:redemption_description, ';
    		if(isset($this->total_stock))
    			$query.='total_stock=:total_stock, ';
    		if(isset($this->stock_user))
    			$query.='stock_user=:stock_user, ';
    		if(isset($this->expiration_date))
    			$query.='expiration_date=:expiration_date, ';
			if(isset($this->promotion_estado))
				$query.='promotion_estado=:promotion_estado, ';
			if(isset($this->promotion_fechacreacion))
				$query.='promotion_fechacreacion=:promotion_fechacreacion, ';

    		if($query!='UPDATE promotion SET ')
    			$query = substr($query, 0, strlen($query) - 2);
    		$query.=' WHERE promotion_id=:promotion_id';
    		$stmt = $pdo->prepare($query);

    		$stmt->bindParam(':promotion_id',	$this->promotion_id,	PDO::PARAM_STR);

    		if(isset($this->promotion_subdominio))
    			$stmt->bindParam(':promotion_subdominio',	$this->promotion_subdominio,	PDO::PARAM_STR);
    		if(isset($this->promotion_domnio))
    			$stmt->bindParam(':promotion_domnio',	$this->promotion_domnio,	PDO::PARAM_STR);
    		if(isset($this->local_id))
    			$stmt->bindParam(':local_id',	$this->local_id,	PDO::PARAM_STR);
    		if(isset($this->categoria_id))
    			$stmt->bindParam(':categoria_id',	$this->categoria_id,	PDO::PARAM_STR);
    		if(isset($this->title))
    			$stmt->bindParam(':title',	$this->title,	PDO::PARAM_STR);
    		if(isset($this->description))
    			$stmt->bindParam(':description',	$this->description,	PDO::PARAM_STR);
    		if(isset($this->imageUrl))
    			$stmt->bindParam(':imageUrl',	$this->imageUrl,	PDO::PARAM_STR);
    		if(isset($this->terms_conditions))
    			$stmt->bindParam(':terms_conditions',	$this->terms_conditions,	PDO::PARAM_STR);
    		if(isset($this->start_date))
    			$stmt->bindParam(':start_date',	$this->start_date,	PDO::PARAM_STR);
    		if(isset($this->end_date))
    			$stmt->bindParam(':end_date',	$this->end_date,	PDO::PARAM_STR);
    		if(isset($this->redemption_description))
    			$stmt->bindParam(':redemption_description',	$this->redemption_description,	PDO::PARAM_STR);
    		if(isset($this->total_stock))
    			$stmt->bindParam(':total_stock',	$this->total_stock,	PDO::PARAM_STR);
    		if(isset($this->stock_user))
    			$stmt->bindParam(':stock_user',	$this->stock_user,	PDO::PARAM_STR);
    		if(isset($this->expiration_date))
    			$stmt->bindParam(':expiration_date',	$this->expiration_date,	PDO::PARAM_STR);
			if(isset($this->promotion_estado))
				$stmt->bindParam(':promotion_estado',	$this->promotion_estado,	PDO::PARAM_STR);
			if(isset($this->promotion_fechacreacion))
				$stmt->bindParam(':promotion_fechacreacion',	$this->promotion_fechacreacion,	PDO::PARAM_STR);

    		return $stmt->execute();
    	} catch (PDOException $e) {
    		echo 'Error: ' . $e->getMessage();
    	}
    }

 
    public function delete(){
    	try {
    		global $pdo;
    		$sql = 'DELETE FROM promotion WHERE promotion_id=:promotion_id';
    		$stmt = $pdo->prepare($sql);
    		$stmt->bindParam(':promotion_id',$this->promotion_id, PDO::PARAM_STR);
    		$stmt->execute();
    		return $stmt->rowCount();
    	} catch (Exception $exc) {
    		echo $exc->getTraceAsString();
    	}
    }
 
    public static function getById($promotion_id){
    	global $pdo;
    	$sql = 'SELECT * FROM promotion WHERE promotion_id=:promotion_id';
    	$stmt = $pdo->prepare($sql);
    	$stmt->bindParam(':promotion_id',$promotion_id, PDO::PARAM_STR);
    	$stmt->execute();
    	$row = $stmt->fetch(PDO::FETCH_ASSOC);
    	if($row){
    		return new Promotion($row);
    	}else{
    		return false;
      }
    }
 
    public static function getList($orderParams = array(), $start = 0, $limit = LIMIT_RESULT) {
 	  	return self::getByFields(array(), $orderParams, $start, $limit);
 	  }
 
    public static function getByFields($whereParams = array(),  $orderParams = array(), $start = 0, $limit = LIMIT_RESULT){
 	  try{
 	  	global $pdo;
 	  	$tbases_vector = array();
 	  	$orderClause = '';
 	  	if(count($orderParams)>0){
 	  		$arrOrderParams = array();
 	  		foreach ($orderParams as $op){
 	  			$arrOrderParams[] = sprintf('%s %s', $op['field'], $op['order']);
 	  		}
 	  	$orderClause = ' ORDER by '. join(', ', $arrOrderParams);
 	  }else{
 	  	$orderClause = ' ORDER by promotion_id';
 	  }
 	  $whereClause = '';
 	  if(count($whereParams)>0){
 	  	$arrWhereParams = array();
 	  	foreach($whereParams as $wp){
 	  		if (isset($wp['conditional'])) {
 	  			if ($wp['conditional'] == '' || $wp['conditional'] == NULL) {
 	  				$conditional = 'and';
 	  			} else {
 	  			switch(strtolower(trim($wp['conditional'],' '))){
 	  				case 'and':
 	  					$conditional = 'and';break;
 	  				case 'or':
 	  					$conditional = 'or';break;
 	  				default :
 	  					$conditional = 'and';
 	  				}
 	  			}
 	  		} else {
 	  			$conditional = 'and';
 	  		}
 	  		$whereClause .= sprintf(' %s %s :%s %s', $wp['field'], $wp['operator'], $wp['field'],$conditional);
 	  	}
 	  		$whereClause = trim($whereClause,'and');
 	  		$whereClause = trim($whereClause,'or');
 	  		$whereClause = ' where '.$whereClause;
 	  	}
 	  	$query = 'SELECT SQL_CALC_FOUND_ROWS * FROM promotion '.$whereClause .' '.$orderClause.' ';
 	  		if($limit!=0){
 	  		$query.=' LIMIT :start, :limit';
 	  		}
 	  	$stmt = $pdo->prepare($query);
 	  	if(count($whereParams)>0){
 	  		foreach($whereParams as $wp){
 	  		$stmt->bindParam(':'.$wp['field'], $wp['value']);
 	  		}
 	  	}
 	  	$start = (int)$start;
 	  	$limit = (int)$limit;
 	  		if($limit!=0){
 	  			$stmt->bindParam(':start', $start, PDO::PARAM_INT);
 	  			$stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
 	  		}
 	  	$stmt->execute();
 	  	$stmt->setFetchMode(PDO::FETCH_CLASS, 'PromotionEntity');
 	  	$result = $pdo->query('SELECT FOUND_ROWS() AS totalCount');
 	  	$result->setFetchMode(PDO::FETCH_ASSOC);
 	  	$row = $result->fetch();
 	  	$tbases = array();
 	  	while($tbases = $stmt->fetch()){
 	  		$tbases_vector[] = $tbases;
 	  	}
 	  	return array('promotion_array' =>$tbases_vector, 'totalCount'=>$row['totalCount']);
 	  	} catch (Exception $exc) {
 	  		echo $exc->getTraceAsString();
 	  	}
 	  }
 
    public static function getTotalRows(){
    	$total_rows = 0;
    	try {
    		global $pdo;
    		$sql = 'select count(*) from  promotion';
    		$stmt = $pdo->query($sql);
    		$stmt->execute();
    		if($row = $stmt->fetch()){
    			$total_rows = $row[0];
    		}
    	} catch (Exception $exc) {
    		$total_rows = 0;
    	}
    	return $total_rows;
    }
}
?>