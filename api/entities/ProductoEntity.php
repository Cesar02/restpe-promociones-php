<?php 
class ProductoEntity extends EntityBase implements DBOCrud { 
     function __construct($options = array()) { 
        parent::__construct($options);
    }
    public $producto_id; 
    public $promotion_id; 
    public $producto_webappid; 
    public $producto_presentacion; 
    public $producto_precio; 
    public $producto_preciopromocion; 
    public $producto_comision; 

    public function getProducto_id(){ 
        return $this->producto_id;
    }
    public function setProducto_id($producto_id){ 
        $this->producto_id = $producto_id;
    }
    public function getPromotion_id(){ 
        return $this->promotion_id;
    }
    public function setPromotion_id($promotion_id){ 
        $this->promotion_id = $promotion_id;
    }
    public function getProducto_webappid(){ 
        return $this->producto_webappid;
    }
    public function setProducto_webappid($producto_webappid){ 
        $this->producto_webappid = $producto_webappid;
    }
    public function getProducto_presentacion(){ 
        return $this->producto_presentacion;
    }
    public function setProducto_presentacion($producto_presentacion){ 
        $this->producto_presentacion = $producto_presentacion;
    }
    public function getProducto_precio(){ 
        return $this->producto_precio;
    }
    public function setProducto_precio($producto_precio){ 
        $this->producto_precio = $producto_precio;
    }
    public function getProducto_preciopromocion(){ 
        return $this->producto_preciopromocion;
    }
    public function setProducto_preciopromocion($producto_preciopromocion){ 
        $this->producto_preciopromocion = $producto_preciopromocion;
    }
    public function getProducto_comision(){ 
        return $this->producto_comision;
    }
    public function setProducto_comision($producto_comision){ 
        $this->producto_comision = $producto_comision;
    }
 
    public function insert(){
    	try {
    		global $pdo;
    		$query = '';
    		$query2 = '';
    		if(isset($this->producto_id))
    			$query.='producto_id, ';
    		if(isset($this->promotion_id))
    			$query.='promotion_id, ';
    		if(isset($this->producto_webappid))
    			$query.='producto_webappid, ';
    		if(isset($this->producto_presentacion))
    			$query.='producto_presentacion, ';
    		if(isset($this->producto_precio))
    			$query.='producto_precio, ';
    		if(isset($this->producto_preciopromocion))
    			$query.='producto_preciopromocion, ';
    		if(isset($this->producto_comision))
    			$query.='producto_comision, ';
    		if(isset($this->producto_id))
    			$query2.=':producto_id, ';
    		if(isset($this->promotion_id))
    			$query2.=':promotion_id, ';
    		if(isset($this->producto_webappid))
    			$query2.=':producto_webappid, ';
    		if(isset($this->producto_presentacion))
    			$query2.=':producto_presentacion, ';
    		if(isset($this->producto_precio))
    			$query2.=':producto_precio, ';
    		if(isset($this->producto_preciopromocion))
    			$query2.=':producto_preciopromocion, ';
    		if(isset($this->producto_comision))
    			$query2.=':producto_comision, ';
    		$query = substr($query, 0, strlen($query) - 2);
    		$query2 = substr($query2, 0, strlen($query2) - 2);

    		$stmt = $pdo->prepare('INSERT INTO producto('.$query.') VALUES('.$query2.')');

    		if(isset($this->producto_id))
    			$stmt->bindParam(':producto_id',	$this->producto_id,	PDO::PARAM_STR);
    		if(isset($this->promotion_id))
    			$stmt->bindParam(':promotion_id',	$this->promotion_id,	PDO::PARAM_STR);
    		if(isset($this->producto_webappid))
    			$stmt->bindParam(':producto_webappid',	$this->producto_webappid,	PDO::PARAM_STR);
    		if(isset($this->producto_presentacion))
    			$stmt->bindParam(':producto_presentacion',	$this->producto_presentacion,	PDO::PARAM_STR);
    		if(isset($this->producto_precio))
    			$stmt->bindParam(':producto_precio',	$this->producto_precio,	PDO::PARAM_STR);
    		if(isset($this->producto_preciopromocion))
    			$stmt->bindParam(':producto_preciopromocion',	$this->producto_preciopromocion,	PDO::PARAM_STR);
    		if(isset($this->producto_comision))
    			$stmt->bindParam(':producto_comision',	$this->producto_comision,	PDO::PARAM_STR);
    		$stmt->execute();
    		if($stmt->rowCount() === 1){
    			return $pdo->lastInsertId();
    		}else{
    			return false;
    		}
    	} catch (PDOException $e) {
    		echo 'Error: ' . $e->getMessage() . '\n'. $e->getTraceAsString();
    	}
    }

 
    public function update(){
    	try {
    		global $pdo;
    		$query='UPDATE producto SET ';
    		if(isset($this->promotion_id))
    			$query.='promotion_id=:promotion_id, ';
    		if(isset($this->producto_webappid))
    			$query.='producto_webappid=:producto_webappid, ';
    		if(isset($this->producto_presentacion))
    			$query.='producto_presentacion=:producto_presentacion, ';
    		if(isset($this->producto_precio))
    			$query.='producto_precio=:producto_precio, ';
    		if(isset($this->producto_preciopromocion))
    			$query.='producto_preciopromocion=:producto_preciopromocion, ';
    		if(isset($this->producto_comision))
    			$query.='producto_comision=:producto_comision, ';

    		if($query!='UPDATE producto SET ')
    			$query = substr($query, 0, strlen($query) - 2);
    		$query.=' WHERE producto_id=:producto_id';
    		$stmt = $pdo->prepare($query);

    		$stmt->bindParam(':producto_id',	$this->producto_id,	PDO::PARAM_STR);

    		if(isset($this->promotion_id))
    			$stmt->bindParam(':promotion_id',	$this->promotion_id,	PDO::PARAM_STR);
    		if(isset($this->producto_webappid))
    			$stmt->bindParam(':producto_webappid',	$this->producto_webappid,	PDO::PARAM_STR);
    		if(isset($this->producto_presentacion))
    			$stmt->bindParam(':producto_presentacion',	$this->producto_presentacion,	PDO::PARAM_STR);
    		if(isset($this->producto_precio))
    			$stmt->bindParam(':producto_precio',	$this->producto_precio,	PDO::PARAM_STR);
    		if(isset($this->producto_preciopromocion))
    			$stmt->bindParam(':producto_preciopromocion',	$this->producto_preciopromocion,	PDO::PARAM_STR);
    		if(isset($this->producto_comision))
    			$stmt->bindParam(':producto_comision',	$this->producto_comision,	PDO::PARAM_STR);
    		return $stmt->execute();
    	} catch (PDOException $e) {
    		echo 'Error: ' . $e->getMessage();
    	}
    }

 
    public function delete(){
    	try {
    		global $pdo;
    		$sql = 'DELETE FROM producto WHERE producto_id=:producto_id';
    		$stmt = $pdo->prepare($sql);
    		$stmt->bindParam(':producto_id',$this->producto_id, PDO::PARAM_STR);
    		$stmt->execute();
    		return $stmt->rowCount();
    	} catch (Exception $exc) {
    		echo $exc->getTraceAsString();
    	}
    }
 
    public static function getById($producto_id){
    	global $pdo;
    	$sql = 'SELECT * FROM producto WHERE producto_id=:producto_id';
    	$stmt = $pdo->prepare($sql);
    	$stmt->bindParam(':producto_id',$producto_id, PDO::PARAM_STR);
    	$stmt->execute();
    	$row = $stmt->fetch(PDO::FETCH_ASSOC);
    	if($row){
    		return new Producto($row);
    	}else{
    		return false;
      }
    }
 
    public static function getList($orderParams = array(), $start = 0, $limit = LIMIT_RESULT) {
 	  	return self::getByFields(array(), $orderParams, $start, $limit);
 	  }
 
    public static function getByFields($whereParams = array(),  $orderParams = array(), $start = 0, $limit = LIMIT_RESULT){
 	  try{
 	  	global $pdo;
 	  	$tbases_vector = array();
 	  	$orderClause = '';
 	  	if(count($orderParams)>0){
 	  		$arrOrderParams = array();
 	  		foreach ($orderParams as $op){
 	  			$arrOrderParams[] = sprintf('%s %s', $op['field'], $op['order']);
 	  		}
 	  	$orderClause = ' ORDER by '. join(', ', $arrOrderParams);
 	  }else{
 	  	$orderClause = ' ORDER by producto_id';
 	  }
 	  $whereClause = '';
 	  if(count($whereParams)>0){
 	  	$arrWhereParams = array();
 	  	foreach($whereParams as $wp){
 	  		if (isset($wp['conditional'])) {
 	  			if ($wp['conditional'] == '' || $wp['conditional'] == NULL) {
 	  				$conditional = 'and';
 	  			} else {
 	  			switch(strtolower(trim($wp['conditional'],' '))){
 	  				case 'and':
 	  					$conditional = 'and';break;
 	  				case 'or':
 	  					$conditional = 'or';break;
 	  				default :
 	  					$conditional = 'and';
 	  				}
 	  			}
 	  		} else {
 	  			$conditional = 'and';
 	  		}
 	  		$whereClause .= sprintf(' %s %s :%s %s', $wp['field'], $wp['operator'], $wp['field'],$conditional);
 	  	}
 	  		$whereClause = trim($whereClause,'and');
 	  		$whereClause = trim($whereClause,'or');
 	  		$whereClause = ' where '.$whereClause;
 	  	}
 	  	$query = 'SELECT SQL_CALC_FOUND_ROWS * FROM producto '.$whereClause .' '.$orderClause.' ';
 	  		if($limit!=0){
 	  		$query.=' LIMIT :start, :limit';
 	  		}
 	  	$stmt = $pdo->prepare($query);
 	  	if(count($whereParams)>0){
 	  		foreach($whereParams as $wp){
 	  		$stmt->bindParam(':'.$wp['field'], $wp['value']);
 	  		}
 	  	}
 	  	$start = (int)$start;
 	  	$limit = (int)$limit;
 	  		if($limit!=0){
 	  			$stmt->bindParam(':start', $start, PDO::PARAM_INT);
 	  			$stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
 	  		}
 	  	$stmt->execute();
 	  	$stmt->setFetchMode(PDO::FETCH_CLASS, 'ProductoEntity');
 	  	$result = $pdo->query('SELECT FOUND_ROWS() AS totalCount');
 	  	$result->setFetchMode(PDO::FETCH_ASSOC);
 	  	$row = $result->fetch();
 	  	$tbases = array();
 	  	while($tbases = $stmt->fetch()){
 	  		$tbases_vector[] = $tbases;
 	  	}
 	  	return array('producto_array' =>$tbases_vector, 'totalCount'=>$row['totalCount']);
 	  	} catch (Exception $exc) {
 	  		echo $exc->getTraceAsString();
 	  	}
 	  }
 
    public static function getTotalRows(){
    	$total_rows = 0;
    	try {
    		global $pdo;
    		$sql = 'select count(*) from  producto';
    		$stmt = $pdo->query($sql);
    		$stmt->execute();
    		if($row = $stmt->fetch()){
    			$total_rows = $row[0];
    		}
    	} catch (Exception $exc) {
    		$total_rows = 0;
    	}
    	return $total_rows;
    }
}
?>