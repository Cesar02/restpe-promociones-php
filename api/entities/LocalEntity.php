<?php 
class LocalEntity extends EntityBase implements DBOCrud { 
     function __construct($options = array()) { 
        parent::__construct($options);
    }
    public $local_id; 
    public $local_webappid; 
    public $local_subdomnio; 
    public $local_domnio; 
    public $local_nombre; 
    public $local_telefono; 
    public $local_urlimagen; 
    public $local_latitud; 
    public $local_longitud; 
    public $local_ruc; 
    public $local_estado; 
    public $local_logo; 
    public $local_direccion; 
    public $local_fecharegistro; 

    public function getLocal_id(){ 
        return $this->local_id;
    }
    public function setLocal_id($local_id){ 
        $this->local_id = $local_id;
    }
    public function getLocal_webappid(){ 
        return $this->local_webappid;
    }
    public function setLocal_webappid($local_webappid){ 
        $this->local_webappid = $local_webappid;
    }
    public function getLocal_subdomnio(){ 
        return $this->local_subdomnio;
    }
    public function setLocal_subdomnio($local_subdomnio){ 
        $this->local_subdomnio = $local_subdomnio;
    }
    public function getLocal_domnio(){ 
        return $this->local_domnio;
    }
    public function setLocal_domnio($local_domnio){ 
        $this->local_domnio = $local_domnio;
    }
    public function getLocal_nombre(){ 
        return $this->local_nombre;
    }
    public function setLocal_nombre($local_nombre){ 
        $this->local_nombre = $local_nombre;
    }
    public function getLocal_telefono(){ 
        return $this->local_telefono;
    }
    public function setLocal_telefono($local_telefono){ 
        $this->local_telefono = $local_telefono;
    }
    public function getLocal_urlimagen(){ 
        return $this->local_urlimagen;
    }
    public function setLocal_urlimagen($local_urlimagen){ 
        $this->local_urlimagen = $local_urlimagen;
    }
    public function getLocal_latitud(){ 
        return $this->local_latitud;
    }
    public function setLocal_latitud($local_latitud){ 
        $this->local_latitud = $local_latitud;
    }
    public function getLocal_longitud(){ 
        return $this->local_longitud;
    }
    public function setLocal_longitud($local_longitud){ 
        $this->local_longitud = $local_longitud;
    }
    public function getLocal_ruc(){ 
        return $this->local_ruc;
    }
    public function setLocal_ruc($local_ruc){ 
        $this->local_ruc = $local_ruc;
    }
    public function getLocal_estado(){ 
        return $this->local_estado;
    }
    public function setLocal_estado($local_estado){ 
        $this->local_estado = $local_estado;
    }
    public function getLocal_logo(){ 
        return $this->local_logo;
    }
    public function setLocal_logo($local_logo){ 
        $this->local_logo = $local_logo;
    }
    public function getLocal_direccion(){ 
        return $this->local_direccion;
    }
    public function setLocal_direccion($local_direccion){ 
        $this->local_direccion = $local_direccion;
    }
    public function getLocal_fecharegistro(){ 
        return $this->local_fecharegistro;
    }
    public function setLocal_fecharegistro($local_fecharegistro){ 
        $this->local_fecharegistro = $local_fecharegistro;
    }
 
    public function insert(){
    	try {
    		global $pdo;
    		$query = '';
    		$query2 = '';
    		if(isset($this->local_id))
    			$query.='local_id, ';
    		if(isset($this->local_webappid))
    			$query.='local_webappid, ';
    		if(isset($this->local_subdomnio))
    			$query.='local_subdomnio, ';
    		if(isset($this->local_domnio))
    			$query.='local_domnio, ';
    		if(isset($this->local_nombre))
    			$query.='local_nombre, ';
    		if(isset($this->local_telefono))
    			$query.='local_telefono, ';
    		if(isset($this->local_urlimagen))
    			$query.='local_urlimagen, ';
    		if(isset($this->local_latitud))
    			$query.='local_latitud, ';
    		if(isset($this->local_longitud))
    			$query.='local_longitud, ';
    		if(isset($this->local_ruc))
    			$query.='local_ruc, ';
    		if(isset($this->local_estado))
    			$query.='local_estado, ';
    		if(isset($this->local_logo))
    			$query.='local_logo, ';
    		if(isset($this->local_direccion))
    			$query.='local_direccion, ';
    		if(isset($this->local_fecharegistro))
    			$query.='local_fecharegistro, ';
    		if(isset($this->local_id))
    			$query2.=':local_id, ';
    		if(isset($this->local_webappid))
    			$query2.=':local_webappid, ';
    		if(isset($this->local_subdomnio))
    			$query2.=':local_subdomnio, ';
    		if(isset($this->local_domnio))
    			$query2.=':local_domnio, ';
    		if(isset($this->local_nombre))
    			$query2.=':local_nombre, ';
    		if(isset($this->local_telefono))
    			$query2.=':local_telefono, ';
    		if(isset($this->local_urlimagen))
    			$query2.=':local_urlimagen, ';
    		if(isset($this->local_latitud))
    			$query2.=':local_latitud, ';
    		if(isset($this->local_longitud))
    			$query2.=':local_longitud, ';
    		if(isset($this->local_ruc))
    			$query2.=':local_ruc, ';
    		if(isset($this->local_estado))
    			$query2.=':local_estado, ';
    		if(isset($this->local_logo))
    			$query2.=':local_logo, ';
    		if(isset($this->local_direccion))
    			$query2.=':local_direccion, ';
    		if(isset($this->local_fecharegistro))
    			$query2.=':local_fecharegistro, ';
    		$query = substr($query, 0, strlen($query) - 2);
    		$query2 = substr($query2, 0, strlen($query2) - 2);

    		$stmt = $pdo->prepare('INSERT INTO local('.$query.') VALUES('.$query2.')');

    		if(isset($this->local_id))
    			$stmt->bindParam(':local_id',	$this->local_id,	PDO::PARAM_STR);
    		if(isset($this->local_webappid))
    			$stmt->bindParam(':local_webappid',	$this->local_webappid,	PDO::PARAM_STR);
    		if(isset($this->local_subdomnio))
    			$stmt->bindParam(':local_subdomnio',	$this->local_subdomnio,	PDO::PARAM_STR);
    		if(isset($this->local_domnio))
    			$stmt->bindParam(':local_domnio',	$this->local_domnio,	PDO::PARAM_STR);
    		if(isset($this->local_nombre))
    			$stmt->bindParam(':local_nombre',	$this->local_nombre,	PDO::PARAM_STR);
    		if(isset($this->local_telefono))
    			$stmt->bindParam(':local_telefono',	$this->local_telefono,	PDO::PARAM_STR);
    		if(isset($this->local_urlimagen))
    			$stmt->bindParam(':local_urlimagen',	$this->local_urlimagen,	PDO::PARAM_STR);
    		if(isset($this->local_latitud))
    			$stmt->bindParam(':local_latitud',	$this->local_latitud,	PDO::PARAM_STR);
    		if(isset($this->local_longitud))
    			$stmt->bindParam(':local_longitud',	$this->local_longitud,	PDO::PARAM_STR);
    		if(isset($this->local_ruc))
    			$stmt->bindParam(':local_ruc',	$this->local_ruc,	PDO::PARAM_STR);
    		if(isset($this->local_estado))
    			$stmt->bindParam(':local_estado',	$this->local_estado,	PDO::PARAM_STR);
    		if(isset($this->local_logo))
    			$stmt->bindParam(':local_logo',	$this->local_logo,	PDO::PARAM_STR);
    		if(isset($this->local_direccion))
    			$stmt->bindParam(':local_direccion',	$this->local_direccion,	PDO::PARAM_STR);
    		if(isset($this->local_fecharegistro))
    			$stmt->bindParam(':local_fecharegistro',	$this->local_fecharegistro,	PDO::PARAM_STR);
    		$stmt->execute();
    		if($stmt->rowCount() === 1){
    			return $pdo->lastInsertId();
    		}else{
    			return false;
    		}
    	} catch (PDOException $e) {
    		echo 'Error: ' . $e->getMessage() . '\n'. $e->getTraceAsString();
    	}
    }

 
    public function update(){
    	try {
    		global $pdo;
    		$query='UPDATE local SET ';
    		if(isset($this->local_webappid))
    			$query.='local_webappid=:local_webappid, ';
    		if(isset($this->local_subdomnio))
    			$query.='local_subdomnio=:local_subdomnio, ';
    		if(isset($this->local_domnio))
    			$query.='local_domnio=:local_domnio, ';
    		if(isset($this->local_nombre))
    			$query.='local_nombre=:local_nombre, ';
    		if(isset($this->local_telefono))
    			$query.='local_telefono=:local_telefono, ';
    		if(isset($this->local_urlimagen))
    			$query.='local_urlimagen=:local_urlimagen, ';
    		if(isset($this->local_latitud))
    			$query.='local_latitud=:local_latitud, ';
    		if(isset($this->local_longitud))
    			$query.='local_longitud=:local_longitud, ';
    		if(isset($this->local_ruc))
    			$query.='local_ruc=:local_ruc, ';
    		if(isset($this->local_estado))
    			$query.='local_estado=:local_estado, ';
    		if(isset($this->local_logo))
    			$query.='local_logo=:local_logo, ';
    		if(isset($this->local_direccion))
    			$query.='local_direccion=:local_direccion, ';
    		if(isset($this->local_fecharegistro))
    			$query.='local_fecharegistro=:local_fecharegistro, ';

    		if($query!='UPDATE local SET ')
    			$query = substr($query, 0, strlen($query) - 2);
    		$query.=' WHERE local_id=:local_id';
    		$stmt = $pdo->prepare($query);

    		$stmt->bindParam(':local_id',	$this->local_id,	PDO::PARAM_STR);

    		if(isset($this->local_webappid))
    			$stmt->bindParam(':local_webappid',	$this->local_webappid,	PDO::PARAM_STR);
    		if(isset($this->local_subdomnio))
    			$stmt->bindParam(':local_subdomnio',	$this->local_subdomnio,	PDO::PARAM_STR);
    		if(isset($this->local_domnio))
    			$stmt->bindParam(':local_domnio',	$this->local_domnio,	PDO::PARAM_STR);
    		if(isset($this->local_nombre))
    			$stmt->bindParam(':local_nombre',	$this->local_nombre,	PDO::PARAM_STR);
    		if(isset($this->local_telefono))
    			$stmt->bindParam(':local_telefono',	$this->local_telefono,	PDO::PARAM_STR);
    		if(isset($this->local_urlimagen))
    			$stmt->bindParam(':local_urlimagen',	$this->local_urlimagen,	PDO::PARAM_STR);
    		if(isset($this->local_latitud))
    			$stmt->bindParam(':local_latitud',	$this->local_latitud,	PDO::PARAM_STR);
    		if(isset($this->local_longitud))
    			$stmt->bindParam(':local_longitud',	$this->local_longitud,	PDO::PARAM_STR);
    		if(isset($this->local_ruc))
    			$stmt->bindParam(':local_ruc',	$this->local_ruc,	PDO::PARAM_STR);
    		if(isset($this->local_estado))
    			$stmt->bindParam(':local_estado',	$this->local_estado,	PDO::PARAM_STR);
    		if(isset($this->local_logo))
    			$stmt->bindParam(':local_logo',	$this->local_logo,	PDO::PARAM_STR);
    		if(isset($this->local_direccion))
    			$stmt->bindParam(':local_direccion',	$this->local_direccion,	PDO::PARAM_STR);
    		if(isset($this->local_fecharegistro))
    			$stmt->bindParam(':local_fecharegistro',	$this->local_fecharegistro,	PDO::PARAM_STR);
    		return $stmt->execute();
    	} catch (PDOException $e) {
    		echo 'Error: ' . $e->getMessage();
    	}
    }

 
    public function delete(){
    	try {
    		global $pdo;
    		$sql = 'DELETE FROM local WHERE local_id=:local_id';
    		$stmt = $pdo->prepare($sql);
    		$stmt->bindParam(':local_id',$this->local_id, PDO::PARAM_STR);
    		$stmt->execute();
    		return $stmt->rowCount();
    	} catch (Exception $exc) {
    		echo $exc->getTraceAsString();
    	}
    }
 
    public static function getById($local_id){
    	global $pdo;
    	$sql = 'SELECT * FROM local WHERE local_id=:local_id';
    	$stmt = $pdo->prepare($sql);
    	$stmt->bindParam(':local_id',$local_id, PDO::PARAM_STR);
    	$stmt->execute();
    	$row = $stmt->fetch(PDO::FETCH_ASSOC);
    	if($row){
    		return new Local($row);
    	}else{
    		return false;
      }
    }
 
    public static function getList($orderParams = array(), $start = 0, $limit = LIMIT_RESULT) {
 	  	return self::getByFields(array(), $orderParams, $start, $limit);
 	  }
 
    public static function getByFields($whereParams = array(),  $orderParams = array(), $start = 0, $limit = LIMIT_RESULT){
 	  try{
 	  	global $pdo;
 	  	$tbases_vector = array();
 	  	$orderClause = '';
 	  	if(count($orderParams)>0){
 	  		$arrOrderParams = array();
 	  		foreach ($orderParams as $op){
 	  			$arrOrderParams[] = sprintf('%s %s', $op['field'], $op['order']);
 	  		}
 	  	$orderClause = ' ORDER by '. join(', ', $arrOrderParams);
 	  }else{
 	  	$orderClause = ' ORDER by local_id';
 	  }
 	  $whereClause = '';
 	  if(count($whereParams)>0){
 	  	$arrWhereParams = array();
 	  	foreach($whereParams as $wp){
 	  		if (isset($wp['conditional'])) {
 	  			if ($wp['conditional'] == '' || $wp['conditional'] == NULL) {
 	  				$conditional = 'and';
 	  			} else {
 	  			switch(strtolower(trim($wp['conditional'],' '))){
 	  				case 'and':
 	  					$conditional = 'and';break;
 	  				case 'or':
 	  					$conditional = 'or';break;
 	  				default :
 	  					$conditional = 'and';
 	  				}
 	  			}
 	  		} else {
 	  			$conditional = 'and';
 	  		}
 	  		$whereClause .= sprintf(' %s %s :%s %s', $wp['field'], $wp['operator'], $wp['field'],$conditional);
 	  	}
 	  		$whereClause = trim($whereClause,'and');
 	  		$whereClause = trim($whereClause,'or');
 	  		$whereClause = ' where '.$whereClause;
 	  	}
 	  	$query = 'SELECT SQL_CALC_FOUND_ROWS * FROM local '.$whereClause .' '.$orderClause.' ';
 	  		if($limit!=0){
 	  		$query.=' LIMIT :start, :limit';
 	  		}
 	  	$stmt = $pdo->prepare($query);
 	  	if(count($whereParams)>0){
 	  		foreach($whereParams as $wp){
 	  		$stmt->bindParam(':'.$wp['field'], $wp['value']);
 	  		}
 	  	}
 	  	$start = (int)$start;
 	  	$limit = (int)$limit;
 	  		if($limit!=0){
 	  			$stmt->bindParam(':start', $start, PDO::PARAM_INT);
 	  			$stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
 	  		}
 	  	$stmt->execute();
 	  	$stmt->setFetchMode(PDO::FETCH_CLASS, 'LocalEntity');
 	  	$result = $pdo->query('SELECT FOUND_ROWS() AS totalCount');
 	  	$result->setFetchMode(PDO::FETCH_ASSOC);
 	  	$row = $result->fetch();
 	  	$tbases = array();
 	  	while($tbases = $stmt->fetch()){
 	  		$tbases_vector[] = $tbases;
 	  	}
 	  	return array('local_array' =>$tbases_vector, 'totalCount'=>$row['totalCount']);
 	  	} catch (Exception $exc) {
 	  		echo $exc->getTraceAsString();
 	  	}
 	  }
 
    public static function getTotalRows(){
    	$total_rows = 0;
    	try {
    		global $pdo;
    		$sql = 'select count(*) from  local';
    		$stmt = $pdo->query($sql);
    		$stmt->execute();
    		if($row = $stmt->fetch()){
    			$total_rows = $row[0];
    		}
    	} catch (Exception $exc) {
    		$total_rows = 0;
    	}
    	return $total_rows;
    }
}
?>