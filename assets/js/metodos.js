function getPaginacion(total,nPagina){  	//recibe el total de pagianas y el numero de la pagina seleccionada y devuelve un arreglo json con las paginas
            var arregloPaginas=new Array();          
            if(nPagina<1)nPagina=1;
                for(var i=nPagina-2;i<total+1;i++){
                    if(i==nPagina){                    	
                        arregloPaginas.push({'estado':'active','numero':i});
                    }else if(i>0 && i<nPagina+2){
                        arregloPaginas.push({'estado':'','numero':i});
                    }else if(i>=nPagina+2){                    	
                        return arregloPaginas;
                    }                         
         }
         return arregloPaginas;
 }
 function confirmacion(titulo,btn1,f1,btn2) { 
        $.SmartMessageBox({
                    title : titulo,
                    content : "",
                    buttons : '['+btn2+']['+btn1+']'
                }, function(ButtonPressed) {
                    if (ButtonPressed === btn1) {
                        f1();                       
                    }                            
                });                        
}
function mostrarMensaje(titulo,texto){
         $.smallBox({
                    title : titulo,
                    content : texto,
                    color : "#296191",
                    iconSmall : "fa fa-thumbs-up bounce animated",
                    timeout : 4000
        });               
}
function mostrarError(titulo,texto){
         $.smallBox({
                    title : titulo,
                    content : "<i>"+texto+"...</i>",
                    color : "#C46A69",
                    iconSmall : "fa fa-times fa-2x fadeInRight animated",
                    timeout : 4000      
        });               
}