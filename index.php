<?php
/* ==========================================================
* Rabbit v2.0
* index.php
* 
* http://www.itdast.com
* Copyright ITDAST PERU
*
* Designed and built exclusively for  @ITDAST
* ========================================================== */
define("PATH", "api/");
include_once("api/config.php");
require_once 'functions.php';
/*
* Config
*/
// relative path to theme common resources (styles/iamges/etc)
$_ROOT = "common/";

$page = isset($_GET['page']) ? $_GET['page'] : 'dashboard';

$section = isset($_GET['section']) ? $_GET['section'] : 'dashboard';
$sub_section = isset($_GET['ss']) ? $_GET['ss'] : 'dashboard';

/*
* Current / default page
*/

if (isset($_POST["correo"])) {
    $correo = $_POST["correo"];
    $persona = Persona::loginBackOfffice($correo);
    // var_dump($persona);
    if ($persona !== null) {
        echo "Inicio de sesión exitoso.";
        // echo "<script>localStorage.setItem('token', '" . $persona->token . "');</script>";
        // $token_json = json_encode($persona->token);
        echo "<script>console.log('Tu token de inicio de sesión es: " .$persona->token. "');</script>";

        Security::setSession("persona_id", $persona->getPersona_id());
        // Security::setSession("usuario_usuario", $usuario->getUsuario_usuario());
        // Security::setSession("", $usuario->getUsuario_usuario());
    
        header("Location: ?page=menucursos#/cursos");
        // $page = 'dashboard';
    }else {
        echo "Error en el inicio de sesión. Verifica tus credenciales.";
    }
}
/*
     * Other variables
     * Used mainly for documentation
     */
/**
 * Incluyo mi archivo de configuracion del sistema
 */
/*
     * Pages
     */
$security = new Security(false);

// var_dump($security->isLogged());

if (!$security->isLogged()) {
    $page = 'login';
    $logged=false;
}
else{
    // $page = 'menulineaaprendizaje';
    $logged=true;
}
// else{
//     $page="login";
// }

switch ($page) {
    case 'index':
        break;
    case 'login':
        break;
    case 'videosolo':
        break;
    case 'recursos':
        break;
    case 'menucursos':
        break;
    case 'menuperfil':
        break;
    case 'menurutanegocio':
        break;
    case 'menulineaaprendizaje':
        break;
    case 'roles':
        break;
    case 'menuexamen':
        break;
    case 'novedades':
        break;
    case 'reportesDominios':
        break;
    case 'reportesUsuarios':
        break;
    default:
        $page = "error-404";
        break;
}
// content       
if (file_exists('pages/' . $page . '.php'))
    require_once 'pages/' . $page . '.php';
require_once 'logger.php';
