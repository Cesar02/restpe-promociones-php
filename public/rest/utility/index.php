<?php 
define('PATH', '../../'); 
 
require '../../../api/config.php';
define("PUBLIC_SERVICES",true);
$app = new SlimApp(); 
 

 $app->get('/getReportesByDominios', 'getReportesByDominios'); 
 $app->get('/getReportesByUsuarios', 'getReportesByUsuarios'); 
 $app->run(); 
 

 function getReportesByDominios($request, $response, $args) { 
 	$ctrl = new Utility(); 
 	return $response->withStatus(200)->withJson($ctrl->getReportesByDominios()); 
 } 

 function getReportesByUsuarios($request, $response, $args) { 
 	$ctrl = new Utility(); 
 	return $response->withStatus(200)->withJson($ctrl->getReportesByUsuarios()); 
 } 
 ?>