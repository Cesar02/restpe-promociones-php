<?php
require '../../../api/config.php';
/**
 * DEfino LOGIN_FORM, para poder LOGUEAR sin que me corte por no tener TOKEN y que sea RESFTULL SIN SESSION
 */
define("PUBLIC_SERVICES",true);

$app = new SlimApp();

// $app->post('/login','login');
$app->get('/obtenerCategorias','obtenerCategorias');
$app->get('/obtenertoken','obtenerToken');
$app->run();

// function login($request, $response, $args) {
//     $body = $request->getBody();
//     $obj = json_decode($body);
//     $ctrl = new CommonController();
//     return $response->withStatus(200)->withJson($ctrl->login($obj));
// }

function obtenerCategorias($request, $response, $args) {
    $ctrl = new CommonController();
    return $response->withStatus(200)->withJson($ctrl->obtenerCategorias());
}
function obtenerToken($request, $response, $args) {
    $ctrl = new TokenController();
    return $response->withStatus(200)->withJson($ctrl->obtenerToken());
}

?>